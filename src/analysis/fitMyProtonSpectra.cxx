//Fit the Spectra

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

using namespace std;

Bool_t draw = false;

void fitMyProtonSpectra(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge, Double_t midY){

  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();
  Double_t mTm0min, mTm0max, dNdy, dNdyErr;
  Int_t nPoints;
  TGraphErrors *tpcCorrectedSpectrum; 
  TF1 *fitThermal, *tSlopeFit;
	vector <TGraphErrors *> tSlopeGraph(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph(nCentBins, (TGraphErrors *)NULL);

  //Create the OutputFile
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
		dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    gPad->DrawFrame(-2,0,2,150);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
   
	  //Create the temperature/inverse slope parameter graph and fit
	  tSlopeGraph.at(iCentBin) = new TGraphErrors();
		tSlopeGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph.at(iCentBin)->SetName(Form("SlopeParameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));

		//tSlopeFit = new TF1("tSlopeFit","pol2",-1.8,0.2);
		tSlopeFit = new TF1("tSlopeFit","gaus(0)",-1.2,0.2);
		tSlopeFit->SetNpx(10000);
		tSlopeFit->SetName(Form("SlopeParameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit->SetParameter(0,0.2);//0.22
		tSlopeFit->FixParameter(1,midY); //0.1 
		tSlopeFit->SetParameter(2,0.4); //0.033

		//Create the dN/dy Graphs
		dNdyGraph.at(iCentBin) = new TGraphErrors();
		dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensity_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		for(Int_t iRound = 0;iRound<2;iRound++){

			for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

				cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
				particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

				//Compute Rapidity
				Double_t rapidity = GetRapidityRangeCenter(yIndex);

				//Get Spectra
				spectraFile->cd();
				//spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
				tpcCorrectedSpectrum = (TGraphErrors *)gDirectory->Get(Form("preparedSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
				cout<<"tpcCorrectedSpectrum "<<tpcCorrectedSpectrum<<endl;
				if (!tpcCorrectedSpectrum) continue;
				nPoints = tpcCorrectedSpectrum->GetN();
				if (nPoints < 1){
					cout<<"WARNING: TGraph has no points!"<<endl;
					continue;
				}
				//if (nPoints <= 5) continue;
				Double_t *xarr = tpcCorrectedSpectrum->GetX();      
				mTm0max = ceil(xarr[nPoints-1]*100)/100;
				mTm0min = floor(xarr[0]*100)/100;

				//Get the fit function
				fitThermal = new TF1("",ThermalFitFunc,mTm0min,mTm0max,3);
				fitThermal->SetName(Form("spectrumFitThermal%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
					
				//Set the parameters
				fitThermal->SetParameter(0,100);
				fitThermal->SetParameter(1,0.23);
				fitThermal->FixParameter(2,particleInfo->GetParticleMass(pid));
				//fitBoseEinstein->SetParLimits(1,0.1,0.5);

				if (iRound == 1)
						fitThermal->FixParameter(1,tSlopeFit->Eval(rapidity));

				//Perform the fit
				tpcCorrectedSpectrum->Fit(fitThermal,"REX0");

				if (iRound==0){
					tSlopeGraph.at(iCentBin)->SetPoint(tSlopeGraph.at(iCentBin)->GetN(),
										rapidity, fitThermal->GetParameter(1));								
					tSlopeGraph.at(iCentBin)->SetPointError(tSlopeGraph.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitThermal->GetParError(1));								
				}

			  if (iRound==1){	
					dNdy = fitThermal->GetParameter(0);
					dNdyErr = fitThermal->GetParError(0);
					dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),rapidity-midY,dNdy);
					dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdyErr);
				}

				if (draw){

					canvas->cd();
					canvas->DrawFrame(0,0.01*TMath::MinElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()),
								2.0,20*TMath::MaxElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()));
					canvas->SetLogy();
					tpcCorrectedSpectrum->Draw("APZ");	
					fitThermal->SetLineColor(2);
					fitThermal->SetLineWidth(2);
					fitThermal->SetLineStyle(7);
					fitThermal->Draw("SAME");

				}

				//Save Spectra
				if (iRound==1){
					resultsFile->cd();
					resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
					if (tpcCorrectedSpectrum) tpcCorrectedSpectrum->Write();
				}

			}//End of rapidity loop


		  if (iRound==0) tSlopeGraph.at(iCentBin)->Fit(tSlopeFit,"REX0");

		}//End of loop over fitting rounds

		if (draw){
			dNdyCanvas->cd();
			dNdyGraph.at(iCentBin)->Draw("APZ");
		}

    //Save Plots
		resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
		if (tSlopeGraph.at(iCentBin)->GetN() > 0){
			tSlopeGraph.at(iCentBin)->Write();
		}

		resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0) dNdyGraph.at(iCentBin)->Write();
	

	}//End of centrality loop

}//End of function

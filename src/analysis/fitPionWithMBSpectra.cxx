//Fit the Spectra

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

using namespace std;

Bool_t draw = false;

void fitPionSpectra(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge, Double_t midY, Bool_t fitMB=false){

  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = 1;//GetNCentralityBins();
  Double_t mTm0min, mTm0max, dNdy, dNdyErr;
  Int_t nPoints;
  TGraphErrors *tpcCorrectedSpectrum; 
  TF1 *fitBoseEinstein, *tSlopeFit, *fitMaxwellBoltzmann;
	vector <TGraphErrors *> tSlopeGraph(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph(nCentBins, (TGraphErrors *)NULL);

  //Create the OutputFile
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
		dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    gPad->DrawFrame(-2,0,2,150);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
   
	  //Create the temperature/inverse slope parameter graph and fit
	  tSlopeGraph.at(iCentBin) = new TGraphErrors();
		tSlopeGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph.at(iCentBin)->SetName(Form("SlopeParameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));

		//tSlopeFit = new TF1("tSlopeFit","pol2",-1.8,0.2);
		tSlopeFit = new TF1("tSlopeFit","gaus(0)",-1.8,0.2);
		tSlopeFit->SetNpx(10000);
		tSlopeFit->SetName(Form("SlopeParameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit->SetParameter(0,0.17);//0.22
		tSlopeFit->FixParameter(1,midY); //SetParameter(1,0.1);
		tSlopeFit->SetParameter(2,0.5); //0.04 //0.033

		//Create the dN/dy Graphs
		dNdyGraph.at(iCentBin) = new TGraphErrors();
		dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensity_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		for(Int_t iRound = 0;iRound<2;iRound++){

			for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

				cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
				particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

				//Compute Rapidity
				Double_t rapidity = GetRapidityRangeCenter(yIndex);

				//Get Spectra
				spectraFile->cd();
				//spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
				tpcCorrectedSpectrum = (TGraphErrors *)gDirectory->Get(Form("preparedSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
				cout<<"tpcCorrectedSpectrum "<<tpcCorrectedSpectrum<<endl;
				if (!tpcCorrectedSpectrum) continue;
				nPoints = tpcCorrectedSpectrum->GetN();
				if (nPoints < 1){
					cout<<"WARNING: TGraph has no points!"<<endl;
					continue;
				}
				if (nPoints <= 5) continue;
				Double_t *xarr = tpcCorrectedSpectrum->GetX();      
				mTm0max = ceil(xarr[nPoints-1]*100)/100;
				mTm0min = floor(xarr[0]*100)/100;

				//Get the fit function
				if (fitMB){

					fitMaxwellBoltzmann = new TF1("",ThermalFitFunc,0,0.5,3);
					fitMaxwellBoltzmann->SetName(Form("spectrumFitMaxwellBoltzmann%s_Cent%02d_yIndex%02d",
							particleInfo->GetParticleName(pid,charge).Data(),
							iCentBin,yIndex));
						
					//Set the parameters
					fitMaxwellBoltzmann->SetParameter(0,70);
					fitMaxwellBoltzmann->SetParameter(1,0.16);
					fitMaxwellBoltzmann->FixParameter(2,particleInfo->GetParticleMass(pid));
					fitMaxwellBoltzmann->SetParLimits(1,0.1,0.5);

				}

				else {
					fitBoseEinstein = new TF1("",BoseEinsteinFitFunc,0,0.5,4);
					//BoseEinsteinFitFunc,mTm0min,mTm0max,4
					fitBoseEinstein->SetName(Form("spectrumFitBoseEinstein_%s_Cent%02d_yIndex%02d",
							particleInfo->GetParticleName(pid,charge).Data(),
							iCentBin,yIndex));
						
					//Set the parameters
					fitBoseEinstein->SetParameter(0,100);
					fitBoseEinstein->SetParameter(1,0.16);
					fitBoseEinstein->FixParameter(2,particleInfo->GetParticleMass(pid));
					fitBoseEinstein->FixParameter(3,rapidity+1.52);
					fitBoseEinstein->SetParLimits(1,0.1,0.5);
				}

				if (iRound ==1){
            if (fitMB) fitMaxwellBoltzmann->FixParameter(1,tSlopeFit->Eval(rapidity));
						else fitBoseEinstein->FixParameter(1,tSlopeFit->Eval(rapidity));
				}

				//Perform the fit
				if (fitMB) tpcCorrectedSpectrum->Fit(fitMaxwellBoltzmann,"REXO");
				else tpcCorrectedSpectrum->Fit(fitBoseEinstein,"REXO");

				if (iRound==0){
		
					if (fitMB){

						tSlopeGraph.at(iCentBin)->SetPoint(tSlopeGraph.at(iCentBin)->GetN(),
											rapidity, fitMaxwellBoltzmann->GetParameter(1));								
						tSlopeGraph.at(iCentBin)->SetPointError(tSlopeGraph.at(iCentBin)->GetN()-1,
											rapidityBinWidth/2.0, fitMaxwellBoltzmann->GetParError(1));								
          }
          
          else{
						tSlopeGraph.at(iCentBin)->SetPoint(tSlopeGraph.at(iCentBin)->GetN(),
											rapidity, fitBoseEinstein->GetParameter(1));								
						tSlopeGraph.at(iCentBin)->SetPointError(tSlopeGraph.at(iCentBin)->GetN()-1,
											rapidityBinWidth/2.0, fitBoseEinstein->GetParError(1));								
					}

				}

        if (iRound==1){
				  
					if (fitMB){
					  	
						dNdy = fitMaxwellBoltzmann->GetParameter(0);
						dNdyErr = fitMaxwellBoltzmann->GetParError(0);

					}
					else{

						dNdy = fitBoseEinstein->GetParameter(0);
						dNdyErr = fitBoseEinstein->GetParError(0);

					}

					dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),rapidity-midY,dNdy);
					dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdyErr);
        }

				if (draw){

					canvas->cd();
					canvas->DrawFrame(0,0.01*TMath::MinElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()),
								2.0,20*TMath::MaxElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()));
					canvas->SetLogy();
					tpcCorrectedSpectrum->Draw("APZ");	
					if (fitMB){
						fitMaxwellBoltzmann->SetLineColor(4);
						fitMaxwellBoltzmann->SetLineWidth(2);
						fitMaxwellBoltzmann->SetLineStyle(7);
						fitMaxwellBoltzmann->Draw("SAME");
					}
					else{
						fitBoseEinstein->SetLineColor(2);
						fitBoseEinstein->SetLineWidth(2);
						fitBoseEinstein->SetLineStyle(7);
						fitBoseEinstein->Draw("SAME");
				  }

				}

				//Save Spectra
				if (iRound==1){
					resultsFile->cd();
					resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
					if (tpcCorrectedSpectrum) tpcCorrectedSpectrum->Write();
				}

			}//End of rapidity loop

		  if (iRound==0) tSlopeGraph.at(iCentBin)->Fit(tSlopeFit,"REX0");

		}//End of loop over fitting rounds

		if (draw){
			dNdyCanvas->cd();
			dNdyGraph.at(iCentBin)->Draw("APZ");
		}

    //Save Plots
		resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
		if (tSlopeGraph.at(iCentBin)->GetN() > 0){
			tSlopeGraph.at(iCentBin)->Write();
		}

		resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0) dNdyGraph.at(iCentBin)->Write();
	

	}//End of centrality loop

}//End of function

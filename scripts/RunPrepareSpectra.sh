#!/bin/bash

######################################################
#SET THE SPECTRA FILE NAME AND PATH (FULL PATH)
spectraFile=../userfiles/AuAu_4_5GeV_2015/officialProduction/analysis/rawPionSpectra.root
#SET THE TOF SPECTRA FILE NAME AND PATH (FULL PATH)
preparedSpectraFile=../userfiles/AuAu_4_5GeV_2015/officialProduction/analysis/sysPreppedTPCPionSpectra.root

#SET THE PARTICLE ID
pid=0

#SET THE PARTICLE CHARGE
charge=1

#SET THE RAPIDITY BIN OR USE -999 FOR ALL
yIndex=-999

#SET THE CENTER-OF-MASS ENERGY
energy=4.5

#SET THE EVENT CONFIGURATION
eventConfig=FixedTarget2015

#SET THE STAR LIBRARY
starLibrary=SL16a

#SET IF TPC ONLY
TPConly=true

######################################################

root -l -b -q ../macros/RunPrepareSpectra.C\(\"$spectraFile\",\"$preparedSpectraFile\",$pid,$charge,$yIndex,$energy,\"$eventConfig\",\"$starLibrary\",$TPConly\)

exit


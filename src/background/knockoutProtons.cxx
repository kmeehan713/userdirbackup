//This code loops over the data and constructs the global dca distributions
//of protons and anit-protons for each centrality, rapidity, and mTm0 bin.

#include <iostream>
#include <vector>
#include <utility>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TEventList.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "usefulDataStructs.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

void knockoutProtons(TString inputDataFile, TString outputFile, TString pidCalibFile){

  //Read the DavisDst
  DavisDstReader *davisDst = NULL;
  if (GetZVertexCuts().first != -999 && GetZVertexCuts().second != 999)
    davisDst = new DavisDstReader(inputDataFile,GetZVertexCuts().first,GetZVertexCuts().second);
  else
    davisDst = new DavisDstReader(inputDataFile);
  
  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  
  //If no output file was specified then we won't produce one
  TFile *outFile = outFile = new TFile(outputFile,"RECREATE");
  
  //Create an instance of the Particle Info Class which will use the empirical fits
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibFile);

  const int nCentralityBins = GetNCentralityBins();
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();

  //Histograms
  TH2D *dEdxHisto = new TH2D("dEdxHisto","dEdxHisto;p/q;dEdx",500,-5,5,500,0,50);
  std::vector<TH3D *> protonDCAHisto(nCentralityBins,(TH3D *)NULL);
  std::vector<TH3D *> antiprotonDCAHisto(nCentralityBins,(TH3D *)NULL);
  std::vector<TH3D *> protondEdxHisto(nCentralityBins,(TH3D *)NULL);
  std::vector<TH3D *> antiprotondEdxHisto(nCentralityBins,(TH3D *)NULL);
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    protonDCAHisto.at(iCentBin) =
      new TH3D(Form("glDCA_%s_CentBin%d",particleInfo->GetParticleName(PROTON,1).Data(),iCentBin),
	       Form("Global DCA Distribution of %s | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});glDCA(cm)",
		    particleInfo->GetParticleSymbol(PROTON,1).Data(),
		    iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		    (int)GetCentralityPercents().at(iCentBin),
		    particleInfo->GetParticleSymbol(PROTON,1).Data(),
		    particleInfo->GetParticleSymbol(PROTON,1).Data()),
	       nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,100,0,3);
    antiprotonDCAHisto.at(iCentBin) =
      new TH3D(Form("glDCA_%s_CentBin%d",particleInfo->GetParticleName(PROTON,-1).Data(),iCentBin),
	       Form("Global DCA Distribution of %s | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});glDCA(cm)",
		    particleInfo->GetParticleSymbol(PROTON,-1).Data(),
		    iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		    (int)GetCentralityPercents().at(iCentBin),
		    particleInfo->GetParticleSymbol(PROTON,-1).Data(),
		    particleInfo->GetParticleSymbol(PROTON,-1).Data()),
	       nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,100,0,3);

    protondEdxHisto.at(iCentBin) =
      new TH3D(Form("dEdx_%s_CentBin%d",particleInfo->GetParticleName(PROTON,1).Data(),iCentBin),
	       Form("Energy Loss Distribution of %s | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});glDCA(cm)",
		    particleInfo->GetParticleSymbol(PROTON,1).Data(),
		    iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		    (int)GetCentralityPercents().at(iCentBin),
		    particleInfo->GetParticleSymbol(PROTON,1).Data(),
		    particleInfo->GetParticleSymbol(PROTON,1).Data()),
	       nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-10,10);
    antiprotondEdxHisto.at(iCentBin) =
      new TH3D(Form("dEdx_%s_CentBin%d",particleInfo->GetParticleName(PROTON,-1).Data(),iCentBin),
	       Form("Energy Loss Distribution of %s | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});glDCA(cm)",
		    particleInfo->GetParticleSymbol(PROTON,-1).Data(),
		    iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		    (int)GetCentralityPercents().at(iCentBin),
		    particleInfo->GetParticleSymbol(PROTON,-1).Data(),
		    particleInfo->GetParticleSymbol(PROTON,-1).Data()),
	       nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-10,10);
    
    
  }//End Loop Over cent bins to make histograms

  //Get the Number of Events
  const long nEntries = davisDst->GetEntries();

  //Loop Over the entries/events
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    //Loop Over the Vertices
    for (Int_t iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){

      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;

      //Get the Centrality Bin
      UInt_t triggerID = event->GetTriggerID(&allowedTriggers);
      Int_t centralityVariable = GetCentralityVariable(primaryVertex);
      Int_t centralityBin      = GetCentralityBin(centralityVariable,triggerID,
						  primaryVertex->GetZVertex());

      //Skip events with bad centrality bins
      if (centralityBin < 0)
	continue;


      //Loop Over the Tracks
      for (Int_t iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){
	
	track = primaryVertex->GetPrimaryTrack(iTrack);
	if (!IsGoodTrack(track,false))
	  continue;

	//Compute the Track Kinematics
	int speciesIndex = PROTON;
	Double_t massAssumption = particleInfo->GetParticleMass(speciesIndex);
	Double_t rapidity       = track->GetRapidity(massAssumption);
	Double_t mTm0           = track->GetmTm0(massAssumption);
	Int_t yIndex = GetRapidityIndex(rapidity);
	Int_t mTm0Index = GetmTm0Index(mTm0);
	
	if (yIndex < 0 || mTm0Index < 0)
	  continue;

	//Set the Particle Species and Rapidity Bin of interest
	if (!particleInfo->SetEmpiricalBichselFunction(speciesIndex,yIndex)){
	  continue;
	}
	
	//Compute the ZTPC Variable 
	Double_t zTPC = particleInfo->ComputeZTPC(track->GetdEdx(),track->GetPt(),
						  track->GetPz(),speciesIndex);

	//Make a cut so that only protons continue
	if (fabs(zTPC) > 0.15)
	  continue;

	dEdxHisto->Fill(track->GetPTotal()*track->GetCharge(),track->GetdEdx()*pow(10,6));
	
	//Fill the DCA Histograms depending on Charge
	if (track->GetCharge() > 0){
	  protonDCAHisto.at(centralityBin)->Fill(rapidity,mTm0,track->GetGlobalDCA()->Mag());
	  protondEdxHisto.at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	}
	else {
	  antiprotonDCAHisto.at(centralityBin)->Fill(rapidity,mTm0,track->GetGlobalDCA()->Mag());
	  antiprotondEdxHisto.at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	}
	
      }//End Loop Over Tracks
      
    }//End Loop Over Vertices

  }//End Loop Over Events

  //SAVE
  outFile->cd();
  dEdxHisto->Write();
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    
    protonDCAHisto.at(iCentBin)->Write();
    antiprotonDCAHisto.at(iCentBin)->Write();

    protondEdxHisto.at(iCentBin)->Write();
    antiprotondEdxHisto.at(iCentBin)->Write();
    
  }//End Loop Over cent bins to make histograms

  
}

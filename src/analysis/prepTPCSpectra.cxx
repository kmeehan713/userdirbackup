#include <iostream>
#include <vector>
#include <utility>
#include <numeric>

#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TRandom3.h>
#include <TLatex.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"

using namespace std;

void prepTPCSpectra(TString spectraFileName, TString preparedSpectraFileName, Int_t pid, Int_t charge, Int_t yIndex){

	ParticleInfo *particleInfo = new ParticleInfo();
  TGraphErrors *tpcSpectrum, *preparedSpectrum;
  TGraphErrors *rawtpcSpectrum, *statpreparedSpectrum;
  TGraphErrors *systpcSpectrum, *syspreparedSpectrum;

	//Create output file
	TFile *preparedSpectraFile = new TFile(preparedSpectraFileName, "UPDATE");
	preparedSpectraFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	preparedSpectraFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	preparedSpectraFile->mkdir(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Load spectra file
	TFile *spectraFile = new TFile(spectraFileName, "READ");

  //Loop over all rapidities
  Int_t yMin = GetMinRapidityIndexOfInterest(pid);
  if (pid==0 && charge==1) yMin = 4;
  Int_t yMax = GetMaxRapidityIndexOfInterest(pid);
  for (Int_t y=yMin; y<yMax; y++){
  
		if (yIndex >=0 && y != yIndex) continue;
    
    spectraFile->cd();
    tpcSpectrum = (TGraphErrors*)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),
          particleInfo->GetParticleName(pid,charge).Data(),0,y));
    rawtpcSpectrum = (TGraphErrors*)gDirectory->Get(Form("RawSpectra_%s/rawSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),
          particleInfo->GetParticleName(pid,charge).Data(),0,y));
    systpcSpectrum = (TGraphErrors*)gDirectory->Get(Form("SysErrCorrectedSpectra_%s/correctedSysErrSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),
          particleInfo->GetParticleName(pid,charge).Data(),0,y));

    if (!tpcSpectrum){
      cout<<"WARNING: TPC Spectrum for y "<<y<<" does not exist"<<endl;
      continue;
    }

    preparedSpectrum = new TGraphErrors();
    statpreparedSpectrum = new TGraphErrors();
    syspreparedSpectrum = new TGraphErrors();
    preparedSpectrum->SetName(Form("preparedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),0,y));
    statpreparedSpectrum->SetName(Form("statpreparedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),0,y));
    syspreparedSpectrum->SetName(Form("syspreparedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),0,y));

    //Loop to create combined spectra
    Int_t totalPoints = tpcSpectrum->GetN();
		Int_t jPoint=0;
    for (Int_t iPoint=0; iPoint<totalPoints; iPoint++){

      //Get all TPC Points
      if (iPoint < tpcSpectrum->GetN()){
			  
        Double_t mTm0TPC  = tpcSpectrum->GetX()[iPoint];
				Double_t yTPC = tpcSpectrum->GetY()[iPoint];
				Double_t yError = tpcSpectrum->GetEY()[iPoint];
				cout<<"i: "<<iPoint<<" x: "<<mTm0TPC<<" y: "<<yTPC<<endl;
				cout<<"jPoint: "<<jPoint<<endl;
				cout<<"yError: "<<tpcSpectrum->GetEY()[iPoint]<<endl;
				if (yError > 10 || TMath::IsNaN(yError)) continue;
        preparedSpectrum->SetPoint(jPoint,tpcSpectrum->GetX()[iPoint],tpcSpectrum->GetY()[iPoint]);
        preparedSpectrum->SetPointError(jPoint,tpcSpectrum->GetEX()[iPoint],tpcSpectrum->GetEY()[iPoint]);
        statpreparedSpectrum->SetPoint(jPoint,rawtpcSpectrum->GetX()[iPoint],rawtpcSpectrum->GetY()[iPoint]);
        statpreparedSpectrum->SetPointError(jPoint,rawtpcSpectrum->GetEX()[iPoint],rawtpcSpectrum->GetEY()[iPoint]);
        syspreparedSpectrum->SetPoint(jPoint,systpcSpectrum->GetX()[iPoint],systpcSpectrum->GetY()[iPoint]);
        syspreparedSpectrum->SetPointError(jPoint,systpcSpectrum->GetEX()[iPoint],systpcSpectrum->GetEY()[iPoint]);
        syspreparedSpectrum->SetPoint(jPoint,systpcSpectrum->GetX()[iPoint],systpcSpectrum->GetY()[iPoint]);
				++jPoint;
        continue;

      }

    }//End loop over total points

		preparedSpectrum->SetMarkerStyle(20);

		preparedSpectraFile->cd();
		preparedSpectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
		preparedSpectrum->Write();
		preparedSpectraFile->cd();
		preparedSpectraFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
		statpreparedSpectrum->Write();
		preparedSpectraFile->cd();
		preparedSpectraFile->cd(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
		syspreparedSpectrum->Write();

		preparedSpectrum->Delete();
		statpreparedSpectrum->Delete();
		syspreparedSpectrum->Delete();

	}//End loop over rapidity bins

}//End of fnxn

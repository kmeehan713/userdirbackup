#include <iostream>
#include <vector>
#include <utility>

#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TRandom3.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"

using namespace std;

Bool_t draw = false;
Double_t minTPCEfficiency=0.3;
Int_t pidIndex;
Int_t pidCharge;
Double_t pidMass;

//___FUNCTIONS THAT APPLY THE CORRECTIONS (DEFINED BELOW)________________
TGraphErrors *ApplyFiniteBinWidthCorrection(TGraphErrors *spectrum, TF1 *energyLossCurve, Bool_t doSystematics=false); //dmT Correction
TGraphErrors *ApplyEnergyLossCorrection(TGraphErrors *spectrum, TF1 *energyLossCurve, Bool_t doSystematics=false); //Correct the pT
TGraphErrors *ApplyEfficiencyCorrection(TGraphErrors *spectrum, TF1 *efficiencyCurve, Bool_t doSystematics=false); //Correct for tracking eff
TGraphErrors *ApplyKnockoutProtonCorrection(TGraphErrors *spectrum, TF1 *backgroundCurve, Bool_t doSystematics=false);

//___________________________________________________________
void TGraphChop(TGraphErrors *graph, Double_t threshold, Bool_t below){

  //Remove points from graph that are below (or above) the
  //the x value of threshold

  for (Int_t iPoint=graph->GetN()-1; iPoint>=0; iPoint--){

    //If the user wants to remove points above the threshold
    if (!below && graph->GetX()[iPoint] > threshold){
      graph->RemovePoint(iPoint);
    }   

    else if (below && graph->GetX()[iPoint] < threshold) {
      graph->RemovePoint(iPoint);
    }   

  }

}

//___________________________________________________________________________
void RemovePointsWithLargeErrors(TGraphErrors *spectrum, Double_t maxRelativeError=.1){

  //Loop Over the Points of the spectrum. Remove any point which is found
	//to have a relativeError larger than maxRelativeError
	for (int iPoint=spectrum->GetN()-1; iPoint>=0; iPoint--){
	
	    if (spectrum->GetEY()[iPoint] / spectrum->GetY()[iPoint] > maxRelativeError)
	          spectrum->RemovePoint(iPoint);
	}
}

//____MAIN_______________________________________________________________
void TempCorrectSpectra(TString spectraFileName, TString correctionFileName, Int_t pid, Int_t charge){

  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();
  pidIndex = pid;
  pidCharge = charge;
  pidMass = particleInfo->GetParticleMass(pid);
  
  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"UPDATE");
  spectraFile->cd();
  spectraFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  spectraFile->mkdir(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Open the Correction File
  TFile *corrFile = new TFile(correctionFileName,"READ");

  //Create a Canvas if drawing
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
  }

  //Local Pointers
  TF1 *energyLossCurve, *efficiencyCurve, *knockoutProtonCurve;
  TGraphErrors *tpcRawSpectrum, *tofRawSpectrum, *tpcSysErrSpectrum;
  TGraphErrors *tpcBackgroundCorrectedSpectrum, *tofBackgroundCorrectedSpectrum;
  TGraphErrors *tpcdmTCorrectedSpectrum, *tofdmTCorrectedSpectrum;
  TGraphErrors *tpcEnergyLossCorrectedSpectrum, *tofEnergyLossCorrectedSpectrum;
  TGraphErrors *tpcEfficiencyCorrectedSpectrum, *tofEfficiencyCorrectedSpectrum;
  TGraphErrors *tpcFullyCorrectedSpectrum, *tofFullyCorrectedSpectrum;

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

      cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
		  particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

      //Get the Correction Curves
      corrFile->cd();
/*      energyLossCurve = (TF1 *)corrFile->Get(Form("%s/EnergyLossFits/energyLossFit_%s_yIndex%d",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  particleInfo->GetParticleName(pid,charge).Data(),
							yIndex));
*/      efficiencyCurve = (TF1 *)corrFile->Get(Form("%s/EfficiencyFits/tpcEfficiencyFit_%s_Cent%d_yIndex%d",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin,yIndex));
/*      if (pid == PROTON){
	knockoutProtonCurve = (TF1 *)corrFile->Get(Form("%s/KnockoutBackgroundFits/knockoutProton_%d_%d_Fit;2",
							particleInfo->GetParticleName(pid,charge).Data(),
							iCentBin,yIndex));
      }
      else

	knockoutProtonCurve = NULL;
*/
      
      //Get the Spectra
      spectraFile->cd();
      spectraFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
      tpcRawSpectrum = (TGraphErrors *)gDirectory->Get(Form("rawSpectra_%s_Cent%02d_yIndex%02d",
							    particleInfo->GetParticleName(pid,charge).Data(),
							    iCentBin,yIndex));
      tpcSysErrSpectrum = new TGraphErrors();
		  
/*      tofRawSpectrum = (TGraphErrors *)gDirectory->Get(Form("rawSpectraTOF_%s_Cent%02d_yIndex%02d",
							    particleInfo->GetParticleName(pid,charge).Data(),
							    iCentBin,yIndex));
*/
      cout <<efficiencyCurve <<" " <<" " <<" " <<tpcRawSpectrum <<" " <<tofRawSpectrum <<" " <<endl;//add energyLossCurve and knockoutProtonCurve

			if (!efficiencyCurve){
				cout <<"    TPC Spectrum does not exist. Skipping this rapidity bin." <<endl;
				continue;
      }
      
      //Skip if the tpcSpectrum doesn't exist
      if (!tpcRawSpectrum){
				cout <<"    TPC Spectrum does not exist. Skipping this rapidity bin." <<endl;
				continue;
      }

      //Skip if the tpcSpectrum has no points
      if (tpcRawSpectrum->GetN() == 0){
	cout <<"    TPC Spectrum has no points. Skipping this rapidity bin." <<endl;
	continue;
      }
     
      if (draw){
				TH1F *frame = canvas->DrawFrame(0,.01,1.5,700);
				frame->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
								particleInfo->GetParticleSymbol(pid).Data()));
				frame->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{2}");
				frame->GetYaxis()->SetTitleOffset(1.2);

				canvas->SetRightMargin(.05);
				canvas->SetTopMargin(.05);
				canvas->SetLogy();
      }

      //----Knockout Proton Correction if this is a proton----
      //This correction should happen before the energy loss correction since
      //it is parameterized a function of the the measured momentum
/*    if (pid == PROTON && charge > 0){
				tpcBackgroundCorrectedSpectrum = ApplyKnockoutProtonCorrection(tpcRawSpectrum,knockoutProtonCurve);
				if (tofRawSpectrum)
							tofBackgroundCorrectedSpectrum = ApplyKnockoutProtonCorrection(tofRawSpectrum,knockoutProtonCurve);
      }
      else {
*/
				tpcBackgroundCorrectedSpectrum = new TGraphErrors(*tpcRawSpectrum);
		  	if (tofRawSpectrum) tofBackgroundCorrectedSpectrum = new TGraphErrors(*tofRawSpectrum);

//      }
      
      //----Finite Bin Width Correction (dmT)----
/*    tpcdmTCorrectedSpectrum = ApplyFiniteBinWidthCorrection(tpcBackgroundCorrectedSpectrum,energyLossCurve);
      if (tofRawSpectrum)
						 tofdmTCorrectedSpectrum = ApplyFiniteBinWidthCorrection(tofBackgroundCorrectedSpectrum,energyLossCurve);

      //----Energy Loss Correction----
      tpcEnergyLossCorrectedSpectrum = ApplyEnergyLossCorrection(tpcdmTCorrectedSpectrum,energyLossCurve);
      if (tofdmTCorrectedSpectrum)
						 tofEnergyLossCorrectedSpectrum = ApplyEnergyLossCorrection(tofdmTCorrectedSpectrum,energyLossCurve);
*/
       //----Efficiency Correction----
      tpcEfficiencyCorrectedSpectrum = ApplyEfficiencyCorrection(tpcBackgroundCorrectedSpectrum,efficiencyCurve);
//      if (tofEnergyLossCorrectedSpectrum)
      if (tofRawSpectrum)
			 			tofEfficiencyCorrectedSpectrum = ApplyEfficiencyCorrection(tofBackgroundCorrectedSpectrum,efficiencyCurve);

      //----Fully Corrected Spectra----
      tpcFullyCorrectedSpectrum = tpcEfficiencyCorrectedSpectrum;
      if (tofRawSpectrum) tofFullyCorrectedSpectrum = tofEfficiencyCorrectedSpectrum;

 
      //----Systematic Errors ----
      //Create n new spectra. The systematic error is then
      //computed as the RMS of the distribution of corrected yields at each point
      const int nSpectra(500);
      std::vector<TGraphErrors *> sysBackgroundCorrectedSpectra(nSpectra);
      std::vector<TGraphErrors *> sysdmTCorrectedSpectra(nSpectra);
      std::vector<TGraphErrors *> sysEnergyLossCorrectedSpectra(nSpectra);
      std::vector<TGraphErrors *> sysEfficiencyCorrectedSpectra(nSpectra);
      
      std::vector<TGraphErrors *> sysFullyCorrectedSpectra(nSpectra);
      
      std::vector<TGraphErrors *> sysFullyCorrectedSpectraTOF(nSpectra);
      std::vector<TGraphErrors *> sysEfficiencyCorrectedSpectraTOF(nSpectra);

      //TPC
      for (int iSpectra=0; iSpectra<nSpectra; iSpectra++){
	
				/*if (pid == PROTON && charge > 0)
							sysBackgroundCorrectedSpectra.at(iSpectra) =
					  	ApplyKnockoutProtonCorrection(tpcRawSpectrum,knockoutProtonCurve,true);
				*/
				//else
					sysBackgroundCorrectedSpectra.at(iSpectra) = new TGraphErrors(*tpcRawSpectrum);
				
				//sysdmTCorrectedSpectra.at(iSpectra) =
				//	ApplyFiniteBinWidthCorrection(sysBackgroundCorrectedSpectra.at(iSpectra),energyLossCurve,true);
				
				//sysEnergyLossCorrectedSpectra.at(iSpectra) =
					//ApplyEnergyLossCorrection(sysdmTCorrectedSpectra.at(iSpectra),energyLossCurve,true);
				
				sysEfficiencyCorrectedSpectra.at(iSpectra) =
					ApplyEfficiencyCorrection(sysBackgroundCorrectedSpectra.at(iSpectra),efficiencyCurve,true);

				sysFullyCorrectedSpectra.at(iSpectra) = sysEfficiencyCorrectedSpectra.at(iSpectra);
				
				delete sysBackgroundCorrectedSpectra.at(iSpectra);
				//delete sysdmTCorrectedSpectra.at(iSpectra);
				//delete sysEnergyLossCorrectedSpectra.at(iSpectra);
      }
      
      //For each point in the spectrum, combine the statistical error in quadrature with the systematic Error
			//Set up spectra that will carry the systematic-only errors for drawing purposes in later macro

      for (int iPoint=0; iPoint<tpcFullyCorrectedSpectrum->GetN(); iPoint++){
	
				Double_t yErrStat = tpcFullyCorrectedSpectrum->GetEY()[iPoint];
				Double_t xErrStat = tpcFullyCorrectedSpectrum->GetEX()[iPoint];

				Double_t yVal = tpcFullyCorrectedSpectrum->GetY()[iPoint];
				Double_t xVal = tpcFullyCorrectedSpectrum->GetX()[iPoint];

				std::vector<double> ySysVec(nSpectra);
				std::vector<double> xSysVec(nSpectra);
				for (unsigned int iSpectra=0; iSpectra<nSpectra; iSpectra++){
					ySysVec.at(iSpectra) = sysFullyCorrectedSpectra.at(iSpectra)->GetY()[iPoint];
					xSysVec.at(iSpectra) = sysFullyCorrectedSpectra.at(iSpectra)->GetX()[iPoint];
				}

				Double_t yErrSys = TMath::RMS(nSpectra,&ySysVec.at(0));
				Double_t xErrSys = TMath::RMS(nSpectra,&xSysVec.at(0));

				Double_t yTotalError = TMath::Sqrt(pow(yErrStat,2) + pow(yErrSys,2));
				Double_t xTotalError = xErrStat;//TMath::Sqrt(pow(xErrStat,2) + pow(xErrSys,2));
				
				tpcFullyCorrectedSpectrum->SetPointError(iPoint,xTotalError,yTotalError);
	  
			  tpcSysErrSpectrum->SetPoint(iPoint,xVal,yVal);
			  tpcSysErrSpectrum->SetPointError(iPoint,xErrSys,yErrSys);

      }

      //TOF
      for (int iSpectra=0; iSpectra<nSpectra; iSpectra++){
			  if (!tofRawSpectrum) break;
/*
				if (pid == PROTON && charge > 0)
							sysBackgroundCorrectedSpectra.at(iSpectra) =
							ApplyKnockoutProtonCorrection(tofRawSpectrum,knockoutProtonCurve,true);
				else
*/							sysBackgroundCorrectedSpectra.at(iSpectra) = new TGraphErrors(*tofRawSpectrum);
				
/*				sysdmTCorrectedSpectra.at(iSpectra) =
							ApplyFiniteBinWidthCorrection(sysBackgroundCorrectedSpectra.at(iSpectra),energyLossCurve,true);
				sysEnergyLossCorrectedSpectra.at(iSpectra) =
							ApplyEnergyLossCorrection(sysdmTCorrectedSpectra.at(iSpectra),energyLossCurve,true);
*/				sysEfficiencyCorrectedSpectraTOF.at(iSpectra) =
							ApplyEfficiencyCorrection(sysBackgroundCorrectedSpectra.at(iSpectra),efficiencyCurve,true);

				sysFullyCorrectedSpectraTOF.at(iSpectra) = sysEfficiencyCorrectedSpectraTOF.at(iSpectra);

				delete sysBackgroundCorrectedSpectra.at(iSpectra);
				//delete sysdmTCorrectedSpectra.at(iSpectra);
				//delete sysEnergyLossCorrectedSpectra.at(iSpectra);
				
      }
      
      //For each point in the spectrum, combine the statistical error in quadrature with the systematic Error
      for (int iPoint=0; iPoint<tofFullyCorrectedSpectrum->GetN(); iPoint++){
	
			  if (!tofRawSpectrum) break;
	      
				Double_t yErrStat = tofFullyCorrectedSpectrum->GetEY()[iPoint];
				Double_t xErrStat = tofFullyCorrectedSpectrum->GetEX()[iPoint];

				std::vector<double> ySysVec(nSpectra);
				std::vector<double> xSysVec(nSpectra);
				for (unsigned int iSpectra=0; iSpectra<nSpectra; iSpectra++){
					ySysVec.at(iSpectra) = sysFullyCorrectedSpectraTOF.at(iSpectra)->GetY()[iPoint];
					xSysVec.at(iSpectra) = sysFullyCorrectedSpectraTOF.at(iSpectra)->GetX()[iPoint];
				}

				Double_t yErrSys = TMath::RMS(nSpectra,&ySysVec.at(0));
				Double_t xErrSys = TMath::RMS(nSpectra,&xSysVec.at(0));

				Double_t yTotalError = TMath::Sqrt(pow(yErrStat,2) + pow(yErrSys,2));
				Double_t xTotalError = xErrStat;//TMath::Sqrt(pow(xErrStat,2) + pow(xErrSys,2));
				
				tofFullyCorrectedSpectrum->SetPointError(iPoint,xTotalError,yTotalError);
	  
      }
      
		  RemovePointsWithLargeErrors(tpcFullyCorrectedSpectrum);	
      TGraphChop(tpcFullyCorrectedSpectrum,0.08,true);

      //Draw
      if (draw){
	
				for (unsigned int i=0; i<nSpectra; i++){
					sysEfficiencyCorrectedSpectra.at(i)->SetMarkerColor(kBlack);
					sysEfficiencyCorrectedSpectra.at(i)->SetMarkerStyle(kFullTriangleUp);
					sysEfficiencyCorrectedSpectra.at(i)->Draw("P");
					
					if (tofRawSpectrum){
						sysEfficiencyCorrectedSpectraTOF.at(i)->SetMarkerColor(kBlack);
						sysEfficiencyCorrectedSpectraTOF.at(i)->SetMarkerStyle(kFullTriangleUp);
						sysEfficiencyCorrectedSpectraTOF.at(i)->Draw("P");
					}
				}
				
			  /*	
				tpcdmTCorrectedSpectrum->Draw("P");
				if (tofdmTCorrectedSpectrum)
					tofdmTCorrectedSpectrum->Draw("P");
				tpcEnergyLossCorrectedSpectrum->Draw("P");
				if (tofEnergyLossCorrectedSpectrum)
					tofEnergyLossCorrectedSpectrum->Draw("P");
			  */
				tpcEfficiencyCorrectedSpectrum->SetMarkerStyle(kFullCircle);
				if (tofRawSpectrum) tofEfficiencyCorrectedSpectrum->SetMarkerStyle(kFullSquare);
				
				tpcEfficiencyCorrectedSpectrum->Draw("PZ");
				if (tofEfficiencyCorrectedSpectrum)
					tofEfficiencyCorrectedSpectrum->Draw("PZ");

				canvas->Update();
				gSystem->Sleep(2000);

      }


      
      //Set Names and Titles
      tpcFullyCorrectedSpectrum->SetName(Form("correctedSpectra_%s_Cent%02d_yIndex%02d",
					     particleInfo->GetParticleName(pid,charge).Data(),
					     iCentBin,yIndex));
      tpcSysErrSpectrum->SetName(Form("correctedSysErrSpectra_%s_Cent%02d_yIndex%02d",
					     particleInfo->GetParticleName(pid,charge).Data(),
					     iCentBin,yIndex));

      if (tofRawSpectrum) tofFullyCorrectedSpectrum->SetName(Form("correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
					      particleInfo->GetParticleName(pid,charge).Data(),
					      iCentBin,yIndex));
      
      tpcFullyCorrectedSpectrum->SetTitle(Form("Corrected TPC Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					       particleInfo->GetParticleSymbol(pidIndex,charge).Data(),
					       iCentBin!=0 ? (int)GetCentralityPercents().at(iCentBin-1):0,
					       (int)GetCentralityPercents().at(iCentBin),
					       particleInfo->GetParticleSymbol(pidIndex).Data(),
					       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					       particleInfo->GetParticleSymbol(pidIndex).Data()));
      if (tofRawSpectrum) tofFullyCorrectedSpectrum->SetTitle(Form("Corrected TOF Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					       particleInfo->GetParticleSymbol(pidIndex,charge).Data(),
					       iCentBin!=nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					       (int)GetCentralityPercents().at(iCentBin),
					       particleInfo->GetParticleSymbol(pidIndex).Data(),
					       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					       particleInfo->GetParticleSymbol(pidIndex).Data()));

      //Save
      spectraFile->cd();
      spectraFile->cd(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
      tpcSysErrSpectrum->Write();
      spectraFile->cd();
      spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
      tpcFullyCorrectedSpectrum->Write();
      if (tofRawSpectrum) tofFullyCorrectedSpectrum->Write();

      //Clean Up
      if (tpcRawSpectrum)
	delete tpcRawSpectrum;
     if (tofRawSpectrum)
	delete tofRawSpectrum;
/*      if (energyLossCurve)
	delete energyLossCurve;
*/      if (efficiencyCurve)
	delete efficiencyCurve;
      if (tpcBackgroundCorrectedSpectrum)
	delete tpcBackgroundCorrectedSpectrum;
      if (tofRawSpectrum && tofBackgroundCorrectedSpectrum)
	delete tofBackgroundCorrectedSpectrum;
/*      if (tpcdmTCorrectedSpectrum)
	delete tpcdmTCorrectedSpectrum;
      if (tofdmTCorrectedSpectrum)
	delete tofdmTCorrectedSpectrum;
      if (tpcEnergyLossCorrectedSpectrum)
	delete tpcEnergyLossCorrectedSpectrum;
      if (tofEnergyLossCorrectedSpectrum)
	delete tofEnergyLossCorrectedSpectrum;
*/      if (tpcEfficiencyCorrectedSpectrum)
	delete tpcEfficiencyCorrectedSpectrum;
      if (tofRawSpectrum && tofEfficiencyCorrectedSpectrum)
	delete tofEfficiencyCorrectedSpectrum;
    }    

  }
  
}//END MAIN


//_________________________________________________________________
TGraphErrors *ApplyFiniteBinWidthCorrection(TGraphErrors *spectrum, TF1 *energyLossCurve, Bool_t doSystematics){

  //Loop Over the points of the spectrum and apply the Finite Bin Width (dmT) correction.
  //Return a new spectrum
  TGraphErrors *corrSpectrum = new TGraphErrors();
  for (int iPoint=0; iPoint<spectrum->GetN(); iPoint++){

    //Get the point info
    Double_t mTm0  = spectrum->GetX()[iPoint];
    Double_t yield = spectrum->GetY()[iPoint];
    Double_t xErr  = spectrum->GetEX()[iPoint];
    Double_t yErr  = spectrum->GetEY()[iPoint];

    //Compute the shift for the Lower and Upper Edges of the mTm0 Bin
    //keeping in mind that the energy loss curve is in pT and the
    //spectra is in mT-m0...so we need to convert everything first to pT.
    
    //Original Bin Values
    Double_t originalBinLowEdge = ConvertmTm0ToPt(GetmTm0RangeLow(GetmTm0Index(mTm0)),pidMass);   //In pT
    Double_t originalBinHighEdge = ConvertmTm0ToPt(GetmTm0RangeHigh(GetmTm0Index(mTm0)),pidMass); //In pT
    Double_t originalBinWidth = originalBinHighEdge - originalBinLowEdge;                         //In pT

    //Make sure the bin Center is in the range of the energy loss fit
    //if not then don't add a point to the corrected spectrum
    Double_t minFitRange(0), maxFitRange(0);
    energyLossCurve->GetRange(minFitRange,maxFitRange);
    Double_t originalBinCenter = originalBinLowEdge + originalBinWidth/2.0;
    if (originalBinCenter < minFitRange || originalBinCenter > maxFitRange)
      continue;
    
    //Compute the Shift. By default the energy loss curve
    //is evaluated at the original bin corrdinates. Otherwise a random
    //shift is computed for the systematic error determination.
    Double_t lowEdgeShift = energyLossCurve->Eval(originalBinLowEdge);
    Double_t highEdgeShift = energyLossCurve->Eval(originalBinHighEdge);
    /*
    if (doSystematics){
      TRandom3 rand(0);
      Double_t randomBinWidthAdj = rand.Gaus(0,originalBinWidth/2.0);
      lowEdgeShift = energyLossCurve->Eval(originalBinWidth+randomBinWidthAdj);
      highEdgeShift = energyLossCurve->Eval(originalBinWidth+randomBinWidthAdj);
    }
    */
    
    //Compute the New Bin Edges by applying the shift
    Double_t correctedBinLowEdge = originalBinLowEdge - lowEdgeShift;
    Double_t correctedBinHighEdge = originalBinHighEdge - highEdgeShift;
    Double_t correctedBinWidth = correctedBinHighEdge - correctedBinLowEdge; //Corrected Bin Width in pT
    Double_t correctedBinWidthmTm0 = ComputemTm0(correctedBinHighEdge,pidMass) -
      ComputemTm0(correctedBinLowEdge,pidMass); //Corrected Bin Width in mT-m0
    
    //Compute the Correction Factor
    Double_t correctionFactor = originalBinWidth/correctedBinWidth; //pT Cancels in ratio
						   
    //Apply the dmT Correction
    corrSpectrum->SetPoint(corrSpectrum->GetN(),mTm0,yield*correctionFactor);
    corrSpectrum->SetPointError(corrSpectrum->GetN()-1,correctedBinWidthmTm0/2.0,yErr*correctionFactor);

  }//End Loop Over Points

  corrSpectrum->SetMarkerStyle(spectrum->GetMarkerStyle());
  corrSpectrum->SetMarkerColor(spectrum->GetMarkerColor());
  
  return corrSpectrum;
  
}


//_________________________________________________________________
TGraphErrors *ApplyEnergyLossCorrection(TGraphErrors *spectrum, TF1 *energyLossCurve, Bool_t doSystematics){

  //Apply the correction to the measured momentum and return a new spectrum
  TGraphErrors *corrSpectrum = new TGraphErrors();
  for (int iPoint=0; iPoint<spectrum->GetN(); iPoint++){

    //Get the point info
    Double_t mTm0  = spectrum->GetX()[iPoint];
    Double_t yield = spectrum->GetY()[iPoint];
    Double_t xErr  = spectrum->GetEX()[iPoint];
    Double_t yErr  = spectrum->GetEY()[iPoint];

    //Find the Shift by evaluating the Energy Loss Curve
    //keeping in mind that the energy loss curve is in pT and the
    //spectra is in mT-m0...so we need to convert
    Double_t pTOfPoint = ConvertmTm0ToPt(mTm0,pidMass);

    //Make sure the pT of the point is in the energy Loss fit range
    //if not then do not add a point to the corrected spectrum
    Double_t minFitRange(0), maxFitRange(0);
    energyLossCurve->GetRange(minFitRange,maxFitRange);
    if (pTOfPoint < minFitRange || pTOfPoint > maxFitRange)
      continue;

    //Compute the correction shift. By default  the energy
    //loss curve is evaluated at the pT of the point. Otherwise a
    //random pT in the bin is chosen for systematic error determination.
    Double_t correctionShift = energyLossCurve->Eval(pTOfPoint);
    if (doSystematics){

      Double_t pTLowEdge = ConvertmTm0ToPt(GetmTm0RangeLow(GetmTm0Index(mTm0)),pidMass);
      Double_t pTHighEdge = ConvertmTm0ToPt(GetmTm0RangeHigh(GetmTm0Index(mTm0)),pidMass);
      
      TRandom3 rand(0);
      Double_t randomPtInBin = rand.Uniform(pTLowEdge,pTHighEdge);
      correctionShift = energyLossCurve->Eval(randomPtInBin);
    }
    
    //Apply the Correction and convert back to mTm0
    Double_t correctedpT = pTOfPoint - correctionShift;
    Double_t correctedmTm0 = ComputemTm0(correctedpT,pidMass);

    //Apply the correction, this correction does not effect the errors
    //so they are propagated through
    corrSpectrum->SetPoint(corrSpectrum->GetN(),correctedmTm0,yield);
    corrSpectrum->SetPointError(corrSpectrum->GetN()-1,xErr,yErr);

  }

  corrSpectrum->SetMarkerStyle(spectrum->GetMarkerStyle());
  corrSpectrum->SetMarkerColor(spectrum->GetMarkerColor());

  return corrSpectrum;
}

//_________________________________________________________________
TGraphErrors *ApplyEfficiencyCorrection(TGraphErrors *spectrum, TF1 *efficiencyCurve, Bool_t doSystematics){

  //Apply the TPC Tracking Efficiency Correction
  //and return a new spectrum
  TGraphErrors *corrSpectrum = new TGraphErrors();

  //Create a copy of the efficiency curve in case it is needed
  //to do the systematic errors
  TF1 tempFunc(*efficiencyCurve);
  if (pidIndex !=2 && doSystematics){

    Bool_t isGoodCurve = false;
    while (!isGoodCurve){
      TRandom rand(0);
      tempFunc.SetParameter(0,rand.Gaus(efficiencyCurve->GetParameter(0),efficiencyCurve->GetParError(0)));
      tempFunc.SetParameter(1,rand.Gaus(efficiencyCurve->GetParameter(1),efficiencyCurve->GetParError(1)));
      tempFunc.SetParameter(2,rand.Gaus(efficiencyCurve->GetParameter(2),efficiencyCurve->GetParError(2)));

      //Check the end points to make sure they are within[0,1]
      Double_t min(0),max(0);
      tempFunc.GetRange(min,max);
      if (tempFunc.Eval(min) < 0 || tempFunc.Eval(max) < 0 ||
	  tempFunc.Eval(min) > 1 || tempFunc.Eval(max) > 1)
	isGoodCurve = false;
      else
	isGoodCurve = true;
    }//End Check for good curve

  }//End If Systematics
  
  //Loop Over the Points
  for (int iPoint=0; iPoint<spectrum->GetN(); iPoint++){

    //Get the point info
    Double_t mTm0  = spectrum->GetX()[iPoint];
    Double_t yield = spectrum->GetY()[iPoint];
    Double_t xErr  = spectrum->GetEX()[iPoint];
    Double_t yErr  = spectrum->GetEY()[iPoint]; 

    //Get the Efficiency Correction Factor. By default the correction factor
    //is determined by evaluating the efficiency curve. Otherwise, the parameters
    //of the curve are varied (see above) to determine the systematic error.
    Double_t effCorrFactor = efficiencyCurve->Eval(mTm0);

    //Make sure the TPC tracking efficiency is above the minimum, if not
    //do not add a point to the corrected spectrum
    if (effCorrFactor < minTPCEfficiency)
      continue;    

    //If Systematic Errors are requested
    if (pidIndex !=2 && doSystematics){

      effCorrFactor = tempFunc.Eval(mTm0);
      
    }
    
    //Apply the correction
    corrSpectrum->SetPoint(corrSpectrum->GetN(),mTm0,yield/effCorrFactor);
    corrSpectrum->SetPointError(corrSpectrum->GetN()-1,xErr,yErr/effCorrFactor);
    
  }//End Loop Over Points

  corrSpectrum->SetMarkerStyle(spectrum->GetMarkerStyle());
  corrSpectrum->SetMarkerColor(spectrum->GetMarkerColor());
  
  return corrSpectrum;
  
}

//__________________________________________________________________________________
TGraphErrors *ApplyKnockoutProtonCorrection(TGraphErrors *spectrum, TF1 *backgroundCurve, Bool_t doSystematics){

  //Apply the Knockout Proton Correction and return a new spectrum
  TGraphErrors *corrSpectrum = new TGraphErrors();

  for (int iPoint=0; iPoint<spectrum->GetN(); iPoint++){

    //Get the point info
    Double_t mTm0  = spectrum->GetX()[iPoint];
    Double_t yield = spectrum->GetY()[iPoint];
    Double_t xErr  = spectrum->GetEX()[iPoint];
    Double_t yErr  = spectrum->GetEY()[iPoint];

    //Find the Background fraction by evaluating the backgroundCurve
    Double_t backgroundFraction = backgroundCurve->Eval(mTm0);

    //If we are doing the systematics then evaluate the curve
    //at a random location within the bin
    if (doSystematics){

      TRandom3 rand(0);
      Double_t randommTm0 = rand.Uniform(mTm0-xErr,mTm0+xErr);
      backgroundFraction = backgroundCurve->Eval(randommTm0);

    }//End Do Systematics

    //Compute the Correction Factor
    Double_t correctionFactor = 1.0 - backgroundFraction;

    //Make sure the correction factor is good
    //if bad then set it to 1 so no change will be made
    if (TMath::IsNaN(correctionFactor))
      correctionFactor = 1.0;

    //Apply the Correction
    Double_t correctedYield = yield * correctionFactor;
    Double_t correctedError = yErr * correctionFactor;

    //Set the Point in the corrected Spectrum
    corrSpectrum->SetPoint(corrSpectrum->GetN(),mTm0,correctedYield);
    corrSpectrum->SetPointError(corrSpectrum->GetN()-1,xErr,correctedError);

  }//End Loop Over Points

  corrSpectrum->SetMarkerStyle(spectrum->GetMarkerStyle());
  corrSpectrum->SetMarkerColor(spectrum->GetMarkerColor());

  return corrSpectrum;

}

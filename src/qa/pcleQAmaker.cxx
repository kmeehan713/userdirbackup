#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

//after- verify that you need all these classes
#include "../submodules/datacollectorreaderlibs/TrackInfo/TrackInfo.h"
#include "../submodules/datacollectorreaderlibs/PrimaryVertexInfo/PrimaryVertexInfo.h"
#include "../submodules/datacollectorreaderlibs/EventInfo/EventInfo.h"
#include "../submodules/datacollectorreaderlibs/DavisDstReader/DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void pcleQAmaker(TString inputDataFile, TString outputFile, Int_t nEvents = -1, Bool_t makeTOFplots=0){
//This function takes your input data file and produces an output file with vertex qa plots
//with no vertex cuts or with vertex-level cuts. Note that no track qa plots are made 
//you must use trackQAmaker.cxx for track qa. The purpose of this function is to allow 
//the user to optimize vertex cuts.

DavisDstReader davisDst(inputDataFile);

TFile *outFile  = new TFile(outputFile,"RECREATE");

TrackInfo *track = NULL;
PrimaryVertexInfo *primaryVertex = NULL;
EventInfo *event = NULL;

std::vector<int> centCuts = GetCentralityCuts();
const int NYBINS = 20;

//dEdx info
TH2D *dEdxMtm0HistPiAll = new TH2D("dEdxMtm0HistPiAll","dEdxMtm0HistPiAll",300,-1.5,1.5,1000,0,10);
TH2D *dEdxMtm0HistPiTofImprovedAll = new TH2D("dEdxMtm0HistPiTofImprovedAll","dEdxMtm0HistPiTofImprovedAll",300,-1.5,1.5,1000,0,10);
TH2D *dEdxMtm0HistPi_KFocusedAll = new TH2D("dEdxMtm0HistPi_KFocusedAll","dEdxMtm0HistPi_KFocusedAll",300,-1.5,1.5,1000,0,10);

TH2D* hdEdxmTm0Pro[NYBINS];
TH2D* hdEdxmTm0Pi[NYBINS];
TH2D* hdEdxmTm0K[NYBINS];

//TOF info
TH2D* hInvBetamTm0Pro[NYBINS];
TH2D* hInvBetamTm0Pi[NYBINS];
TH2D* hInvBetamTm0K[NYBINS];

//dEdx info using tof selection to purify piplus
TH2D *hdEdxmTm0PiTofImproved[NYBINS];//tracks that match tof in e, k, p bands are excluded (pi phase space)
TH2D *hdEdxmTm0Pi_KFocused[NYBINS];//tracks that match tof in e, pi, p bands are excluded (pi phase space)

for (int i=0; i<NYBINS; i++){
  hdEdxmTm0Pro[i] = new TH2D(Form("dEdxMtm0HistPro_%d",i),Form("dEdxMtm0HistPro_%d",i),300,-1.5,1.5,1000,0,10);
  hdEdxmTm0Pi[i] = new TH2D(Form("dEdxMtm0HistPi_%d",i),Form("dEdxMtm0HistPi_%d",i),300,-1.5,1.5,1000,0,10);
  hdEdxmTm0K[i] = new TH2D(Form("dEdxMtm0HistK_%d",i),Form("dEdxMtm0HistK_%d",i),300,-1.5,1.5,1000,0,10);
	hdEdxmTm0PiTofImproved[i] = new TH2D(Form("dEdxMtm0HistPiTofImproved_%d",i),Form("dEdxMtm0HistPiTofImproved_%d",i),300,-1.5,1.5,1000,0,10);
	hdEdxmTm0Pi_KFocused[i] = new TH2D(Form("dEdxMtm0HistPi_KFocused_%d",i),Form("dEdxMtm0HistPi_KFocused_%d",i),300,-1.5,1.5,1000,0,10);

	if (!makeTOFplots) continue;

	hInvBetamTm0Pro[i] = new TH2D(Form("InvBetaMtm0HistPro_%d",i),Form("InvBetaMtm0HistPro_%d",i),300,-1.5,1.5,500,0,5);
	hInvBetamTm0Pi[i] = new TH2D(Form("InvBetaMtm0HistPi_%d",i),Form("InvBetaMtm0HistPi_%d",i),300,-1.5,1.5,500,0,5);
	hInvBetamTm0K[i] = new TH2D(Form("InvBetaMtm0HistK_%d",i),Form("InvBetaMtm0HistK_%d",i),300,-1.5,1.5,500,0,5);
}


Int_t pvEntries, nPrimaryTracks, nGoodTracks;
Double_t mPion = 0.13957018;
Double_t mProton = 0.938272046;
Double_t mKaon = 0.493677;
Double_t entries, ymin, ymax;
if(nEvents > 0) entries = nEvents;
else entries = davisDst.GetEntries();
for(Int_t i=0; i<entries; i++){//loop over triggers
	event = davisDst.GetEntry(i);
	if (!IsGoodEvent(event)) continue;
	pvEntries = event->GetNPrimaryVertices();
  for (Int_t j=0; j<pvEntries; j++){//loop over vertices
	  primaryVertex = event->GetPrimaryVertex(j);
    if (!IsGoodVertex(primaryVertex)) continue;
    
		//centrality selection
		nGoodTracks = GetUserCentralityVariable(primaryVertex);
		if (nGoodTracks < centCuts.at(0) || nGoodTracks > 240) continue;

		nPrimaryTracks = primaryVertex->GetNPrimaryTracks();
		for(Int_t k = 0; k<nPrimaryTracks;k++){//track loop 
			track = primaryVertex->GetPrimaryTrack(k);
			if (!IsGoodTrack(track)) continue;
			Double_t eta       = track->GetEta();
			Double_t phi       = track->GetPhi();
			Double_t q = track->GetCharge();
			Double_t pT = track->GetPt();
			Double_t p = sqrt(pow(pT,2) + pow(track->GetPz(),2));
			Double_t dEdx = track->GetdEdx(1)*1000000;
			Double_t invBeta = 1.0/track->GetTofBeta();
			Double_t yPi = TMath::ATanH(track->GetPz() / sqrt(mPion*mPion + p*p));
			Double_t yPro = TMath::ATanH(track->GetPz() / sqrt(mProton*mProton + p*p));
			Double_t yK   = TMath::ATanH(track->GetPz() / sqrt(mKaon*mKaon + p*p));
			Double_t mTpro = sqrt(mProton*mProton + pT*pT);
			Double_t mTK = sqrt(mKaon*mKaon + pT*pT);
			Double_t mTm0  = sqrt(mPion*mPion + pT*pT) - mPion;
			Double_t mTm0Pro = mTpro - mProton;
			Double_t mTm0K = mTK - mKaon;
			Double_t nSigmaTofPi = track->GetTofNSigmaPion();
			Double_t nSigmaTofK = track->GetTofNSigmaKaon();
			Double_t nSigmaTofE = track->GetTofNSigmaElectron();
			Double_t nSigmaTofPro = track->GetTofNSigmaProton();
			dEdxMtm0HistPiAll->Fill(mTm0/q, dEdx);
			if (fabs(nSigmaTofK) > 3 && fabs(nSigmaTofE) > 3 && fabs(nSigmaTofPro) > 3)
				dEdxMtm0HistPiTofImprovedAll->Fill(mTm0/q, dEdx);
			if (fabs(nSigmaTofPi) > 2 && fabs(nSigmaTofE) > 2 && fabs(nSigmaTofPro) > 2)
				dEdxMtm0HistPi_KFocusedAll->Fill(mTm0/q, dEdx);

			for (Int_t ybin=0;ybin<NYBINS;ybin++){
				ymin = -2.05 + ybin*0.1;   
				ymax = ymin + 0.1;
				if (yPro > ymin && yPro < ymax) hdEdxmTm0Pro[ybin]->Fill(mTm0Pro/q, dEdx);
				if (yPi  > ymin && yPi  < ymax){
					hdEdxmTm0Pi[ybin]->Fill(mTm0/q, dEdx);
			    if (fabs(nSigmaTofK) > 3 && fabs(nSigmaTofE) > 3 && fabs(nSigmaTofPro) > 3)
				 	  hdEdxmTm0PiTofImproved[ybin]->Fill(mTm0/q, dEdx);
			    if (fabs(nSigmaTofPi) > 2 && fabs(nSigmaTofE) > 2 && fabs(nSigmaTofPro) > 2)
				 	  hdEdxmTm0Pi_KFocused[ybin]->Fill(mTm0/q, dEdx);
				}
				if (yK   > ymin && yK   < ymax) hdEdxmTm0K[ybin]->Fill(mTm0K/q, dEdx);

				
				if (!IsGoodTofTrack(track)) continue;
				if (yPro > ymin && yPro < ymax) hInvBetamTm0Pro[ybin]->Fill(mTm0Pro/q, invBeta);
				if (yPi  > ymin && yPi  < ymax) hInvBetamTm0Pi[ybin]->Fill(mTm0/q, invBeta);
				if (yK   > ymin && yK   < ymax) hInvBetamTm0K[ybin]->Fill(mTm0K/q, invBeta);

			}


	  }//end loop over tracks
 }//end of loop over vertices
}//end of loop over triggers

outFile->Write();

}//end of function

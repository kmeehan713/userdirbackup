#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

//Draw Option
Bool_t draw(false);

//Functions Defined Below
Double_t FindMinFitRange(TGraphAsymmErrors *g,Double_t minEff=.1);

void FitTofMatchEfficiencyGraphs(TString file){

  //*************************
  //This variable is the minimum range of the tof efficiency fit unless,
  //the mTm0 value at which is reaches a minimum efficiency is greater than it.
  //It puts a lower bound on how low in mT-m0 you can extract tof spectra.
  const double minTofFit = 0.350;
  //**************************
  gStyle->SetOptFit(1);
  
  TFile *tofFile = new TFile(file,"UPDATE");
  tofFile->mkdir("FitTofEfficiency");
  tofFile->mkdir("TofEfficiencyCurves");
  
  TCanvas *canvas = NULL;
  if (draw)
    canvas = new TCanvas("canvas","canvas",20,20,800,600);

  ParticleInfo *particleInfo = new ParticleInfo();

  const int nCentralityBins = GetNCentralityBins();
  const int nParticles = particleInfo->GetNumberOfDefinedParticles();

  TGraphAsymmErrors *tofGraph = NULL;

  TF1 *fitFunc = NULL; //pointer to current function

  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int iParticle=0; iParticle<nParticles; iParticle++){

      //Only Do this for Pions, Kaons, and Protons
      if (iParticle != PION && iParticle != KAON && iParticle != PROTON)
	continue;
      
      for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

	tofFile->cd();
	tofFile->cd("TofEfficiency");
	tofGraph = (TGraphAsymmErrors *)gDirectory->
	  Get(Form("tofMatchingEfficiency_Cent%d_%s_%d",
		   iCentBin,
		   particleInfo->GetParticleName(iParticle).Data(),
		   yIndex));

	//Make Sure the graph exists
	if (!tofGraph)
	  continue;

	//Make sure the graph has a min number of points
	if (tofGraph->GetN() < 10)
	  continue;

	//Get the Maximum Y Value of the graph and make sure
	//that it is at least 30%
	if (TMath::MaxElement(tofGraph->GetN(),tofGraph->GetY()) < .3)
	  continue;

	//Make sure that the graph reaches 30% before mT-m0=.8
	bool reaches30=false;
	for (int i=0; i<tofGraph->GetN(); i++){

	  if (tofGraph->GetY()[i] > .3 && tofGraph->GetX()[i] < .8){
	    reaches30=true;
	    break;
	  }	  
	}
	if (!reaches30)
	  continue;

	//Draw
	if (draw){
	  canvas->DrawFrame(0,0,2,1.2);
	  tofGraph->Draw("PZ");
	}

	//Find the Point at which the graph exceeds a minEfficiency
	Double_t threshold = FindMinFitRange(tofGraph);	
	Double_t minFitRange = TMath::Max(minTofFit,threshold);

	//Possible Fitting Functions
	fitFunc = NULL;
	TF1 constFit("","pol0",minFitRange,2.0);
	TF1 fitExp1("","[0]*exp(-[1]/pow(x,[2]))",minFitRange,2.0);
	TF1 fitExp2("","[0]*exp(-[1]/pow(x,[2]))+[3]",minFitRange,2.0);

	//Try the constant, if it works then use it, otherwise try another
	tofGraph->Fit(&constFit,"RNQ");
	if (fabs(constFit.GetChisquare()/(double)constFit.GetNDF()) <= 1.3){

	  fitFunc = &constFit;
	  
	}

	if (!fitFunc){

	  fitExp1.SetParameters(16,0.005,2);
	  fitExp1.SetParLimits(0,0,75);
	  fitExp1.SetParLimits(1,0,0.05);
	  fitExp1.SetParLimits(2,1,3);
	  
	  tofGraph->Fit(&fitExp1,"RNQ");
	  if (fabs(fitExp1.GetChisquare()/(double)fitExp1.GetNDF()) <= 1.3)
	    fitFunc = &fitExp1;
	  
	}

	if (!fitFunc){

	  fitExp2.SetParameters(16,0.005,2,-15);
	  fitExp2.SetParLimits(0,0,50);
	  fitExp2.SetParLimits(1,0,0.05);
	  fitExp2.SetParLimits(2,1,3);
	  fitExp2.SetParLimits(3,-30,0);
	  
	  tofGraph->Fit(&fitExp2,"RNQ");
	  
	  fitFunc = &fitExp2;
	  
	}

	//Fit Properties
	fitFunc->SetName(Form("tofMatchingEfficiencyCurve_Cent%d_%s_%d",
			      iCentBin,
			      particleInfo->GetParticleName(iParticle).Data(),
			      yIndex));
	fitFunc->SetLineColor(particleInfo->GetParticleColor(iParticle));
	fitFunc->SetLineWidth(3);


	if (draw){
	  fitFunc->Draw("SAME");
	  canvas->Update();
	  gSystem->Sleep(1000);
	}

	tofGraph->GetListOfFunctions()->Add(fitFunc);
	
	tofFile->cd();
	tofFile->cd("FitTofEfficiency");
	tofGraph->Write();

	tofFile->cd();
	tofFile->cd("TofEfficiencyCurves");
	fitFunc->Write();
	


      }//End Loop Over Rapidity Bins
    }//End Loop Over Particles
  }//End Loop Over Cent bins

}


//_______________________________________________________________
Double_t FindMinFitRange(TGraphAsymmErrors *g,Double_t minEff){

  //Find the lower range of the fit which is the mTm0 value where
  //the efficiency first reaches above minEff

  Double_t minmTm0(0);

  for (int i=0; i<g->GetN(); i++){

    if (g->GetY()[i] >= minEff){
      minmTm0 = g->GetX()[i];
      return minmTm0;
    }

  }

  //Failure mode
  return -1;

}

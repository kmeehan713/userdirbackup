//Source Code to test using a Coulomb-Modified BE Fit on the pi plus spectra

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include <TAxis.h>
#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TLatex.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

using namespace std;

Bool_t draw = false;

void fitPiPlusSpectra(TString spectraFileName, TString resultFileName, Double_t midY){
  Int_t pid = 0;
	Int_t charge = 1;
  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = 1;//GetNCentralityBins();
  Double_t mTm0min, mTm0max, dNdy, dNdyErr, dNdy1, dNdy2, dNdy1Err, dNdy2Err, dNdySystErr;
  Int_t nPoints;
  TGraphErrors *tpcCorrectedSpectrum, *tpcRawSpectrum, *tpcSysErrSpectrum;
  TF1 *fitCoulombModifiedBE;
	vector <TGraphErrors *> dNdyGraph(nCentBins, (TGraphErrors *)NULL);

  //Create the OutputFile
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
		dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    gPad->DrawFrame(-2,0,2,150);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
   
		//Create the dN/dy Graphs
		dNdyGraph.at(iCentBin) = new TGraphErrors();
		dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensity_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){
		 
			cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
			particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

			//Compute Rapidity
			Double_t rapidity = GetRapidityRangeCenter(yIndex);

			//Get Spectra
			spectraFile->cd();
			tpcCorrectedSpectrum = (TGraphErrors *)gDirectory->Get(Form("CorrectedSpectra_%s/preparedSpectra_%s_Cent%02d_yIndex%02d",
								particleInfo->GetParticleName(pid,charge).Data(),
								particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
			cout<<"tpcCorrectedSpectrum "<<tpcCorrectedSpectrum<<endl;
			if (!tpcCorrectedSpectrum) continue;
			tpcRawSpectrum = (TGraphErrors *)gDirectory->Get(Form("RawSpectra_%s/statpreparedSpectra_%s_Cent%02d_yIndex%02d",
								particleInfo->GetParticleName(pid,charge).Data(),
								particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
			tpcSysErrSpectrum = (TGraphErrors *)gDirectory->Get(Form("SysErrCorrectedSpectra_%s/syspreparedSpectra_%s_Cent%02d_yIndex%02d",
								particleInfo->GetParticleName(pid,charge).Data(),
								particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));

			nPoints = tpcCorrectedSpectrum->GetN();
			if (nPoints < 1){
				cout<<"WARNING: TGraph has no points!"<<endl;
				continue;
			}
			if (nPoints <= 5) continue;
			Double_t *xarr = tpcCorrectedSpectrum->GetX();      
			mTm0max = ceil(xarr[nPoints-1]*100)/100;
			mTm0min = floor(xarr[0]*100)/100;

			//Get the fit function
      fitCoulombModifiedBE = new TF1("",CoulombModifiedBoseEinsteinFitFunc,0,0.5,7);
			fitCoulombModifiedBE->SetName(Form("spectrumFitCoulombModifiedBE_%s_Cent%02d_yIndex%02d",
					particleInfo->GetParticleName(pid,charge).Data(),
					iCentBin,yIndex));
			fitCoulombModifiedBE->SetLineStyle(7);
						
			//Set the parameters
		  fitCoulombModifiedBE->SetParameter(0,50);	
			fitCoulombModifiedBE->FixParameter(1,0.092);
			fitCoulombModifiedBE->FixParameter(2,particleInfo->GetParticleMass(PION));
			fitCoulombModifiedBE->FixParameter(3,0.216);
			fitCoulombModifiedBE->FixParameter(4,particleInfo->GetParticleMass(PROTON));
			fitCoulombModifiedBE->FixParameter(5,rapidity-midY);
			fitCoulombModifiedBE->FixParameter(6,0.0191);
			
			fitCoulombModifiedBE->SetParNames("dN/dy","T_{#pi}","#pi mass","T_{p}","p mass","y_{CM}","V_{C}");

			//Perform the fit
			tpcCorrectedSpectrum->Fit(fitCoulombModifiedBE,"REXO");

			dNdy = fitCoulombModifiedBE->GetParameter(0);
			dNdyErr = fitCoulombModifiedBE->GetParError(0);
			dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),rapidity-midY,dNdy);
			dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
							rapidityBinWidth/2.0,dNdyErr);

			if (draw){

				canvas->cd();
				canvas->DrawFrame(0,0.01*TMath::MinElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()),
							2.0,20*TMath::MaxElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()));
				canvas->SetLogy();
				tpcCorrectedSpectrum->Draw("APZ");	
				tpcCorrectedSpectrum->GetXaxis()->SetTitle("m_{T}-m_{0} (GeV)");
				
				fitCoulombModifiedBE->SetLineColor(2);
				fitCoulombModifiedBE->SetLineWidth(2);
				fitCoulombModifiedBE->SetLineStyle(7);
				fitCoulombModifiedBE->Draw("SAME");

			}

			tpcCorrectedSpectrum->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{-2}");
			tpcCorrectedSpectrum->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
										particleInfo->GetParticleSymbol(pid).Data()));
			tpcCorrectedSpectrum->GetYaxis()->SetTitleFont(63);
			tpcCorrectedSpectrum->GetYaxis()->SetTitleSize(25);
			tpcCorrectedSpectrum->GetXaxis()->SetTitleFont(63);
			tpcCorrectedSpectrum->GetXaxis()->SetTitleSize(25);
			tpcCorrectedSpectrum->GetXaxis()->SetTitleOffset(1.3);

			//Save Spectra
			resultsFile->cd();
			resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
			if (tpcCorrectedSpectrum) tpcCorrectedSpectrum->Write();
			resultsFile->cd();
			resultsFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
			if (tpcCorrectedSpectrum)	tpcRawSpectrum->Write();
			resultsFile->cd();
			resultsFile->cd(Form("SysErrCorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
			if (tpcCorrectedSpectrum)	tpcSysErrSpectrum->Write();

		}//End of rapidity loop


		if (draw){
			dNdyCanvas->cd();
			dNdyGraph.at(iCentBin)->Draw("APZ");
		}

    //Save Plots
		resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0) dNdyGraph.at(iCentBin)->Write();
	

	}//End of centrality loop

}//End of function

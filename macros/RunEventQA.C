//This macro runs the compiled eventQAmaker binary.
//The source code for eventQAmaker.cxx can be found at
//  src/qa/eventQAmaker.cxx

void RunEventQA(TString inputFile, TString outputFile, TString starLibrary, Bool_t eventCuts, Int_t nEvents, Double_t energy, TString eventConfig){

		//Load the Necessary Libraries
		gSystem->Load("../bin/TrackInfo_cxx.so");
		gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
    gSystem->Load("../bin/EventInfo_cxx.so");
	  gSystem->Load("../bin/DavisDstReader_cxx.so");
		gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
    gSystem->Load("../bin/UserCuts_cxx.so");
    gSystem->Load("../bin/eventQAmaker_cxx.so");

		SetVariableUserCuts(energy,eventConfig,starLibrary);

    eventQAmaker(inputFile,outputFile,eventCuts,nEvents);

}

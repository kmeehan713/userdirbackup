#!/bin/bash

#This runs the RunTrackQA.C macros which loads the necessary libraries
#and then runs the trackQAmaker.cxx binary. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
dataDirectory=/scratch_menkar/FixedTargetData/AuAu_4_5GeV_2015/
#dataDirectory=/scratch_menkar/FixedTargetData/AlAu_4_9GeV_2015/

#SET THE OUTPUT DIRECTORY HERE
#outputDirectory=../userfiles/AuAu_4_5GeV_2015/officialProduction/qa/
#outputDirectory=../userfiles/AlAu_4_9GeV_2015/qa/VertexIndex0/
outputDirectory=../userfiles/AuAu_4_5GeV_2015/testFiles/

#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1 #1000

#IF FALSE PRODUCES THE ROOT FILE OF HISTOS WITH NO TRACK CUTS
#IF TRUE PRODUCES THE ROOT FILE OF HISTOS WITH TRACKS CUTS
trackCuts=true #Note if this is true, the output root file will NOT have pre-cut plots
  
#SET THE STAR LIBRARY VERSION
starlib=SL16a

#SET THE COLLISION ENERGY HERE
energy=4.5

#SET THE EVENT CONFIGURATION HERE
eventConfig=FixedTarget2015

########################################################### 

#Array containing all of the dataFiles
dataFiles=( $dataDirectory/*.root )
#dataFiles=AuAu_4_5GeV_2015_0.root
processID=()
numberOfFiles=${#dataFiles[@]}
#numberOfFiles=1
outFiles=()
#i=$dataDirectory
#i+=$dataFiles

for i in ${dataFiles[@]}
do
    echo "Running on dataFile: " $i
    outFile=$(basename $i .root)

    outFile=$outputDirectory"$outFile"_Processed.root
 
    outFiles+=($outFile)

    root -l -q -b ../macros/RunTrackQA.C\(\"$i\",\"$outFile\",\"$starlib\",$trackCuts,$nEvents,$energy,\"$eventConfig\"\) 

    processID+=($!)
    echo ${processID[@]}
done

wait ${processID[@]}

suffix=trackQA
if $trackCuts; then
  suffix=$suffix"_trackCuts" 
else
  suffix=$suffix"_noCuts"
fi

hadd $outputDirectory/$suffix.root ${outFiles[@]}

wait

rm ${outFiles[@]}

exit

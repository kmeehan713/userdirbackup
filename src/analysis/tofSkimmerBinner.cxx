//This function uses the the time of flight measurement of the tracks to produce
//optimized z-tpc distributions. It also uses the liklihood weighted average method
//to do the recentering of the particle distributions

#include <iostream>
#include <vector>
#include <utility>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TEventList.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "usefulDataStructs.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"


void tofSkimmerBinner(TString inputDataFile, TString tofFile, TString pidCalibFile,  TString outputFile="",
		      Long64_t nEvents=-1){


  //Read the DavisDst
  DavisDstReader *davisDst = NULL;
  if (GetZVertexCuts().first != -999 && GetZVertexCuts().second != 999)
    davisDst = new DavisDstReader(inputDataFile,GetZVertexCuts().first,GetZVertexCuts().second);
  else
    davisDst = new DavisDstReader(inputDataFile);
  
  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  
  //If no output file was specified then we won't produce one
  TFile *outFile = NULL;
  if (outputFile.CompareTo(""))
    outFile = new TFile(outputFile,"RECREATE");
  
  //Create an instance of the Particle Info Class which will use the empirical fits
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibFile);

  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);

  const int nParticles = (int)particles.size();
  const int nCentralityBins = GetNCentralityBins();
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();

  //Get the Kinematics Tree
  TFile *pidFile = new TFile(pidCalibFile,"READ");
  TTree *kinematicTree = (TTree *)pidFile->Get("TotalMomentum/avgKinematicTree");

  //Build a Look Up Table for the Avg Total Momentum
  std::vector<std::vector<std::vector<std::vector<double> > > >
    avgTotalMomentum (nParticles, std::vector<std::vector<std::vector<double> > >
		      (nCentralityBins, std::vector<std::vector<double> >
		       (nRapidityBins, std::vector<double>
			(nmTm0Bins,-1.0))));

  kinematicTree->Draw(">>particleEntryList",Form("particleSpeciesIndex<=%d",PROTON));
  TEventList *particleEntryList = (TEventList *)gDirectory->Get("particleEntryList");
  kinematicTree->SetEventList(particleEntryList);
  
  TEventList *selectedEntryList = NULL;
  for (int iParticle=0; iParticle<nParticles; iParticle++){
    int speciesIndex = particles.at(iParticle);
    
    for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
 
      for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

	kinematicTree->SetEventList(particleEntryList);
	kinematicTree->Draw(">>selectedEntryList",Form("particleSpeciesIndex==%d&&centBinIndex==%d&&yIndex==%d",
						       speciesIndex,iCentBin,yIndex));
	selectedEntryList = (TEventList *)gDirectory->Get("selectedEntryList");
	kinematicTree->SetEventList(selectedEntryList);
	
	for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	 
	  //Get the Average Momentum For this Bin
	  kinematicTree->Draw("avgTotalP",Form("mTm0Index==%d",mTm0Index),"goff");
	  Long64_t nVals = kinematicTree->GetSelectedRows();
	  Double_t *pTotalAvgVals = kinematicTree->GetV1();
	  Double_t pTotal = TMath::Mean(nVals,pTotalAvgVals);

	  if (TMath::IsNaN(pTotal))
	    continue;

	  avgTotalMomentum.at(iParticle).at(iCentBin).at(yIndex).at(mTm0Index) = pTotal;
	  
	}//End Loop Over mTm0Index
      }//End loop Over yIndex
    }//End Loop Over iCentBin
  }//End Loop Over iParticle

  //Load the TOF Matching Efficiency Curves
  TFile *tofEffFile = new TFile(tofFile,"READ");
  tofEffFile->cd("TofEfficiencyCurves");
  std::vector<std::vector<std::vector<TF1 *> > >
    tofMatchEffCurve(nParticles,std::vector<std::vector<TF1 *> >
		     (nCentralityBins,std::vector<TF1 *>
		      (nRapidityBins,(TF1 *)NULL)));
  for (Int_t iParticle=0; iParticle<nParticles; iParticle++){
    for (Int_t iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
      for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
	tofMatchEffCurve.at(iParticle).at(iCentBin).at(yIndex) = NULL;
	tofMatchEffCurve.at(iParticle).at(iCentBin).at(yIndex) =
	  (TF1 *)gDirectory->Get(Form("tofMatchingEfficiencyCurve_Cent%d_%s_%d",
				      iCentBin,particleInfo->GetParticleName(particles.at(iParticle)).Data(),yIndex));
      }
    }//End Loop Over Centrality Bins    
  }//End Loop Over Particles
  
  //Book Histograms
  TH1D *nEventsHisto = new TH1D("nEvents","Events in Each Centrality Bin",
				nCentralityBins,0,nCentralityBins);
  std::vector<TH1D *> nEventsTriggerHisto (allowedTriggers.size(), (TH1D *) NULL);
  std::vector<TH1D *> centVarHisto(nCentralityBins,(TH1D *)NULL);

  for (unsigned int iTrigger=0; iTrigger<allowedTriggers.size(); iTrigger++){
    nEventsTriggerHisto.at(iTrigger) =
      new TH1D(Form("nEvents_trig%d",allowedTriggers.at(iTrigger)),
	       Form("Events in Each Centrality Bin For Trigger %d",allowedTriggers.at(iTrigger)),
	       nCentralityBins,0,nCentralityBins);
  }
  
  //Charged Separated Full Yield Histograms
  std::vector<std::vector<TH3F *> > ZTPCHistoPlus(nParticles,std::vector<TH3F *>
						  (nCentralityBins,(TH3F *) NULL));
  std::vector<std::vector<TH3F *> > ZTPCHistoMinus(nParticles,std::vector<TH3F *>
						   (nCentralityBins,(TH3F *) NULL));
  std::vector<std::vector<TH3F *> > ZTOFHistoPlus(nParticles,std::vector<TH3F *>
						  (nCentralityBins,(TH3F *) NULL));
  std::vector<std::vector<TH3F *> > ZTOFHistoMinus(nParticles,std::vector<TH3F *>
						   (nCentralityBins,(TH3F *) NULL));

  //Tof Optimized Charged combimed yield histograms
  std::vector<std::vector<TH3F *> >
    ZTPCHistoTofOpt(nParticles,std::vector<TH3F *>
		    (nParticles,(TH3F *)NULL));

  //Centrality and Charged Combined zTOF Histograms
  std::vector<TH3F *> ZTOFHistoAllCentCharge (nParticles, (TH3F *)NULL);
    
  Int_t nZTPCBins(600), nZTOFBins(700);
  Double_t minZTPC(-2), maxZTPC(4);
  Double_t minZTOF(-1), maxZTOF(1);
  
  for (int iParticle=0; iParticle<nParticles; iParticle++){

    const int speciesIndex   = particles.at(iParticle);
    const char *speciesNamePlus    = particleInfo->GetParticleName(speciesIndex,1).Data();
    const char *speciesNameMinus   = particleInfo->GetParticleName(speciesIndex,-1).Data();
    const char *speciesSymbolPlus  = particleInfo->GetParticleSymbol(speciesIndex,1).Data();
    const char *speciesSymbolMinus = particleInfo->GetParticleSymbol(speciesIndex,-1).Data();
    
    //The TOF Optimized ZTPC Histograms
    for (int iSubParticle=0; iSubParticle<nParticles; iSubParticle++){
      
      const int subSpeciesIndex     = particles.at(iSubParticle);
      const char *speciesName       = particleInfo->GetParticleName(speciesIndex).Data();
      const char *speciesSymbol     = particleInfo->GetParticleSymbol(speciesIndex).Data();
      const char *subSpeciesName    = particleInfo->GetParticleName(subSpeciesIndex).Data();
      const char *subSpeciesSymbol  = particleInfo->GetParticleSymbol(subSpeciesIndex).Data();
      
      ZTPCHistoTofOpt.at(iParticle).at(iSubParticle) =
	new TH3F(Form("zTPC_%s_%s",speciesName,subSpeciesName),
		 Form("Z_{TPC} of %s (%s Opt.);y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TPC,%s}",
		      speciesSymbol,subSpeciesSymbol,speciesSymbol,speciesSymbol, speciesSymbol),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTPCBins,minZTPC,maxZTPC);
      
    }//End Loop Over Subspecies

    ZTOFHistoAllCentCharge.at(iParticle) =
      new TH3F(Form("zTOF_AllCentCharge_%s",particleInfo->GetParticleName(speciesIndex).Data()),
	       Form("z_{TOF} of %s (All Centralities and Charges);y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TOF,%s}",
		    particleInfo->GetParticleSymbol(speciesIndex).Data(),
		    particleInfo->GetParticleSymbol(speciesIndex).Data(),
		    particleInfo->GetParticleSymbol(speciesIndex).Data(),
		    particleInfo->GetParticleSymbol(speciesIndex).Data()),
	       nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTOFBins,minZTOF,maxZTOF);

    //Histograms binned in charge, particle species, and centrality
    for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
      
      if (!centVarHisto.at(iCentBin)){
	centVarHisto.at(iCentBin) =
	  new TH1D(Form("centralityVariable_Cent%d",iCentBin),
		   Form("CentralityVariable Distribution in Centrality Bin %d",iCentBin),
		   1000,0,1000);
      }

      //The Total Histograms
      ZTPCHistoPlus.at(iParticle).at(iCentBin) =
	new TH3F(Form("zTPC_%s_Cent%d_%s",speciesNamePlus,iCentBin,"Total"),
		 Form("Z_{TPC} of %s (%s) | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TPC,%s}",
		      speciesSymbolPlus,"Total",
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      speciesSymbolPlus,speciesSymbolPlus, speciesSymbolPlus),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTPCBins,minZTPC,maxZTPC);

      ZTPCHistoMinus.at(iParticle).at(iCentBin) =
	new TH3F(Form("zTPC_%s_Cent%d_%s",speciesNameMinus,iCentBin,"Total"),
		 Form("Z_{TPC} of %s (%s) | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TPC,%s}",
		      speciesSymbolMinus,"Total",
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      speciesSymbolMinus,speciesSymbolMinus, speciesSymbolMinus),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTPCBins,minZTPC,maxZTPC);

      ZTOFHistoPlus.at(iParticle).at(iCentBin) =
	new TH3F(Form("zTOF_%s_Cent%d_%s",speciesNamePlus,iCentBin,"Total"),
		 Form("Z_{TOF} of %s (%s) | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TOF,%s}",
		      speciesSymbolPlus,"Total",
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      speciesSymbolPlus,speciesSymbolPlus, speciesSymbolPlus),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTOFBins,minZTOF,maxZTOF);
      ZTOFHistoPlus.at(iParticle).at(iCentBin)->Sumw2();

      ZTOFHistoMinus.at(iParticle).at(iCentBin) =
	new TH3F(Form("zTOF_%s_Cent%d_%s",speciesNameMinus,iCentBin,"Total"),
		 Form("Z_{TOF} of %s (%s) | Cent=[%d,%d]%%;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TOF,%s}",
		      speciesSymbolMinus,"Total",
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      speciesSymbolMinus,speciesSymbolMinus, speciesSymbolMinus),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,nZTOFBins,minZTOF,maxZTOF);
      ZTOFHistoMinus.at(iParticle).at(iCentBin)->Sumw2();
      
      
    }//End Loop Over Centrality Bins
  }//End Loop Over Particles

  //If the user has passed a number of events to process then use it,
  //otherwise use all the entries in the tree
  Long64_t nEntries(0);
  if (nEvents > 0)
    nEntries = nEvents;
  else
    nEntries = davisDst->GetEntries();

  //Loop Over the entries/events
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    //Loop Over the Vertices
    for (Int_t iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){

      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;

      //Get the Centrality Bin
      UInt_t triggerID = event->GetTriggerID(&allowedTriggers);
      Int_t centralityVariable = GetCentralityVariable(primaryVertex);
      Int_t centralityBin      = GetCentralityBin(centralityVariable,triggerID,
						  primaryVertex->GetZVertex());

      //Skip events with bad centrality bins
      if (centralityBin < 0)
	continue;

      //Get the Index of the Trigger
      unsigned int triggerIndex(-1);
      for (unsigned int iTrig=0; iTrig<allowedTriggers.size(); iTrig++){
	if (triggerID == allowedTriggers.at(iTrig)){
	  triggerIndex=iTrig;
	  break;
	}	
      }
      
      //Fill the Centrality Histograms
      nEventsHisto->Fill(centralityBin);
      centVarHisto.at(centralityBin)->Fill((double)centralityVariable);
      nEventsTriggerHisto.at(triggerIndex)->Fill(centralityBin);      
      
      //Loop Over the Tracks
      for (Int_t iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){

	track = primaryVertex->GetPrimaryTrack(iTrack);
	if (!IsGoodTrack(track))
	  continue;

	//Loop Over each of the Particle Species and Fill the Yield Histograms
	for (Int_t iParticle=0; iParticle<nParticles; iParticle++){

	  Int_t speciesIndex      = particles.at(iParticle);
	  Double_t massAssumption = particleInfo->GetParticleMass(speciesIndex);
	  Double_t rapidity       = track->GetRapidity(massAssumption);
	  Double_t mTm0           = track->GetmTm0(massAssumption);
	  Int_t yIndex = GetRapidityIndex(rapidity);
	  Int_t mTm0Index = GetmTm0Index(mTm0);

	  if (yIndex < 0 || mTm0Index < 0)
	    continue;

	  Double_t avgTotalPInBin = avgTotalMomentum.at(speciesIndex).at(centralityBin).at(yIndex).at(mTm0Index);

	  if (avgTotalPInBin < 0)
	    continue;

	  //Set the Particle Species and Rapidity Bin of interest
	  if (!particleInfo->SetEmpiricalBichselFunction(speciesIndex,yIndex)){
	    continue;
	  }
	  
	  //Compute the ZTPC Variable 
	  //Double_t zTPC = particleInfo->ComputeZTPC(track->GetdEdx(),track->GetPt(),
	  //track->GetPz(),speciesIndex);
	  Double_t zTPC = particleInfo->ComputeWeightedZTPC(track->GetdEdx(),track->GetPt(),
	  						    track->GetPz(),speciesIndex,avgTotalPInBin);

	  //Fill the Total zTPC Histograms depending on charge
	  if (track->GetCharge() > 0){
	    ZTPCHistoPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	  }
	  else if (track->GetCharge() < 0){
	    ZTPCHistoMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	  }

	  //Check if this is a good tof track
	  if (!IsGoodTofTrack(track))
	    continue;

	  //Compute the zTOF
	  /*
	  Double_t zTOF = particleInfo->ComputeZTOF(1.0/track->GetTofBeta(),track->GetPt(),
						    track->GetPz(),speciesIndex);
	  */	  
	  Double_t zTOF = particleInfo->ComputeWeightedZTOF(1.0/track->GetTofBeta(),track->GetPt(),
							    track->GetPz(),speciesIndex,avgTotalPInBin);

	  //Fill the TOF optimized, carged combined ZTPC Histos
	  for(Int_t iSubParticle=0; iSubParticle<nParticles; iSubParticle++){
	    if (fabs(particleInfo->GetPercentErrorTOF(1.0/track->GetTofBeta(),track->GetPTotal(),
						      particles.at(iSubParticle)))<2.0)
	      ZTPCHistoTofOpt.at(iParticle).at(iSubParticle)->Fill(rapidity,mTm0,zTPC);
	  }//End Loop over SubParticles
	  
	  //Get the Tof Matching Efficiency Weight, make sure it within the defined range of the
	  //fit function and that its in rapidity bin that has a function. If either condition is
	  //not fulfilled set the weight to -1 which means that the track will not fill the ZTOF Histos below.
	  Double_t tofEffWeight(1);
	  if (tofMatchEffCurve.at(iParticle).at(centralityBin).at(yIndex)){
	    Double_t tofCurveMin(0), tofCurveMax(0);
	    tofMatchEffCurve.at(iParticle).at(centralityBin).at(yIndex)->GetRange(tofCurveMin,tofCurveMax);
	    if (mTm0 >= tofCurveMin && mTm0 <= tofCurveMax)
	      tofEffWeight = 1.0/tofMatchEffCurve.at(iParticle).at(centralityBin).at(yIndex)->Eval(mTm0);
	    else
	      tofEffWeight = -1;
	  }
	  else
	    tofEffWeight = -1;
	  

	  //Skip Tracks that have a negative Tof Weight
	  if (tofEffWeight <= 0)
	    continue;

	  //Fill the Centrality Integrated and Charged Combined zTOF Histograms
	  ZTOFHistoAllCentCharge.at(iParticle)->Fill(rapidity,mTm0,zTOF,tofEffWeight);
	  
	  //Fill the Total zTOF Histogram depending on charge
	  if (track->GetCharge() > 0)
	    ZTOFHistoPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTOF,tofEffWeight);
	  else if (track->GetCharge() < 0 )
	    ZTOFHistoMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTOF,tofEffWeight);
	  	  
	}//End Loop Over Particles
	
      }//End Loop Over Tracks            
    }//End Loop Over Vertices    
  }//End Loop Over Entries

  //Save
  if (outFile){

    outFile->cd();
    nEventsHisto->Write();
    for (unsigned int iTrig=0; iTrig<allowedTriggers.size(); iTrig++){
      nEventsTriggerHisto.at(iTrig)->Write();
    }
    
    outFile->mkdir("CentralityVariableHistograms");
    outFile->cd("CentralityVariableHistograms");
    for (Int_t iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
      centVarHisto.at(iCentBin)->Write();
    }
    
    outFile->cd();
    outFile->mkdir("YieldHistograms");
    outFile->cd("YieldHistograms");
    for (int iParticle=0; iParticle<nParticles; iParticle++){

      ZTOFHistoAllCentCharge.at(iParticle)->Write();

      for (int iSubParticle=0; iSubParticle<nParticles; iSubParticle++){
	ZTPCHistoTofOpt.at(iParticle).at(iSubParticle)->Write();
      }//End Loop Over Sub particles
	
      for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

	ZTPCHistoPlus.at(iParticle).at(iCentBin)->Write();
	ZTPCHistoMinus.at(iParticle).at(iCentBin)->Write();
	ZTOFHistoPlus.at(iParticle).at(iCentBin)->Write();
	ZTOFHistoMinus.at(iParticle).at(iCentBin)->Write();
	
      }//End Loop Over centrality bins
    }//End Loop Over particles
  }//End Save
  
}

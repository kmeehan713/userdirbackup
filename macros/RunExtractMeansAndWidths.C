void RunExtractMeansAndWidths(TString yieldHistoFile, TString pidCalibrationFile,
			TString starLibrary, Double_t energy, TString eventConfig){

  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/fitZTPCUtilities_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/ExtractMeansAndWidths_cxx.so");
  
  SetVariableUserCuts(energy,eventConfig,starLibrary);
  
  ExtractMeansAndWidths(yieldHistoFile,pidCalibrationFile);

}

#!/bin/bash

#This script runs the Glauber Simulation

#USER DEFINED VARIABLE QUANTITIES
nEvents=200000            #Number of DESIRED events with at least one participating nucleon
#nEvents=50000            #Number of DESIRED events with at least one participating nucleon
nNucleonsA=197           #Number of nucleons in nucleus A
nNucleonsB=197           #Number of nucleons in nucleus B
nnCrossSection=27        #The nucleon-nucleon inelastic crosssection in mb
nucleonDistribution=1    #The method for distributing the nucleons in the nuclei
                         #0=Uniform-Hard Sphere, 1=Woods-Saxon
outDirectory=/scratch_menkar/kmeehan/TeamFXT/UserCode/davisdstanalysis/userfiles/AuAu_4_5GeV_2015/officialProduction/centTest/ #Full path to output directory (file name is automatically generated)
#outDirectory=/scratch_menkar/kmeehan/TeamFXT/UserCode/davisdstanalysis/userfiles/AlAu_4_9GeV_2015/analysis/VertexIndex0/ #Full path to output directory (file name is automatically generated)

#Run the Glauber Model With the Above Options
root -l -b -q ../macros/RunGlauberSimulation.C\($nEvents\,$nNucleonsA\,$nNucleonsB\,$nnCrossSection\,$nucleonDistribution,\"$outDirectory\"\)


//Fit the Spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

bool draw = false;

void fitSpectra(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge,
		Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();
  
  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Create the Output Results File
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
    
  //Create a Canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    dNdyCanvas->Divide(2,1);
    dNdyCanvas->cd(1);
    gPad->DrawFrame(-2,0,2,150);
    dNdyCanvas->cd(2);
    gPad->DrawFrame(-2,0,2,0.4);
  }

  std::vector<TGraphErrors *> dNdyGraph(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> tSlopeGraph1(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> tSlopeGraph2(nCentBins,(TGraphErrors *)NULL);
    
  //Local Pointers
  TGraphErrors *tpcSpectrum, *tofSpectrum;
  TF1 *fitFuncHigh, *fitFuncLow;

  //Get the User's rapidity bin
  Int_t userRapidityBin = GetRapidityIndex(userRapidity);
  if (userRapidity != -999 && userRapidityBin < 0){
    cout <<"ERROR: FitSpectra - Invalid value of rapidity. EXITING!\n";
    exit (EXIT_FAILURE);
  }

  //Loop Over the Centrality and rapidity bins and fit the spectra
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    cout <<iCentBin <<endl;
    //If the user has specified a valid cent bin,
    //then skip all others
    if (userCentBin != -999 && userCentBin >= 0 &&
	userCentBin < nCentBins && iCentBin != userCentBin)
      continue;

    resultsFile->cd();
    dNdyGraph.at(iCentBin) = new TGraphErrors();
    tSlopeGraph1.at(iCentBin) = new TGraphErrors();
    tSlopeGraph2.at(iCentBin) = new TGraphErrors();
 
    dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    tSlopeGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    tSlopeGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    
    dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    tSlopeGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    tSlopeGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));

    //Fits and Confidence intervals of the slope parameters
    TF1 *tSlopeFit1 = new TF1("tSlopeFit1","pol2",-2,2);
    TF1 *tSlopeFit2 = new TF1("tSlopeFit2","pol2",-2,2);
    tSlopeFit1->SetNpx(10000);
    tSlopeFit2->SetNpx(10000);
    
    TGraphErrors *tSlopeConf1 = new TGraphErrors(tSlopeFit1->GetNpx());
    TGraphErrors *tSlopeConf2 = new TGraphErrors(tSlopeFit2->GetNpx());
    tSlopeConf1->SetFillColor(particleInfo->GetParticleColor(pid));
    tSlopeConf2->SetFillColor(particleInfo->GetParticleColor(pid)+2);
    tSlopeConf1->SetFillStyle(3001);
    tSlopeConf2->SetFillStyle(3001);
    Double_t stepSize = (2-(-2))/(double)tSlopeConf1->GetN();
    for (int i=0; i<tSlopeConf1->GetN(); i++){
      tSlopeConf1->SetPoint(i,-2 + i*stepSize,0);
      tSlopeConf2->SetPoint(i,-2 + i*stepSize,0);
    }
    
    //We do two rounds of fitting. The first to parameterize the slope,
    //and the second to extract the dNdy.
 
    for (int iRound=0; iRound<2; iRound++){
      
      for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
	cout <<yIndex <<endl;
	//Skip Rapidity bins not of interest
	if (yIndex < GetMinRapidityIndexOfInterest() || yIndex > GetMaxRapidityIndexOfInterest())
	  continue;
	
	//If the user has specified a vaid rapidity value,
	//then skip all other rapidity bins
	if (userRapidityBin >= 0 && userRapidityBin != yIndex)
	  continue;
	
	//Compute Rapidity
	Double_t rapidity = GetRapidityRangeCenter(yIndex);
	
	//Get the Spectra
	spectraFile->cd();
	spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	tpcSpectrum = (TGraphErrors *)gDirectory->Get(Form("correctedSpectra_%s_Cent%02d_yIndex%02d",
							   particleInfo->GetParticleName(pid,charge).Data(),
							   iCentBin,yIndex));
	tofSpectrum = (TGraphErrors *)gDirectory->Get(Form("correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
							   particleInfo->GetParticleName(pid,charge).Data(),
							   iCentBin,yIndex));

	cout <<tpcSpectrum <<" " <<tofSpectrum <<endl;
	
	//Make sure the tpc Spectrum Exists
	if (!tpcSpectrum){
	  cout <<Form("INFO - TPC Spectrum Does Not Exist! CentIndex:%d, yIndex:%d \n",iCentBin,yIndex);
	  continue;
	}

	//Make sure the tpc Spectrum has points
	if (tpcSpectrum->GetN() < 5){
	  cout <<Form("INFO - TPC Spectrum Does Not have enough points! CentIndex:%d, yIndex:%d \n",
		      iCentBin,yIndex);
	  delete tpcSpectrum;
	  tpcSpectrum = NULL;
	  continue;
	}
	
	//Make sure the tof spectrum exists and has points
	if (tofSpectrum){
	  if (tofSpectrum->GetN() < 4){
	    delete tofSpectrum;
	    tofSpectrum = NULL;
	  }
	}
	else {
	  tofSpectrum = NULL;
	}
	      
	Double_t minRangeLowFit = 0.0;
	Double_t maxRangeLowFit = tpcSpectrum->GetX()[(int)(tpcSpectrum->GetN()*.66)];
	Double_t minRangeHighFit = tofSpectrum!=NULL ? tofSpectrum->GetX()[(int)(tofSpectrum->GetN()*.5)]:
	  tpcSpectrum->GetX()[(int)(tpcSpectrum->GetN()*.5)];
	Double_t maxRangeHighFit = 10.0;
	//The Fit Function can depend on the particle Species
	if (pid == PION){
	  fitFuncLow = new TF1("",BoseEinsteinFitFuncInRange,minRangeLowFit,
			       maxRangeLowFit,6);
	  fitFuncHigh = new TF1("",BoseEinsteinFitFuncInRange,minRangeHighFit,
				maxRangeHighFit,6);	
	}
	else{
	  fitFuncLow = new TF1("",ThermalFitFunc,0.0,0.3,3);
	  
	}
	
	fitFuncLow->SetName(Form("spectrumFitLow_%s_Cent%02d_yIndex%02d",
				 particleInfo->GetParticleName(pid,charge).Data(),
				 iCentBin,yIndex));
	fitFuncHigh->SetName(Form("spectrumFitHigh_%s_Cent%02d_yIndex%02d",
				  particleInfo->GetParticleName(pid,charge).Data(),
				  iCentBin,yIndex));
	

	//Ranges over which to compute the dNdy from the fits.
	//Notes: 1. We must subtract/add half the value of the bin widths (x error)
	//       to the beginning and ending point since the dNdy counting takes
	//       into account the full bin width of all measured points.
	
	//Use the low fit to compute the dNdy in the low, non-measured range of mTm0
	Double_t mindNdyRangeLowFit = 0.0;
	Double_t maxdNdyRangeLowFit = tpcSpectrum->GetX()[0]-tpcSpectrum->GetEX()[0]; 

	//Use the upper fit to compute the dNdy in the high, non-measured range of mTm0
	Double_t mindNdyRangeHighFit = tofSpectrum != NULL ? tofSpectrum->GetX()[tofSpectrum->GetN()-1]+
	  tofSpectrum->GetEX()[tofSpectrum->GetN()-1] : tpcSpectrum->GetX()[tpcSpectrum->GetN()-1] +
	  tpcSpectrum->GetEX()[tpcSpectrum->GetN()-1];
	Double_t maxdNdyRangeHighFit = 10.0;

	//Set the Parameters
	fitFuncLow->SetParameter(0,100);
	fitFuncLow->SetParameter(1,0.16);
	fitFuncLow->FixParameter(2,particleInfo->GetParticleMass(pid));
	fitFuncLow->FixParameter(3,rapidity);
	fitFuncLow->FixParameter(4,mindNdyRangeLowFit);
	fitFuncLow->FixParameter(5,maxdNdyRangeLowFit);
		
	fitFuncHigh->SetParameter(0,100);
	fitFuncHigh->SetParameter(1,0.16);
	fitFuncHigh->FixParameter(2,particleInfo->GetParticleMass(pid));
	fitFuncHigh->FixParameter(3,rapidity);
	fitFuncHigh->FixParameter(4,mindNdyRangeHighFit);
	fitFuncHigh->FixParameter(5,maxdNdyRangeHighFit);

	//Fit this is the first round the set limits on the tSlope Parameters
	if (iRound == 0){
	  fitFuncLow->SetParLimits(1,.15,.35);
	  fitFuncHigh->SetParLimits(1,.15,.35);
	}
	
	//If this is the second round then fixed the tSlope Parameters
	if (iRound == 1){
	  fitFuncLow->FixParameter(1,tSlopeFit1->Eval(rapidity));
	  fitFuncHigh->FixParameter(1,tSlopeFit2->Eval(rapidity));
	}
	
	//Perform the Fit
	Int_t fitStatus(-1);
	Int_t nAttempts(0);
	while (fitStatus != 0 && nAttempts < 5){
	  fitStatus = tpcSpectrum->Fit(fitFuncLow,"REX0");
	  nAttempts++;
	}
	if (tofSpectrum){
	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    fitStatus = tofSpectrum->Fit(fitFuncHigh,"REX0");
	    nAttempts++;
	  }
	}
	else{
	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    fitStatus = tpcSpectrum->Fit(fitFuncHigh,iRound == 1 ? "R+EX0":"REX0");
	    nAttempts++;
	  }
	}
	
	//Extract the Slope Parameters if this is the first round
	if (iRound == 0){
	  tSlopeGraph1.at(iCentBin)->SetPoint(tSlopeGraph1.at(iCentBin)->GetN(),
					      GetRapidityRangeCenter(yIndex),
					      fitFuncLow->GetParameter(1));
	  tSlopeGraph1.at(iCentBin)->SetPointError(tSlopeGraph1.at(iCentBin)->GetN()-1,
						   rapidityBinWidth/2.0,
						   fitFuncLow->GetParError(1));
	  
	  tSlopeGraph2.at(iCentBin)->SetPoint(tSlopeGraph2.at(iCentBin)->GetN(),
					      GetRapidityRangeCenter(yIndex),
					      fitFuncHigh->GetParameter(1));
	  tSlopeGraph2.at(iCentBin)->SetPointError(tSlopeGraph2.at(iCentBin)->GetN()-1,
						   rapidityBinWidth/2.0,
						   fitFuncHigh->GetParError(1));
	}
	
	//Extract the dNdy if this is the second round
	if (iRound == 1){

	  //Use the fit functions to compute the dNdy in the non measured regions
	  Double_t dNdyBelowMeasured = fitFuncLow->GetParameter(0);
	  Double_t dNdyAboveMeasured = fitFuncHigh->GetParameter(0);

	  //Count the dNdy in the measured region
	  std::pair<double,double> tpcCount = CountdNdyOfSpectrum(tpcSpectrum,pid);
	  std::pair<double,double> tofCount = (tofSpectrum != NULL ? CountdNdyOfSpectrum(tofSpectrum,pid):
					       std::make_pair(0.0,0.0));	  
	  Double_t dNdyMeasured = tpcCount.first + tofCount.first;

	  //Sum the measured dNdy and the dNdy from extrapolations
	  Double_t dNdy = dNdyBelowMeasured + dNdyMeasured + dNdyAboveMeasured;

	  //**********************************
	  //Statistical Error
	  //**********************************
	  //The statistical error is the combination of the fit error on the dNdys from the
	  //fits, rootN errors of the measured region, and varying the fixed slope parameters
	  
	  //Fits used to vary the slope parameter
	  Int_t iPoint = TMath::BinarySearch(tSlopeConf1->GetN(),tSlopeConf1->GetX(),rapidity);
	  Double_t upperErr(0), lowerErr(0);
	  TF1 tempFit1(*fitFuncLow), tempFit2(*fitFuncHigh), tempFit3(*fitFuncLow), tempFit4(*fitFuncHigh);	  
	  tempFit1.FixParameter(1,tSlopeFit1->Eval(rapidity)+tSlopeConf1->GetEY()[iPoint]);
	  tempFit2.FixParameter(1,tSlopeFit2->Eval(rapidity)+tSlopeConf2->GetEY()[iPoint]);
	  tempFit3.FixParameter(1,tSlopeFit1->Eval(rapidity)+tSlopeConf1->GetEY()[iPoint]);
	  tempFit4.FixParameter(1,tSlopeFit2->Eval(rapidity)+tSlopeConf2->GetEY()[iPoint]);

	  //If the fit fails retry up to five times
	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    fitStatus = tpcSpectrum->Fit(&tempFit1,"NRQEX0");
	    nAttempts++;
	  }
	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    fitStatus = tpcSpectrum->Fit(&tempFit3,"NRQEX0");
	    nAttempts++;
	  }
	  if (tofSpectrum){
	    fitStatus = -1;
	    nAttempts = 0;
	    while (fitStatus != 0 && nAttempts < 5){
	      fitStatus = tofSpectrum->Fit(&tempFit2,"NRQEX0");
	      nAttempts++;
	    }
	    fitStatus = -1;
	    nAttempts = 0;
	    while (fitStatus != 0 && nAttempts < 5){
	      fitStatus = tofSpectrum->Fit(&tempFit4,"NRQEX0");
	      nAttempts++;
	    }
	  }
	  else{
	    fitStatus = -1;
	    nAttempts = 0;
	    while (fitStatus != 0 && nAttempts < 5){
	      fitStatus = tpcSpectrum->Fit(&tempFit2,"NRQEX0");
	      nAttempts++;
	    }
	    fitStatus = -1;
	    nAttempts = 0;
	    while (fitStatus != 0 && nAttempts < 5){
	      fitStatus = tpcSpectrum->Fit(&tempFit4,"NRQEX0");
	      nAttempts++;
	    }
	  }
	  			  
	  
	  Double_t dNdyErr = sqrt(pow(fitFuncLow->GetParError(0),2) + pow(fitFuncHigh->GetParError(0),2) +
				  pow(tempFit1.GetParError(0),2) + pow(tempFit2.GetParError(0),2) +
				  pow(tempFit3.GetParError(0),2) + pow(tempFit4.GetParError(0),2) +
				  pow(tpcCount.second,2) + pow(tofCount.second,2));


	  //**********************************
	  //Systematic Error
	  //**********************************
	  //The systematic error is determined by chaging the fit for at low mTm0
	  TF1 systematicFitLow("",mTExponentialFitFuncInRange,
			    minRangeLowFit,maxRangeLowFit,6);
	  systematicFitLow.SetParameter(0,fitFuncLow->GetParameter(0));
	  systematicFitLow.SetParameter(1,fitFuncLow->GetParameter(1));
	  systematicFitLow.FixParameter(2,fitFuncLow->GetParameter(2));
	  systematicFitLow.FixParameter(3,fitFuncLow->GetParameter(3));
	  systematicFitLow.FixParameter(4,fitFuncLow->GetParameter(4));
	  systematicFitLow.FixParameter(5,fitFuncLow->GetParameter(5));

	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    tpcSpectrum->Fit(&systematicFitLow,"RNQEX0");
	    nAttempts++;
	  }
	  
	  TF1 systematicFitHigh("",mTExponentialFitFuncInRange,
			       minRangeHighFit,maxRangeHighFit,6);
	  systematicFitHigh.SetParameter(0,fitFuncHigh->GetParameter(0));
	  systematicFitHigh.SetParameter(1,fitFuncHigh->GetParameter(1));
	  systematicFitHigh.FixParameter(2,fitFuncHigh->GetParameter(2));
	  systematicFitHigh.FixParameter(3,fitFuncHigh->GetParameter(3));
	  systematicFitHigh.FixParameter(4,fitFuncHigh->GetParameter(4));
	  systematicFitHigh.FixParameter(5,fitFuncHigh->GetParameter(5));

	  fitStatus = -1;
	  nAttempts = 0;
	  while (fitStatus != 0 && nAttempts < 5){
	    tpcSpectrum->Fit(&systematicFitHigh,"RNQEX0");
	    nAttempts++;
	  }
	  
	  //Find the percent difference between the systematic dNdy and the default
	  //dNdy, it will be for the systematic error.
	  Double_t systematicdNdyLow = systematicFitLow.GetParameter(0);
	  Double_t systematicdNdyHigh = systematicFitHigh.GetParameter(0);
	  Double_t systematicdNdyTotal = systematicdNdyLow+systematicdNdyHigh+tpcCount.first+tofCount.first;
	  
	  Double_t percentDiff = fabs(systematicdNdyTotal - dNdy)/dNdy;
	  cout <<"PercentDiff: " <<percentDiff <<endl;

	  //Add the percent diff to the dNdyErr
	  Double_t sysErr = percentDiff*dNdyBelowMeasured;

	  dNdyErr = sqrt( pow(dNdyErr,2) + pow(sysErr,2) );
	  
	  
	  //**********************************
	  //Set Point on dNdy Graph
	  //**********************************
	  dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),
					   GetRapidityRangeCenter(yIndex),
					   dNdy);
	  dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
						rapidityBinWidth/2.0,
						dNdyErr);

	}//End if Round 1
      
      	
	if (draw){
	  canvas->cd();
	  canvas->DrawFrame(0,0.01*TMath::MinElement((tofSpectrum!=NULL ? tofSpectrum->GetN():tpcSpectrum->GetN()),
						     (tofSpectrum!=NULL ? tofSpectrum->GetY():tpcSpectrum->GetY())),
			    2.0,20*TMath::MaxElement(tpcSpectrum->GetN(),tpcSpectrum->GetY()));
	  canvas->SetLogy();
	  tpcSpectrum->Draw("PZ");
	  if (tofSpectrum)
	    tofSpectrum->Draw("PZ");

	  fitFuncLow->Draw("SAME");
	  fitFuncHigh->Draw("SAME");
	  canvas->Update();
	  
	  if (iRound == 1){
	    dNdyCanvas->cd(1);
	    dNdyGraph.at(iCentBin)->Draw("PZ");
	  }
	  if (iRound == 0){
	    dNdyCanvas->cd(2);
	    tSlopeGraph1.at(iCentBin)->Draw("PZ");
	    tSlopeGraph2.at(iCentBin)->Draw("PZ");
	  }
	  
	  dNdyCanvas->Update();
	  if (iRound == 1)
	    gSystem->Sleep(2000);
	}

	if (iRound == 1){
	  //Save Spectra
	  resultsFile->cd();
	  resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	  if (tpcSpectrum)
	    tpcSpectrum->Write();
	  if (tofSpectrum)
	    tofSpectrum->Write();
	}
	
	/*
	//CleanUp
	if (iRound == 0){
	  if (tpcSpectrum)
	    delete tpcSpectrum;
	  if (tofSpectrum)
	    delete tofSpectrum;
	  if (fitFuncLow)
	    delete fitFuncLow;
	  if (fitFuncHigh)
	    delete fitFuncHigh;
	  
	  tpcSpectrum = NULL;
	  tofSpectrum = NULL;
	}
	*/
      }//End Loop Over yIndex
      
      cout <<"Why am I fitting here!" <<endl;
      
      //Fit the Slope Parameter Graphs
      if (iRound == 0){
	
	tSlopeGraph1.at(iCentBin)->Fit(tSlopeFit1,"REX0");
	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(tSlopeConf1,0.68);	
	tSlopeGraph2.at(iCentBin)->Fit(tSlopeFit2,"REX0");
	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(tSlopeConf2,0.68);	
	
	if (draw){
	  dNdyCanvas->cd(2);
	  tSlopeConf1->Draw("2");
	  tSlopeConf2->Draw("2");
	  
	  dNdyCanvas->Update();
	}
	
      }
      
      
    }//End Loop Over fitting Rounds



    //Set Names
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensity_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
    tSlopeGraph1.at(iCentBin)->SetName(Form("SlopeParameterLowmTm0_%s_Cent%02d",
					    particleInfo->GetParticleName(pid,charge).Data(),
					    iCentBin));
    tSlopeGraph2.at(iCentBin)->SetName(Form("SlopeParameterHighmTm0_%s_Cent%02d",
					    particleInfo->GetParticleName(pid,charge).Data(),
					    iCentBin));
    tSlopeFit1->SetName(Form("SlopeParameterLowmTm0Fit_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeFit2->SetName(Form("SlopeParameterHighmTm0Fit_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeConf1->SetName(Form("SlopeParameterLowmTm0Conf_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeConf2->SetName(Form("SlopeParameterHighmTm0Conf_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    
    //Save
    resultsFile->cd();
    resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0)
      dNdyGraph.at(iCentBin)->Write();
    resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (tSlopeGraph1.at(iCentBin)->GetN() > 0){
      tSlopeGraph1.at(iCentBin)->Write();
      tSlopeFit1->Write();
      tSlopeConf1->Write();
    }
    if (tSlopeGraph2.at(iCentBin)->GetN() > 0){
      tSlopeGraph2.at(iCentBin)->Write();
      tSlopeFit2->Write();
      tSlopeConf2->Write();
    }    

    
  }//End Loop Over centBins

      
}

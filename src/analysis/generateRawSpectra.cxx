//Extracts the yield of pions plus and minus by doing a simultaneous
//fit to the tof optimized and combined histograms

#include <iostream>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TF1.h>
#include <TEventList.h>

#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TPaveText.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TString.h>
#include <Fit/Fitter.h>
#include <Fit/BinData.h>
#include <Fit/Chi2FCN.h>
#include <Math/WrappedMultiTF1.h>
#include <HFitInterface.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "utilityFunctions.h"
#include "fitZTPCUtilities.h"

/****************************************/
//Global Control over the length of the spectra
const Double_t MIN_MTM0(0.1);
const Double_t MAX_MTM0(1.5);
//Global Control for where the tof and tpc spectra should be stitched
//This is a suggestion. If the TPC spectrum does not extend this far
//then the tof spectrum is allowed to start earlier.
const Double_t TPC_TOF_STITCH(0.5);
/****************************************/

//Previous Electron Amplitude
Double_t PREVIOUS_ELECTRON_AMP(-1);

//Controls used to stop spectra
Bool_t STOP_TPC_SPECTRUM(false);
Bool_t STOP_TOF_SPECTRUM(false);

class CurrentBinInfo{

private:
  Int_t particleSpecies;
  Int_t centIndex, yIndex, mTm0Index;

  Double_t yieldPlus, yieldPlusErr;
  Double_t yieldMinus, yieldMinusErr;

public:
  Double_t mass;
  Double_t mTm0;
  Double_t mT;
  Double_t rapidity;
  Double_t pT;
  Double_t pZ;
  Double_t pTotal;

private:
  TH1D *zPlusHisto;
  TH1D *zMinusHisto;

  TF1 *zPlusFit;
  TF1 *zMinusFit;

public:
  CurrentBinInfo(Int_t speciesInterest, Int_t centralityIndex, Int_t rapidityIndex, Int_t transverseMassIndex);
  ~CurrentBinInfo();
  Bool_t SetYieldHistograms(TH1D *plusHisto, TH1D *minusHisto);
  Bool_t SetYieldFits(TF1 *plusFit, TF1 *minusFit);
  void SetYield(Double_t plusYield, Double_t minuYield);
  void SetYieldError(Double_t plusYieldErr, Double_t minusYieldErr);

  Int_t GetParticleOfInterest(){return particleSpecies;}
  Double_t GetMass(){return mass;}
  Double_t GetmTm0(){return mTm0;}
  Double_t GetmT(){return mT;}
  Double_t GetRapidity(){return rapidity;}
  Double_t GetpT(){return pT;}
  Double_t GetpZ(){return pZ;}
  Double_t GetpTotal(){return pTotal;}
  TH1D *GetZPlusHisto(){return zPlusHisto;}
  TH1D *GetZMinusHisto(){return zMinusHisto;}
  TF1  *GetZPlusFit(){return zPlusFit;}
  TF1  *GetZMinusFit(){return zMinusFit;}
  Double_t GetYieldPlus(){return yieldPlus;}
  Double_t GetYieldMinus(){return yieldMinus;}
  Double_t GetYieldPlusError(){return yieldPlusErr;}
  Double_t GetYieldMinusError(){return yieldMinusErr;}

};

CurrentBinInfo::CurrentBinInfo(Int_t speciesInterest, Int_t centralityIndex,
			       Int_t rapidityIndex, Int_t transverseMassIndex){
  particleSpecies = speciesInterest;
  centIndex = centralityIndex;
  yIndex = rapidityIndex;
  mTm0Index = transverseMassIndex;

  zPlusHisto = zMinusHisto = NULL;
  zPlusFit = zMinusFit = NULL;
}

CurrentBinInfo::~CurrentBinInfo(){
  if (zPlusHisto){
    for (int i=0; i<zPlusHisto->GetListOfFunctions()->GetEntries(); i++)
      delete zPlusHisto->GetListOfFunctions()->At(i);
    if (zPlusHisto)
      delete zPlusHisto;
  }
  if (zMinusHisto){
    for (int i=0; i<zMinusHisto->GetListOfFunctions()->GetEntries(); i++)
      delete zMinusHisto->GetListOfFunctions()->At(i);
    if (zMinusHisto)
      delete zMinusHisto;
  }
}

Bool_t CurrentBinInfo::SetYieldHistograms(TH1D *plusHisto, TH1D *minusHisto){
  zPlusHisto = plusHisto;
  zMinusHisto = minusHisto;
  if (!zPlusHisto || !zMinusHisto)
    return false;
  
  return true;
}

Bool_t CurrentBinInfo::SetYieldFits(TF1 *plusFit, TF1 *minusFit){
  zPlusFit = plusFit;
  zMinusFit = minusFit;
  if (!zMinusFit || !zPlusFit)
    return false;
  return true;      
}

void CurrentBinInfo::SetYield(Double_t plusYield, Double_t minusYield){
  yieldPlus=plusYield;
  yieldMinus=minusYield;
}

void CurrentBinInfo::SetYieldError(Double_t plusYieldErr, Double_t minusYieldErr){
  yieldPlusErr=plusYieldErr;
  yieldMinusErr=minusYieldErr;
}

//Globals
CurrentBinInfo *currentBinTPC, *currentBinTOF;
TCanvas *yieldCanvas, *spectraCanvas, *ratioCanvas;
ParticleInfo *particleInfo;

//Other Functions in this file
Bool_t FitZTPC(CurrentBinInfo *binInfo, TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
	       Double_t nEvents, std::vector<TGraphErrors *> *particleMeans,
	       std::vector<TGraphErrors *> *particleWidths);
Bool_t FitZTOF(CurrentBinInfo *binInfo, TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
	       Double_t nEvents, std::vector<TGraphErrors *> *particleMeans,
	       std::vector<TGraphErrors *> *particleWidths);

std::pair<double,double> ComputeYield(TH1D *yieldHisto, TF1 *fit, Int_t particleSpecies,
				      Double_t mT,Double_t nEvents, bool isWidthFixed=true);
std::vector<TF1 *> DrawIndividualFits(TPad *pad, TF1 *fitFunc, int speciesInterest,bool isTof=false);
Double_t ComputeSystematicErrors(CurrentBinInfo *binInfo, Double_t nEvents, Int_t plusOrMinus,
				 std::vector<std::pair<double,double> > particleParamErrs);
			    
void ScaleTofSpectrum(TGraphErrors *tpcSpectrum, TGraphErrors *tofSpectrum);
void RemovePointsAbove(TGraphErrors *graph, Double_t xVal);
void RemovePointsBelow(TGraphErrors *graph, Double_t xVal);
void ConstructSpectraRatio(TGraphErrors *spectraPlus, TGraphErrors *spectraMinus, TGraphErrors *spectraRatio);

//____MAIN____
void GenerateRawSpectra(TString inputYieldHistoFile, TString outputSpectraFile, TString pidCalibrationFile,
			Int_t userSpecies=-999, Int_t userCentIndex=-999, Int_t userRapidityIndex=-999){

  gStyle->SetOptFit(1);

  //Create an instance of Particle Info
  particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibrationFile);

  //Particles Whose yeilds should be extracted
  std::vector<int> particles;
  if (userSpecies != -999 && userSpecies >=0 && userSpecies <= PROTON)
    particles.push_back(userSpecies);
  else {
    particles.push_back(PION);
    particles.push_back(KAON);
    particles.push_back(PROTON);
  }
  
  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Create the Output File
  TFile *outFile = NULL;
  if (outputSpectraFile.CompareTo("")){
    outFile = new TFile(outputSpectraFile,"RECREATE");
    outFile->cd();
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
      outFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      outFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      outFile->mkdir(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      outFile->mkdir(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
    }
  }
  
  //Get the Kinematics Tree
  TFile *pidFile = new TFile(pidCalibrationFile,"READ");
  TTree *kinematicTree = (TTree *)pidFile->Get("TotalMomentum/avgKinematicTree");
  kinematicTree->Draw(">>particleEntryList",Form("particleSpeciesIndex<=%d",PROTON));
  TEventList *particleEntryList = (TEventList *)gDirectory->Get("particleEntryList");
  kinematicTree->SetEventList(particleEntryList);
  
  //Create Canvases
  yieldCanvas = new TCanvas("fitCanvas","fitCanvas",20,20,1800,500);
  yieldCanvas->Divide(4,1);
  yieldCanvas->cd(1);
  gPad->SetLogy();
  yieldCanvas->cd(2);
  gPad->SetLogy();
  yieldCanvas->cd(3);
  gPad->SetLogy();
  yieldCanvas->cd(4);
  gPad->SetLogy();


  spectraCanvas = new TCanvas("spectraCanvas","spectraCanvas",20,600,800,600);
  spectraCanvas->SetLogy();
  TH1F *spectraFrame = NULL;

  ratioCanvas = new TCanvas("ratioCanvas","ratioCanvas",900,600,800,600);
  TH1F *ratioFrame = NULL;
  
  std::vector<TGraphErrors *> spectraPlus(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraMinus(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraRatio(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraPlusTOF(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraMinusTOF(nRapidityBins,(TGraphErrors *)NULL);
  
  //Open the input Yield Histogram file
  TFile *yieldHistoFile = new TFile(inputYieldHistoFile,"READ");

  //Get the Number of Events Histo
  TH1D *nEventsHisto = (TH1D *)yieldHistoFile->Get("nEvents");

  //Yield Histograms
  TH3D *zTPCPlusHisto3D, *zTPCMinusHisto3D;
  TH3D *zTOFPlusHisto3D, *zTOFMinusHisto3D;

  std::vector<std::vector<TH1D *> > zTPCPlusHisto (nRapidityBins, std::vector<TH1D *>
						   (nmTm0Bins, (TH1D *)NULL));
  std::vector<std::vector<TH1D *> > zTPCMinusHisto (nRapidityBins, std::vector<TH1D *>
						    (nmTm0Bins, (TH1D *)NULL));
  std::vector<std::vector<TH1D *> > zTOFPlusHisto (nRapidityBins, std::vector<TH1D *>
						   (nmTm0Bins, (TH1D *)NULL));
  std::vector<std::vector<TH1D *> > zTOFMinusHisto (nRapidityBins, std::vector<TH1D *>
						    (nmTm0Bins, (TH1D *)NULL));

  std::vector<TGraphErrors *> particleMeans (3,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> particleWidths(3,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> particleMeansTOF (3,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> particleWidthsTOF (3,(TGraphErrors *)NULL);
  
  std::vector<TF1 *> particleWidthsFitsTOF (3, (TF1 *)NULL);
  std::vector<TGraphErrors *> particleWidthConfTOF(3, (TGraphErrors *)NULL);
  
  //Pointers to curent histograms, Prediction Lines, and Fit Functions
  std::vector<double>  zTOFPrediction(3);
  std::vector<TLine *> predictionLineTOF(3);
  TF1 *zTPCPlusFit, *zTPCMinusFit;

  //Set Line Styles
  predictionLineTOF.at(0) = new TLine();
  predictionLineTOF.at(1) = new TLine();
  predictionLineTOF.at(2) = new TLine();
  predictionLineTOF.at(0)->SetLineColor(particleInfo->GetParticleColor(PION));
  predictionLineTOF.at(1)->SetLineColor(particleInfo->GetParticleColor(KAON));
  predictionLineTOF.at(2)->SetLineColor(particleInfo->GetParticleColor(PROTON));
  predictionLineTOF.at(0)->SetLineWidth(3);
  predictionLineTOF.at(1)->SetLineWidth(3);
  predictionLineTOF.at(2)->SetLineWidth(3);
  
  //Loop Over the Particles, Centrality Bins, Rapidity Bins, and mTm0 Bins and extract the yield
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

    int particleSpecies = particles.at(iParticle);

    if (outFile){
      outFile->cd();
    }

    Double_t yMin(0.05), yMax(1000);
    if (particleSpecies == PROTON){
      yMin = 0.001;
      yMax = 200;
    }
    else if (particleSpecies == KAON){
      yMin = 0.01;
      yMax = 500;
    }
    spectraCanvas->cd();
    spectraFrame = spectraCanvas->DrawFrame(0,yMin,MAX_MTM0+.1,yMax);
    spectraCanvas->Update();

    ratioCanvas->cd();
    ratioFrame = ratioCanvas->DrawFrame(0,0,MAX_MTM0+.1,1.2);
    ratioCanvas->Update();

    for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

      //If the user has requested a (reasonable) specific bin then skip others
      if (userCentIndex >= 0 && userCentIndex < (int)nCentralityBins && userCentIndex != (int)iCentBin)
	continue;

      //Get the 3D Yield Histos
      zTPCPlusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTPC_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
		 iCentBin));
      zTPCMinusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTPC_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
		 iCentBin));
      zTOFPlusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTOF_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
		 iCentBin));
      zTOFMinusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTOF_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
		 iCentBin));

      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

	//Skip Uninteresting rapidity bins
	if ((int)yIndex < GetMinRapidityIndexOfInterest() || (int)yIndex > GetMaxRapidityIndexOfInterest())
	  continue;
	
	//Skip rapidity bins that were not requested
	if (userRapidityIndex >=0 && (int)yIndex != userRapidityIndex)
	  continue;

	//Make the Total Yield 1D Histos
	LoadYieldHistograms(zTPCPlusHisto3D,&zTPCPlusHisto,"TPC",yIndex);
	LoadYieldHistograms(zTPCMinusHisto3D,&zTPCMinusHisto,"TPC",yIndex);
	LoadYieldHistograms(zTOFPlusHisto3D,&zTOFPlusHisto,"TOF",yIndex);
	LoadYieldHistograms(zTOFMinusHisto3D,&zTOFMinusHisto,"TOF",yIndex);

	//Set the Empirical Bichsel Function to be used. If it fails it means
	//that one does not exist so skip the rapidity bin
	if (!particleInfo->SetEmpiricalBichselFunction(iParticle,yIndex))
	  continue;

	//Get the Particle Means and Widths for this rapidity bin
	for (unsigned int i=0; i<particleMeans.size(); i++){
	  particleMeans.at(i) = (TGraphErrors *)pidFile->
	    Get(Form("MeanAndWidthGraphs/meanGraph_%s_%s_%d",
		     particleInfo->GetParticleName(particleSpecies).Data(),
		     particleInfo->GetParticleName(i).Data(),
		     yIndex));
	  particleWidths.at(i) = (TGraphErrors *)pidFile->
	    Get(Form("MeanAndWidthFits/widthGraph_%s_%s_%d_Conf",
		     particleInfo->GetParticleName(particleSpecies).Data(),
		     particleInfo->GetParticleName(i).Data(),
		     yIndex));
	  
	  particleMeansTOF.at(i) = (TGraphErrors *)pidFile->
	    Get(Form("MeanAndWidthGraphs/meanGraphTOF_%s_%s_%d",
		     particleInfo->GetParticleName(particleSpecies).Data(),
		     particleInfo->GetParticleName(i).Data(),
		     yIndex));
	  particleWidthsTOF.at(i) = (TGraphErrors *)pidFile->
	    Get(Form("MeanAndWidthFits/widthGraphTOF_%s_%s_%d_Conf",
		     particleInfo->GetParticleName(particleSpecies).Data(),
		     particleInfo->GetParticleName(i).Data(),
		     yIndex));

	}
	
	//Create the Spectrum
	if (outFile)
	  outFile->cd();
	spectraPlus.at(yIndex) = new TGraphErrors();
	spectraMinus.at(yIndex) = new TGraphErrors();
	spectraPlusTOF.at(yIndex) = new TGraphErrors();
	spectraMinusTOF.at(yIndex) = new TGraphErrors();
	spectraRatio.at(yIndex) = new TGraphErrors();

	STOP_TPC_SPECTRUM = false;
	STOP_TOF_SPECTRUM = false;	

	spectraPlus.at(yIndex)->SetName(Form("rawSpectra_%s_Cent%02d_yIndex%02d",
					     particleInfo->GetParticleName(particleSpecies,1).Data(),
					     iCentBin,yIndex));
	spectraMinus.at(yIndex)->SetName(Form("rawSpectra_%s_Cent%02d_yIndex%02d",
					      particleInfo->GetParticleName(particleSpecies,-1).Data(),
					     iCentBin,yIndex));
	spectraPlusTOF.at(yIndex)->SetName(Form("rawSpectraTOF_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(particleSpecies,1).Data(),
						iCentBin,yIndex));
	spectraMinusTOF.at(yIndex)->SetName(Form("rawSpectraTOF_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(particleSpecies,-1).Data(),
						iCentBin,yIndex));
	spectraRatio.at(yIndex)->SetName(Form("rawSpectraRatio_%s_Cent%02d_yIndex%02d",
					      particleInfo->GetParticleName(particleSpecies).Data(),
					      iCentBin,yIndex));
	spectraPlus.at(yIndex)->SetTitle(Form("Uncorrected TPC Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					      particleInfo->GetParticleSymbol(particleSpecies,1).Data(),
					      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					      (int)GetCentralityPercents().at(iCentBin),
					      particleInfo->GetParticleSymbol(particleSpecies).Data(),
					      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					      particleInfo->GetParticleSymbol(particleSpecies).Data()));
	spectraMinus.at(yIndex)->SetTitle(Form("Uncorrected TPC Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					       particleInfo->GetParticleSymbol(particleSpecies,-1).Data(),
					       iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					       (int)GetCentralityPercents().at(iCentBin),
					       particleInfo->GetParticleSymbol(particleSpecies).Data(),
					       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					       particleInfo->GetParticleSymbol(particleSpecies).Data()));
	spectraPlusTOF.at(yIndex)->SetTitle(Form("Uncorrected TOF Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					      particleInfo->GetParticleSymbol(particleSpecies,1).Data(),
					      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					      (int)GetCentralityPercents().at(iCentBin),
					      particleInfo->GetParticleSymbol(particleSpecies).Data(),
					      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					      particleInfo->GetParticleSymbol(particleSpecies).Data()));
	spectraMinusTOF.at(yIndex)->SetTitle(Form("Uncorrected TOF Spectrum %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};",
					       particleInfo->GetParticleSymbol(particleSpecies,-1).Data(),
					       iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					       (int)GetCentralityPercents().at(iCentBin),
					       particleInfo->GetParticleSymbol(particleSpecies).Data(),
					       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					       particleInfo->GetParticleSymbol(particleSpecies).Data()));

	spectraRatio.at(yIndex)->SetTitle(Form("Ratio %s/%s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s};%s/%s",
					       particleInfo->GetParticleSymbol(particleSpecies,1).Data(),
					       particleInfo->GetParticleSymbol(particleSpecies,-1).Data(),
					       iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					       (int)GetCentralityPercents().at(iCentBin),
					       particleInfo->GetParticleSymbol(particleSpecies).Data(),
					       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					       particleInfo->GetParticleSymbol(particleSpecies).Data(),
					       particleInfo->GetParticleSymbol(particleSpecies,1).Data(),
					       particleInfo->GetParticleSymbol(particleSpecies,-1).Data()));
	
	spectraPlus.at(yIndex)->SetMarkerStyle(kFullCircle);
	spectraMinus.at(yIndex)->SetMarkerStyle(kOpenCircle);
	spectraRatio.at(yIndex)->SetMarkerStyle(kFullCircle);
	spectraPlusTOF.at(yIndex)->SetMarkerStyle(kFullSquare);
	spectraMinusTOF.at(yIndex)->SetMarkerStyle(kOpenSquare);

	spectraPlus.at(yIndex)->SetMarkerColor(particleInfo->GetParticleColor(particleSpecies));
	spectraMinus.at(yIndex)->SetMarkerColor(particleInfo->GetParticleColor(particleSpecies));
	spectraRatio.at(yIndex)->SetMarkerColor(particleInfo->GetParticleColor(particleSpecies));
	spectraPlusTOF.at(yIndex)->SetMarkerColor(particleInfo->GetParticleColor(particleSpecies));
	spectraMinusTOF.at(yIndex)->SetMarkerColor(particleInfo->GetParticleColor(particleSpecies));
	
	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	  if (STOP_TPC_SPECTRUM && STOP_TOF_SPECTRUM){
	    cout <<"Stopping Spectrum" <<endl;
	    break;
	  }

	  cout <<"Fitting: " <<"Centrality Bin: " <<iCentBin <<" "
	       <<"Rapidity Bin: " <<yIndex <<" " <<"mT-m0 Index: " <<mTm0Index <<endl;
	  
	  //Set the Current Bin Attributes
	  currentBinTPC = new CurrentBinInfo(particleSpecies,iCentBin,yIndex,mTm0Index);
	  currentBinTOF = new CurrentBinInfo(particleSpecies,iCentBin,yIndex,mTm0Index);
	  currentBinTPC->SetYieldHistograms(zTPCPlusHisto.at(yIndex).at(mTm0Index),
					    zTPCMinusHisto.at(yIndex).at(mTm0Index));
	  currentBinTOF->SetYieldHistograms(zTOFPlusHisto.at(yIndex).at(mTm0Index),
					    zTOFMinusHisto.at(yIndex).at(mTm0Index));
	  currentBinTPC->mass = particleInfo->GetParticleMass(particleSpecies);
	  currentBinTOF->mass = particleInfo->GetParticleMass(particleSpecies);

	  /*
	  //Get the Avg Total Momentum of the Tracks in this bin from the kinematic Tree
	  kinematicTree->Draw("avgTotalP:avgRapidity:avgmTm0",
			      Form("centBinIndex==%d&&particleSpeciesIndex==%d&&yIndex==%d&&mTm0Index==%d",
				   iCentBin,particleSpecies,yIndex,mTm0Index),"goff");
	  Long64_t nVals = kinematicTree->GetSelectedRows();
	  Double_t *pTotalAvgVals = kinematicTree->GetV1();
	  Double_t *rapAvgVals = kinematicTree->GetV2();
	  Double_t *mTm0AvgVals = kinematicTree->GetV3();
	  
	  currentBinTPC->mTm0 = TMath::Mean(nVals,mTm0AvgVals);
	  currentBinTPC->mT = currentBinTPC->mTm0 + currentBinTPC->mass;
	  currentBinTPC->rapidity = TMath::Mean(nVals,rapAvgVals);
	  currentBinTPC->pTotal = TMath::Mean(nVals,pTotalAvgVals);
	  */

	  currentBinTPC->mTm0 = GetmTm0RangeCenter(mTm0Index);
	  currentBinTPC->mT   = currentBinTPC->GetmTm0() + currentBinTPC->GetMass();
	  currentBinTPC->rapidity = GetRapidityRangeCenter(yIndex);
	  currentBinTPC->pT = ConvertmTm0ToPt(currentBinTPC->GetmTm0(),currentBinTPC->GetMass());
	  currentBinTPC->pZ = ComputepZ(currentBinTPC->GetmT(),currentBinTPC->GetRapidity());
	  currentBinTPC->pTotal = ComputepTotal(currentBinTPC->GetpT(),currentBinTPC->GetpZ());
	  
	  currentBinTOF->mTm0 = currentBinTPC->GetmTm0();
	  currentBinTOF->mT   = currentBinTPC->GetmT();
	  currentBinTOF->rapidity = currentBinTPC->GetRapidity();
	  currentBinTOF->pT = currentBinTPC->GetpT();
	  currentBinTOF->pZ = currentBinTPC->GetpZ();
	  currentBinTOF->pTotal = currentBinTPC->GetpTotal();

	  if (TMath::IsNaN(currentBinTPC->pTotal) || TMath::IsNaN(currentBinTPC->mTm0) ||
	      TMath::IsNaN(currentBinTPC->rapidity)){
	    delete currentBinTPC;
	    delete currentBinTOF;
	    continue;
	  }

	  if (currentBinTPC->GetmTm0() > MAX_MTM0){
	    delete currentBinTPC;
	    delete currentBinTOF;
	    break;
	  }

	  //--- Get the Number of Events in this Centrality Bin ---
	  Double_t nEvents  = (double)nEventsHisto->GetBinContent(nEventsHisto->FindBin(iCentBin));

	  //Controls to determine if the fits get saved
	  Bool_t saveFitTPC=false;
	  Bool_t saveFitTOF=false;
	  
	  //--- Fit the ZTPC Yield Histograms
	  if (!STOP_TPC_SPECTRUM){
	    
	    //Make sure we have what we need, if not skip
	    Bool_t doTPCFit = true;
	    for (unsigned int i=0; i<particleMeans.size(); i++){
	      if (!particleMeans.at(i) || !particleWidths.at(i)){
		doTPCFit = false;
		break;
	      }
	      if (particleMeans.at(i)->GetN() < 3 || particleWidths.at(i)->GetN() < 3){
		doTPCFit = false;
		break;
	      }
	    }

	    if (doTPCFit)
	      saveFitTPC = FitZTPC(currentBinTPC,spectraPlus.at(yIndex),spectraMinus.at(yIndex),
					nEvents,&particleMeans,&particleWidths);

	  }//End TPC Fit
	  
	  
	  //--- Fit the ZTOF Yield Histograms
	  if (!STOP_TOF_SPECTRUM){

	    //Make sure we have what we need
	    Bool_t doTOFFit = true;
	    for (unsigned int i=0; i<particleMeans.size(); i++){
	      if (!particleMeansTOF.at(i) || !particleWidthsTOF.at(i)){
		doTOFFit = false;
		break;
	      }
	      if (particleMeansTOF.at(i)->GetN() < 3 || particleWidthsTOF.at(i)->GetN() < 3){
		doTOFFit = false;
		break;
	      }
	    }
	    
	    if (doTOFFit)
	      saveFitTOF = FitZTOF(currentBinTOF,spectraPlusTOF.at(yIndex),spectraMinusTOF.at(yIndex),
				   nEvents,&particleMeansTOF,&particleWidthsTOF);
	    
	  }//End TOF FIT

	  //Stop the tpc Spectrum if the tof spectrum has been stopped
	  if (STOP_TOF_SPECTRUM)
	    STOP_TPC_SPECTRUM = true;
	  
	  
	  //Save Yield Histograms and Fits
	  if (outFile){
	    
	    outFile->cd();
	    outFile->cd(Form("YieldExtractionFits_%s",
			     particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
	    if (saveFitTPC){
	      currentBinTPC->GetZPlusHisto()->Write();
	    }
	    if (saveFitTOF){
	      currentBinTOF->GetZPlusHisto()->Write();
	    }

	    outFile->cd();
	    outFile->cd(Form("YieldExtractionFits_%s",
			     particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
	    if (saveFitTPC){
	      currentBinTPC->GetZMinusHisto()->Write();
	    }
	    if (saveFitTOF){
	      currentBinTOF->GetZMinusHisto()->Write();
	    }
	    
	  }
	  

	  //Clean Up
	  if (currentBinTPC)
	    delete currentBinTPC;
	  if (currentBinTOF)
	    delete currentBinTOF;

	}//End Loop Over mTm0 Bins

	//Check that the tpc spectra exist
	if (!spectraPlus.at(yIndex))
	  continue;
	if (spectraPlus.at(yIndex)->GetN() < 3)
	  continue;
	
	//Overall TOF Matching Efficiency Correction
	cout <<"Performing overall TOF Matching Efficiency Correction\n";
	if (spectraPlusTOF.at(yIndex))
	  ScaleTofSpectrum(spectraPlus.at(yIndex),spectraPlusTOF.at(yIndex));
	if (spectraMinusTOF.at(yIndex))
	  ScaleTofSpectrum(spectraMinus.at(yIndex),spectraMinusTOF.at(yIndex));

	//Check the stitch value
	Double_t maxTPC = spectraPlus.at(yIndex)->GetX()[TMath::Max(0,spectraPlus.at(yIndex)->GetN()-1)];
	Double_t stitchVal = TPC_TOF_STITCH;
	if (maxTPC < TPC_TOF_STITCH)
	  stitchVal = maxTPC;
	
	//Remove Points from the TPC Spectrum that are above the stitch value
	cout <<"Removing Points from the TPC spectrum that are above the stitch value\n";
	if (spectraPlus.at(yIndex))
	  RemovePointsAbove(spectraPlus.at(yIndex),stitchVal);
	if (spectraMinus.at(yIndex))
	  RemovePointsAbove(spectraMinus.at(yIndex),stitchVal);

	//Remove Points from the TOF Spectrum that are below the stitch value
	cout <<"Removing points from the TOF Spectrum that are below the stitch value\n";
	if (spectraPlusTOF.at(yIndex))
	  RemovePointsBelow(spectraPlusTOF.at(yIndex),stitchVal);
	if (spectraMinusTOF.at(yIndex))
	  RemovePointsBelow(spectraMinusTOF.at(yIndex),stitchVal);

	spectraCanvas->cd();
	spectraPlus.at(yIndex)->Draw("PZ");
	spectraPlusTOF.at(yIndex)->Draw("PZ");
	gPad->Update();
	spectraMinus.at(yIndex)->Draw("PZ");
	spectraMinusTOF.at(yIndex)->Draw("PZ");


	//Construct the Ratio of the Spectra
	ConstructSpectraRatio(spectraPlus.at(yIndex),spectraMinus.at(yIndex),spectraRatio.at(yIndex));
	ConstructSpectraRatio(spectraPlusTOF.at(yIndex),spectraMinusTOF.at(yIndex),spectraRatio.at(yIndex));

	ratioCanvas->cd();
	spectraRatio.at(yIndex)->Draw("PZ");
	gPad->Update();

	//gSystem->Sleep(5000);
	
	//Save Spectra
	if(outFile){
	  outFile->cd();
	  outFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(particleSpecies,1).Data()));

	  spectraPlus.at(yIndex)->Write();
	  spectraPlusTOF.at(yIndex)->Write();

	  delete spectraPlus.at(yIndex);
	  delete spectraPlusTOF.at(yIndex);
	  
	  outFile->cd();
	  gDirectory->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(particleSpecies,-1).Data()));
	  spectraMinus.at(yIndex)->Write();	  
	  spectraMinusTOF.at(yIndex)->Write();

	  delete spectraMinus.at(yIndex);
	  delete spectraMinusTOF.at(yIndex);	  
	}

	//Clean Up
	for (unsigned int i=0; i<particleMeans.size(); i++){
	  delete particleMeans.at(i);
	  delete particleWidths.at(i);
	}
	//These are deleted as part of the CurrentBinInfo destructor
	for (unsigned int i=0; i<nRapidityBins; i++){
	  for (unsigned int j=0; j<nmTm0Bins; j++){

	    if (zTPCPlusHisto.at(i).at(j))
	      zTPCPlusHisto.at(i).at(j) = NULL;
	    if (zTPCMinusHisto.at(i).at(j))
	      zTPCMinusHisto.at(i).at(j) = NULL;
	    if (zTOFPlusHisto.at(i).at(j))
	      zTOFPlusHisto.at(i).at(j) = NULL;
	    if (zTOFMinusHisto.at(i).at(j))
	      zTOFMinusHisto.at(i).at(j) = NULL;
	    
	  }
	}
	
      }//End Loop Over Rapidity Bins

      //Clean Up
      if (zTPCPlusHisto3D)
	delete zTPCPlusHisto3D;
      if (zTPCMinusHisto3D)
	delete zTPCMinusHisto3D;
      if (zTOFPlusHisto3D)
	delete zTOFPlusHisto3D;
      if (zTOFMinusHisto3D)
	delete zTOFMinusHisto3D;
      
    }//End Loop Over Centrality Bins
  }//End Loop Over Particles

  if (outFile)
    outFile->Close();
  

}


//_______________________________________________________________
Bool_t FitZTPC(CurrentBinInfo *binInfo, TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
	       Double_t nEvents, std::vector<TGraphErrors *> *particleMeans,
	       std::vector<TGraphErrors *> *particleWidths){

  //Skip this min if its mTm0 is less than the desired minimum
  if (binInfo->GetmTm0() < MIN_MTM0)
    return false;
  
  //Skip this bin if it does not have enough entries. If the Bin's mTm0 is greater than .1
  //then set the global control to stop the spectrum
  if (binInfo->GetZPlusHisto()->GetEntries() < 100 || binInfo->GetZMinusHisto()->GetEntries() < 100){
    if (binInfo->GetmTm0() > .1)
      STOP_TPC_SPECTRUM = true;
    return false;
    
  }
  
  //Get the Particle Means
  std::vector<double> means;
  means.push_back(particleMeans->at(PION)->Eval(binInfo->GetmTm0()));
  means.push_back(particleMeans->at(KAON)->Eval(binInfo->GetmTm0()));
  means.push_back(particleMeans->at(PROTON)->Eval(binInfo->GetmTm0()));
  means.push_back(particleInfo->PredictZTPC(binInfo->GetpTotal(),
					    binInfo->GetParticleOfInterest(),ELECTRON));

  //Get the Particle Widths
  std::vector<double> widths;
  widths.push_back(particleWidths->at(PION)->Eval(binInfo->GetmTm0()));
  widths.push_back(particleWidths->at(KAON)->Eval(binInfo->GetmTm0()));
  widths.push_back(particleWidths->at(PROTON)->Eval(binInfo->GetmTm0()));
  widths.push_back(particleWidths->at(PION)->Eval(binInfo->GetmTm0())); //Assume width of pion for electrons

  //Check for Overlaps between the particle of interest and confounding particles
  Double_t widthFactor(.25);
  Bool_t stopSpectrum = false;
  Double_t minDistance(100);
  for (unsigned int i=0; i<PROTON; i++){

    if ((int)i == binInfo->GetParticleOfInterest())
      continue;

    Double_t distance = fabs(means.at(binInfo->GetParticleOfInterest())-means.at(i));
    if (distance < minDistance)
      minDistance = distance;
    
    if (distance < widthFactor * (widths.at(binInfo->GetParticleOfInterest()) + widths.at(i))){
      stopSpectrum = true;
      break;
    }
  }
  if (stopSpectrum && binInfo->GetmTm0() > 0.3){
    STOP_TPC_SPECTRUM = true;
    return false;
  }
  else if (stopSpectrum)
    return false;

  /*
  //If the Min Distance is less than 3 sigma, fix the width of
  //the particle of interest below
  Bool_t fixWidth = true;
  if (minDistance > 3 * widths.at(binInfo->GetParticleOfInterest()))
    fixWidth = false;
  */
  
  //Determine the Fitting Range
  Double_t minFitRange = TMath::Min(means.at(PION),means.at(PROTON)) - 0.7;
  Double_t maxFitRange = TMath::Max(means.at(PION),means.at(PROTON)) + 0.7;
  
  
  //Draw the Histograms
  binInfo->GetZPlusHisto()->GetXaxis()->SetRangeUser(minFitRange-.2,maxFitRange+.2);
  binInfo->GetZMinusHisto()->GetXaxis()->SetRangeUser(minFitRange-.2,maxFitRange+.2);
  yieldCanvas->cd(1);
  binInfo->GetZPlusHisto()->Draw("E");
  yieldCanvas->cd(2);
  binInfo->GetZMinusHisto()->Draw("E");

  yieldCanvas->Update();

  //We'll use either a three or four gaussian fit where the fourth
  //gaussian, if it is used, will always be for the electron.
  Int_t whichFitForm = 0;
  TString fitForm[2] = {"gaus(0)+gaus(3)+gaus(6)+gaus(9)","gaus(0)+gaus(3)+gaus(6)"};

  //For The yield Extraction of Pions, we'll always use a four gaussian fit
  if (binInfo->GetParticleOfInterest() == PION)
    whichFitForm = 0;
  //For Kaons we'll also always use the electrons so a four gaussian fit
  if (binInfo->GetParticleOfInterest() == KAON)
    whichFitForm = 0;
  //For Protons we'll never use the electron so always a three gaussian fit
  if (binInfo->GetParticleOfInterest() == PROTON)
    whichFitForm = 1;
  
  //Create the Fit Function
  binInfo->SetYieldFits(new TF1("",fitForm[whichFitForm],minFitRange,maxFitRange),
			new TF1("",fitForm[whichFitForm],minFitRange,maxFitRange));

  binInfo->GetZPlusFit()->SetNpx(10000);
  binInfo->GetZMinusFit()->SetNpx(10000);
 
  binInfo->GetZPlusFit()->SetLineColor(kBlack);
  binInfo->GetZMinusFit()->SetLineColor(kBlack);
  
  binInfo->GetZPlusFit()->SetLineWidth(3);
  binInfo->GetZMinusFit()->SetLineWidth(3);

  binInfo->GetZPlusFit()->SetName(Form("%s_FullFit",binInfo->GetZPlusHisto()->GetName()));
  binInfo->GetZMinusFit()->SetName(Form("%s_FullFit",binInfo->GetZMinusHisto()->GetName()));

  //Set Fit Parameters
  for (int iPar=0; iPar<binInfo->GetZPlusFit()->GetNpar(); iPar+=3){
    binInfo->GetZPlusFit()->SetParameter(iPar,binInfo->GetZPlusHisto()->
					 GetBinContent(binInfo->GetZPlusHisto()->
						       FindBin(means.at(iPar/3))));
    binInfo->GetZPlusFit()->SetParLimits(iPar,0,binInfo->GetZPlusFit()->GetParameter(iPar)*2.0);
    binInfo->GetZPlusFit()->FixParameter(iPar+1,means.at(iPar/3));
    binInfo->GetZPlusFit()->FixParameter(iPar+2,widths.at(iPar/3));
    
    binInfo->GetZMinusFit()->SetParameter(iPar,binInfo->GetZMinusHisto()->
					 GetBinContent(binInfo->GetZMinusHisto()->
						       FindBin(means.at(iPar/3))));
    binInfo->GetZMinusFit()->SetParLimits(iPar,0,binInfo->GetZMinusFit()->GetParameter(iPar)*2.0);
    binInfo->GetZMinusFit()->FixParameter(iPar+1,means.at(iPar/3));
    binInfo->GetZMinusFit()->FixParameter(iPar+2,widths.at(iPar/3));
  }

  //Add Additional Boundries for the Electron Aplidtude
  if (binInfo->GetZPlusFit()->GetNpar() > 9){
    binInfo->GetZPlusFit()->SetParameter(9,binInfo->GetZPlusFit()->GetParameter(0)*0.05);
    binInfo->GetZMinusFit()->SetParameter(9,binInfo->GetZMinusFit()->GetParameter(0)*0.05);
    binInfo->GetZPlusFit()->SetParLimits(9,0,binInfo->GetZPlusFit()->GetParameter(9)*1.1);
    binInfo->GetZMinusFit()->SetParLimits(9,0,binInfo->GetZMinusFit()->GetParameter(9)*1.1);
    if (PREVIOUS_ELECTRON_AMP > 0 && binInfo->GetmTm0() > .125){
      binInfo->GetZPlusFit()->SetParameter(9,.8*PREVIOUS_ELECTRON_AMP);
      binInfo->GetZMinusFit()->SetParameter(9,.8*PREVIOUS_ELECTRON_AMP);
      binInfo->GetZPlusFit()->SetParLimits(9,.2*PREVIOUS_ELECTRON_AMP,PREVIOUS_ELECTRON_AMP*.95);
      binInfo->GetZMinusFit()->SetParLimits(9,.2*PREVIOUS_ELECTRON_AMP,PREVIOUS_ELECTRON_AMP*.95);
    }
  }

  /*
  //Unfix the width if requested
  if (!fixWidth){
    binInfo->GetZPlusFit()->SetParLimits(binInfo->GetParticleOfInterest()*3+2,.06,.1);
    binInfo->GetZMinusFit()->SetParLimits(binInfo->GetParticleOfInterest()*3+2,.06,.1);
  }
  */
  
  //Perform the Fits, catched failed fit and try again for up to five times
  int fitStatus(-1);
  int nAttempts(0);
  while (fitStatus != 0 && nAttempts < 5){
    fitStatus = binInfo->GetZPlusHisto()->Fit(binInfo->GetZPlusFit(),"RL");
    nAttempts++;
  }

  fitStatus = -1;
  nAttempts = 0;
  while (fitStatus != 0 && nAttempts < 5){
    fitStatus = binInfo->GetZMinusHisto()->Fit(binInfo->GetZMinusFit(),"RL");
    nAttempts++;
  }
  
  //------ Draw Individual Fits ------
  yieldCanvas->cd(1);
  std::vector<TF1 *> indivPlusFits =
    DrawIndividualFits((TPad *)gPad,binInfo->GetZPlusFit(),binInfo->GetParticleOfInterest());
  yieldCanvas->cd(2);
  std::vector <TF1 *> indivMinusFits = 
    DrawIndividualFits((TPad *)gPad,binInfo->GetZMinusFit(),binInfo->GetParticleOfInterest());
  
  for (unsigned int i=0; i<indivPlusFits.size(); i++){
    binInfo->GetZPlusHisto()->GetListOfFunctions()->Add(indivPlusFits.at(i));
  }
  for (unsigned int i=0; i<indivMinusFits.size(); i++){
    binInfo->GetZMinusHisto()->GetListOfFunctions()->Add(indivMinusFits.at(i));
  }
  
  yieldCanvas->cd(1);
  binInfo->GetZPlusFit()->Draw("SAME");
  gPad->Update();
  yieldCanvas->cd(2);
  binInfo->GetZMinusFit()->Draw("SAME");
  gPad->Update();

  //----- Extract the Yield -----  
  std::pair<double,double> yieldPlusResult =
    ComputeYield(binInfo->GetZPlusHisto(),binInfo->GetZPlusFit(),
		 binInfo->GetParticleOfInterest(),binInfo->GetmT(),nEvents);
  Double_t yieldPlus = yieldPlusResult.first;
  Double_t yieldPlusErr = yieldPlusResult.second;
  
  std::pair<double,double> yieldMinusResult =
    ComputeYield(binInfo->GetZMinusHisto(),binInfo->GetZMinusFit(),
		 binInfo->GetParticleOfInterest(),binInfo->GetmT(),nEvents);
  Double_t yieldMinus = yieldMinusResult.first;
  Double_t yieldMinusErr = yieldMinusResult.second;
  
  binInfo->SetYield(yieldPlus,yieldMinus);

  //----- Systematic Errors -----

  //Set the Parameter Error range of variation
  //All means are varied by 5%
  //Widths are varied according to their confidence intervals,
  //except for electron width, which is varied by 5%
  std::vector<std::pair<double,double> > particleParamErrs;
  for (int i=0; i<binInfo->GetZPlusFit()->GetNpar()/3; i++){
    particleParamErrs.push_back(std::make_pair(0,0));
    particleParamErrs.back().first = means.at(i)*.05;
    if (i<ELECTRON)
      particleParamErrs.back().second = GetErrorAtValue(particleWidths->at(i),binInfo->GetmTm0());
    else
      particleParamErrs.back().second = widths.at(ELECTRON)*.05;
    
  }
  
  Double_t systematicErrPlus = ComputeSystematicErrors(binInfo,nEvents,1,particleParamErrs);
  Double_t systematicErrMinus = ComputeSystematicErrors(binInfo,nEvents,-1,particleParamErrs);

 
  //----- Total Error - Quad sum of stat and sys -------
  Double_t totalErrorPlus = sqrt(pow(yieldPlusErr,2)+pow(systematicErrPlus,2));
  Double_t totalErrorMinus = sqrt(pow(yieldMinusErr,2)+pow(systematicErrMinus,2));

  //----- Add point To Spectrum -----
  spectraPlus->SetPoint(spectraPlus->GetN(),binInfo->mTm0,yieldPlus);
  spectraPlus->SetPointError(spectraPlus->GetN()-1,0,totalErrorPlus);
  spectraMinus->SetPoint(spectraMinus->GetN(),binInfo->mTm0,yieldMinus);
  spectraMinus->SetPointError(spectraMinus->GetN()-1,0,totalErrorMinus);

  spectraCanvas->cd();
  spectraPlus->Draw("P");
  spectraMinus->Draw("P");
  spectraCanvas->Update();

  PREVIOUS_ELECTRON_AMP = binInfo->GetZMinusFit()->GetParameter(9);

  return true; //Save
}

//____________________________________________________________________________________________
Bool_t FitZTOF(CurrentBinInfo *binInfo, TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
	       Double_t nEvents, std::vector<TGraphErrors *> *particleMeans,
	       std::vector<TGraphErrors *> *particleWidths){

  //Skip this bin if its mTm0 is too low or high
  if (binInfo->GetmTm0() < MIN_MTM0)
    return false;
  if (binInfo->GetmTm0() > MAX_MTM0){
    STOP_TOF_SPECTRUM = true; //Stop the Spectrum
    return false;
  }
  
  //Skip This bin if it does not have enough entries
  if (binInfo->GetZPlusHisto()->GetEntries() < 100 || binInfo->GetZMinusHisto()->GetEntries() < 100){
    if (binInfo->GetmTm0() > .6)
      STOP_TOF_SPECTRUM = true;
    return false;
  }

  //Get the Particle Means
  std::vector<double> means;
  means.push_back(particleMeans->at(PION)->Eval(binInfo->GetmTm0()));
  means.push_back(particleMeans->at(KAON)->Eval(binInfo->GetmTm0()));
  means.push_back(particleMeans->at(PROTON)->Eval(binInfo->GetmTm0()));


  //Get the Particle Widths
  std::vector<double> widths;
  widths.push_back(particleWidths->at(PION)->Eval(binInfo->GetmTm0()));
  widths.push_back(particleWidths->at(KAON)->Eval(binInfo->GetmTm0()));
  widths.push_back(particleWidths->at(PROTON)->Eval(binInfo->GetmTm0()));

  //Check for Overalps between the particle of interest and the confounding particles
  Double_t widthFactor(1.5);
  Bool_t stopSpectrum = false;
  for (unsigned int i=0; i<means.size(); i++){
    
    if ((int)i == binInfo->GetParticleOfInterest())
      continue;
    
    if (fabs(means.at(binInfo->GetParticleOfInterest())-means.at(i)) <
	widthFactor * (widths.at(binInfo->GetParticleOfInterest()) + widths.at(i))){
      stopSpectrum = true;
      break;
    }
  }
  if (stopSpectrum && binInfo->GetmTm0() > TPC_TOF_STITCH){
    STOP_TOF_SPECTRUM = true;
    return false;
  }
  else if (stopSpectrum)
    return false;

  //Determine the Fitting Range
  Double_t minFitRange = TMath::Min(means.at(PION),means.at(PROTON)) - 0.08;
  Double_t maxFitRange = TMath::Max(means.at(PION),means.at(PROTON)) + 0.08;
  
  //Draw the Histograms
  binInfo->GetZPlusHisto()->GetXaxis()->SetRangeUser(minFitRange-.05,maxFitRange+.05);
  binInfo->GetZMinusHisto()->GetXaxis()->SetRangeUser(minFitRange-.05,maxFitRange+.05);
  yieldCanvas->cd(3);
  binInfo->GetZPlusHisto()->Draw("E");
  yieldCanvas->cd(4);
  binInfo->GetZMinusHisto()->Draw("E");
  yieldCanvas->Update();

  binInfo->SetYieldFits(new TF1("","gaus(0)+gaus(3)+gaus(6)",minFitRange,maxFitRange),
			new TF1("","gaus(0)+gaus(3)+gaus(6)",minFitRange,maxFitRange));

  binInfo->GetZPlusFit()->SetNpx(10000);
  binInfo->GetZMinusFit()->SetNpx(10000);

  binInfo->GetZPlusFit()->SetLineColor(kBlack);
  binInfo->GetZMinusFit()->SetLineColor(kBlack);
  
  binInfo->GetZPlusFit()->SetLineWidth(3);
  binInfo->GetZMinusFit()->SetLineWidth(3);

  binInfo->GetZPlusFit()->SetName(Form("%s_FullFit",binInfo->GetZPlusHisto()->GetName()));
  binInfo->GetZMinusFit()->SetName(Form("%s_FullFit",binInfo->GetZMinusHisto()->GetName()));

  //Set the Fit Parameters
  for (int iPar=0; iPar<binInfo->GetZPlusFit()->GetNpar(); iPar+=3){
    binInfo->GetZPlusFit()->SetParameter(iPar,binInfo->GetZPlusHisto()->
					 GetBinContent(binInfo->GetZPlusHisto()->
						       FindBin(means.at(iPar/3))));
    binInfo->GetZPlusFit()->SetParLimits(iPar,0,binInfo->GetZPlusFit()->GetParameter(iPar)*2.0);
    binInfo->GetZPlusFit()->FixParameter(iPar+1,means.at(iPar/3));
    binInfo->GetZPlusFit()->FixParameter(iPar+2,widths.at(iPar/3));
    
    binInfo->GetZMinusFit()->SetParameter(iPar,binInfo->GetZMinusHisto()->
					  GetBinContent(binInfo->GetZMinusHisto()->
							FindBin(means.at(iPar/3))));
    binInfo->GetZMinusFit()->SetParLimits(iPar,0,binInfo->GetZMinusFit()->GetParameter(iPar)*2.0);
    binInfo->GetZMinusFit()->FixParameter(iPar+1,means.at(iPar/3));
    binInfo->GetZMinusFit()->FixParameter(iPar+2,widths.at(iPar/3));
  }  
  
  //Perform the Fits, catch failed fits and try again for up to five times
  int fitStatus(-1);
  int nAttempts(0);
  while (fitStatus != 0 && nAttempts < 5){
    fitStatus = binInfo->GetZPlusHisto()->Fit(binInfo->GetZPlusFit(),"RL");
    nAttempts++;
  }

  fitStatus = -1;
  nAttempts = 0;
  while (fitStatus != 0 && nAttempts < 5){
    fitStatus = binInfo->GetZMinusHisto()->Fit(binInfo->GetZMinusFit(),"RL");
    nAttempts++;
  }
  
  //------ Draw Individual Fits ------
  yieldCanvas->cd(3);
  std::vector<TF1 *> indivPlusFits =
    DrawIndividualFits((TPad *)gPad,binInfo->GetZPlusFit(),binInfo->GetParticleOfInterest(),true);
  yieldCanvas->cd(4);
  std::vector <TF1 *> indivMinusFits = 
    DrawIndividualFits((TPad *)gPad,binInfo->GetZMinusFit(),binInfo->GetParticleOfInterest(),true);
  
  for (unsigned int i=0; i<indivPlusFits.size(); i++){
    binInfo->GetZPlusHisto()->GetListOfFunctions()->Add(indivPlusFits.at(i));
  }
  for (unsigned int i=0; i<indivMinusFits.size(); i++){
    binInfo->GetZMinusHisto()->GetListOfFunctions()->Add(indivMinusFits.at(i));
  }
  
  yieldCanvas->cd(3);
  binInfo->GetZPlusFit()->Draw("SAME");
  gPad->Update();
  yieldCanvas->cd(4);
  binInfo->GetZMinusFit()->Draw("SAME");
  gPad->Update();

  //---- Extract the Yield ----
  std::pair<double,double> yieldPlusResult =
    ComputeYield(binInfo->GetZPlusHisto(),binInfo->GetZPlusFit(),
		 binInfo->GetParticleOfInterest(),binInfo->GetmT(),nEvents);
  Double_t yieldPlus = yieldPlusResult.first;
  Double_t yieldPlusErr = yieldPlusResult.second;
  
  std::pair<double,double> yieldMinusResult =
    ComputeYield(binInfo->GetZMinusHisto(),binInfo->GetZMinusFit(),
		 binInfo->GetParticleOfInterest(),binInfo->GetmT(),nEvents);
  Double_t yieldMinus = yieldMinusResult.first;
  Double_t yieldMinusErr = yieldMinusResult.second;
  
  binInfo->SetYield(yieldPlus,yieldMinus);

  //----- Systematic Errors ------
  //Set the Parameter Error range of variation
  //All means are varied by 5%.
  //Widths are varied according to their confidence intervals.
  std::vector<std::pair<double,double> > particleParamErrs;
  for (int i=0; i<binInfo->GetZPlusFit()->GetNpar()/3; i++){
    particleParamErrs.push_back(std::make_pair(0,0));
    particleParamErrs.back().first = means.at(i)*.05;
    particleParamErrs.back().second = GetErrorAtValue(particleWidths->at(i),binInfo->GetmTm0());
  }
  
  Double_t systematicErrPlus = ComputeSystematicErrors(binInfo,nEvents,1,particleParamErrs);
  Double_t systematicErrMinus = ComputeSystematicErrors(binInfo,nEvents,-1,particleParamErrs);
  
  //----- Total Error - Quad Sum of stat and sys -----
  Double_t totalErrorPlus = sqrt(pow(yieldPlusErr,2)+pow(systematicErrPlus,2));
  Double_t totalErrorMinus = sqrt(pow(yieldMinusErr,2)+pow(systematicErrMinus,2));

  binInfo->SetYieldError(totalErrorPlus,totalErrorMinus);

    //----- Add point To Spectrum -----
  spectraPlus->SetPoint(spectraPlus->GetN(),binInfo->mTm0,yieldPlus);
  spectraPlus->SetPointError(spectraPlus->GetN()-1,0,totalErrorPlus);
  spectraMinus->SetPoint(spectraMinus->GetN(),binInfo->mTm0,yieldMinus);
  spectraMinus->SetPointError(spectraMinus->GetN()-1,0,totalErrorMinus);

  spectraCanvas->cd();
  spectraPlus->Draw("P");
  spectraMinus->Draw("P");
  spectraCanvas->Update();
  
  return true;
  

}

//______________________________________________________________________________________
std::pair<double,double> ComputeYield(TH1D *yieldHisto, TF1 *fit, Int_t particleSpecies,
				      Double_t mT,Double_t nEvents, bool isWidthFixed){

  //Pair to return
  std::pair<double,double> yield = std::make_pair(0.0,0.0);
  
  //Get the Fit for the particle of interest
  ParticleInfo particleInfo;
  TF1 *pidInterestFit = (TF1 *)yieldHisto->GetListOfFunctions()->
    FindObject(Form("%s_Fit",particleInfo.GetParticleName(particleSpecies).Data()));
  
  //Loop Over the bins of the histogram in the range of the full fit.
  //For each bin compute the yields of the particle of interest and non interest
  Double_t minRange(0), maxRange(0);
  fit->GetRange(minRange,maxRange);
  for (int iBin=yieldHisto->FindBin(minRange); iBin<=yieldHisto->FindBin(maxRange); iBin++){
    
    //Ratio of full fit to interest fit in this bin
    Double_t binCenter = yieldHisto->GetBinCenter(iBin);
    Double_t ratio = pidInterestFit->Eval(binCenter) / fit->Eval(binCenter);
    
    Double_t binInterestYield = ratio * yieldHisto->GetBinContent(iBin);
    Double_t binConfoundYield = (1.0-ratio) * yieldHisto->GetBinContent(iBin);
    
    yield.first += binInterestYield;
    
  }
  
  //Compute the Error
  yield.second = sqrt(yield.first);
  
  Double_t pi = TMath::Pi();
  Double_t normFactor = (1.0/mT) * (1.0/mTm0BinWidth) * (1.0/rapidityBinWidth) *
    (1.0/nEvents) * (1.0/(2.0*pi));
  
  yield.first = yield.first * normFactor;
  yield.second = yield.second * normFactor;
  
  return yield;
}

/*
//_____________________________________________________________
std::pair<double,double> ComputeYield(TH1D *yieldHisto, TF1 *fit, Int_t particleSpecies,
				      Double_t mT,Double_t nEvents, bool isWidthFixed){


  Double_t zBinWidth = (double)yieldHisto->GetBinWidth(1);
  Double_t pi = TMath::Pi();
  Double_t normFactor = (1.0/mT) * (1.0/mTm0BinWidth) * (1.0/rapidityBinWidth) *
    (1.0/nEvents) * (1.0/(2.0*pi));

  Double_t amp      = fit->GetParameter(3*particleSpecies);
  Double_t width    = fit->GetParameter(3*particleSpecies+2);
  Double_t ampErr   = fit->GetParError(3*particleSpecies);
  Double_t widthErr = 0;

  //If the Width is fixed the error is included in systematics
  if (!isWidthFixed){
    widthErr = fit->GetParError(3*particleSpecies+2);
  }
  
  Double_t yield = ( (amp * width * sqrt(2.0*pi) ) / zBinWidth ) * normFactor;
  Double_t yieldErr = yield * sqrt(pow(ampErr/amp,2) + pow(widthErr/width,2));

  return std::make_pair(yield,yieldErr);
  
}
*/

//_____________________________________________________________
std::vector<TF1 *> DrawIndividualFits(TPad *pad, TF1 *fitFunc, int speciesInterest, bool isTof){

  //Draws the individual fits for each of the particle species and
  //returns a vector of function pointers

  const int nPars = fitFunc->GetNpar();
  std::vector <TF1 *> particleFits (nPars/3-(speciesInterest==PION&&!isTof ? 1:0),(TF1 *) NULL);
  ParticleInfo particleInfo;
  
  for (unsigned int iParticle=0; iParticle<particleFits.size(); iParticle++){

    particleFits.at(iParticle) = new TF1(Form("%s_Fit",particleInfo.GetParticleName(iParticle).Data()),
					 "gaus(0)",-10,10);
    particleFits.at(iParticle)->SetNpx(10000);
    
    particleFits.at(iParticle)->SetParameter(0,fitFunc->GetParameter(iParticle*3));
    particleFits.at(iParticle)->SetParameter(1,fitFunc->GetParameter(iParticle*3+1));
    particleFits.at(iParticle)->SetParameter(2,fitFunc->GetParameter(iParticle*3+2));

    particleFits.at(iParticle)->SetLineWidth(2);

    particleFits.at(iParticle)->SetLineColor(particleInfo.GetParticleColor(iParticle));

    pad->cd();
    particleFits.at(iParticle)->Draw("SAME");
    
  }

  //The Electron
  if (speciesInterest == PION && !isTof){

    particleFits.push_back(new TF1("Electron_Fit","gaus(0)",-10,10));
    particleFits.back()->SetNpx(100000);
    particleFits.back()->SetParameter(0,fitFunc->GetParameter(nPars-3));
    particleFits.back()->SetParameter(1,fitFunc->GetParameter(nPars-2));
    particleFits.back()->SetParameter(2,fitFunc->GetParameter(nPars-1));

    particleFits.back()->SetLineWidth(2);
    particleFits.back()->SetLineColor(particleInfo.GetParticleColor(ELECTRON));

    pad->cd();
    particleFits.back()->Draw("SAME");
    
  }
  
  gPad->Update();
  
  return particleFits;
}


//__________________________________________________________________
Double_t ComputeSystematicErrors(CurrentBinInfo *binInfo, Double_t nEvents, Int_t plusOrMinus,
				 std::vector<std::pair<double,double> > particleParamErrs){

  //Vary all the parameters of the fit to the values passed in in particleParamErrs.

  TH1D *yieldHisto;
  TF1 *fitFunc;
  Double_t nominalYield;
  Int_t speciesInterest = binInfo->GetParticleOfInterest();
  Double_t mTm0 = binInfo->GetmTm0();

  if (plusOrMinus > 0){
    yieldHisto = binInfo->GetZPlusHisto();
    fitFunc    = binInfo->GetZPlusFit();
    nominalYield = binInfo->GetYieldPlus();
  }
  else if (plusOrMinus < 0){
    yieldHisto = binInfo->GetZMinusHisto();
    fitFunc    = binInfo->GetZMinusFit();
    nominalYield = binInfo->GetYieldMinus();
  }
  else {
    cout <<"ERROR - ExtractYields.cxx :: ComputeSystematicErrors() plusOrMinus can only be +1(plus) or -1(minus). EXITING!";
    exit (EXIT_FAILURE);
  }
  
  std::vector<double> yields(0);
  const int nPars = fitFunc->GetNpar();

  //We need to loop Over all the parameters twice - vary them high and low
  for (int iHighLow=0; iHighLow<2; iHighLow++){
    
    Double_t highOrLow(1);
    if (iHighLow == 0)
      highOrLow = 1.0;
    else if (iHighLow == 1)
      highOrLow = -1.0;
    
    for (int iPar=0; iPar<nPars; iPar++){
      
      //if (iPar/3.0 == speciesInterest)
      //continue;
      
      //Copy the fit function into a new function, fix all the parameters
      TF1 tempFunc(*fitFunc);
      tempFunc.SetLineColor(kRed);
      
      for (int jPar=0; jPar<tempFunc.GetNpar(); jPar++){
	
	if (jPar/3 == speciesInterest){
	  tempFunc.SetParameter(jPar,fitFunc->GetParameter(jPar));
	}
	else {
	  //Amplitudes
	  if (jPar%3==0){
	    tempFunc.SetParameter(jPar,TMath::Max(0.0,fitFunc->GetParameter(jPar)+fitFunc->GetParError(jPar)*highOrLow));
	    tempFunc.SetParLimits(jPar,0,tempFunc.GetParameter(jPar)*1.1);
	  }
	  //Means
	  else if (jPar%3 == 1){
	    tempFunc.FixParameter(jPar,fitFunc->GetParameter(jPar)+particleParamErrs.at(jPar/3).first*highOrLow);
	  }
	  //Widths
	  else if (jPar%3 == 2){
	    tempFunc.FixParameter(jPar,fitFunc->GetParameter(jPar)+particleParamErrs.at(jPar/3).second*highOrLow);
	  }
	}
	
      }//End Loop Over jPar

      yieldHisto->Fit(&tempFunc,"RNQL");
      
      //Compute the Yield
      std::pair<double,double> yield = ComputeYield(yieldHisto,&tempFunc,speciesInterest,binInfo->GetmT(),nEvents);
      yields.push_back(yield.first);

      /*
      TCanvas tempCanvas("tempCanvas","tempCanvas",20,20,800,600);
      tempCanvas.SetLogy();
      yieldHisto->Draw();
      fitFunc->Draw("SAME");
      tempFunc.Draw("SAME");
      
      tempCanvas.Update();
      gSystem->Sleep(30);
      */
      
    }//End Loop Over All Parameters iPars
    
  }//End Loop Over High and Low
  
  return TMath::RMS(yields.size(),&yields.at(0));
  
}


//____________________________________________________________
void ScaleTofSpectrum(TGraphErrors *tpcSpectrum, TGraphErrors *tofSpectrum){

  //The TOF Matching efficiency correction applied during the skimmer binner corrects
  //for how the matching efficiency changes as a function of mT-m0, but there is often
  //an overal correction that needs to be applied which is momentum independednt.

  if (tofSpectrum->GetN() < 1)
    return;
  
  std::vector<double> tofToTpcRatio;
  for (int iPoint=0; iPoint<tpcSpectrum->GetN(); iPoint++){
    
    int tofSpectrumPoint = TMath::BinarySearch(tofSpectrum->GetN(),
					       tofSpectrum->GetX(),
					       tpcSpectrum->GetX()[iPoint]);
    if (fabs(tofSpectrum->GetX()[tofSpectrumPoint] -
	     tpcSpectrum->GetX()[iPoint]) > mTm0BinWidth)
      continue;

    //Only consider points with mT-m0 above .4 GeV and less than the stitch
    //value - where the TOF is considered reliable
    if (tofSpectrum->GetX()[tofSpectrumPoint] < 0.4 || tofSpectrum->GetX()[tofSpectrumPoint] > TPC_TOF_STITCH)
      continue;
    
    tofToTpcRatio.push_back(tofSpectrum->GetY()[tofSpectrumPoint] /
			    tpcSpectrum->GetY()[iPoint]);
    
  }

  Double_t correctionFactor(1);
  
  //If there are no overlapping points between the TPC and TOF Spectra  
  if (tofToTpcRatio.size() == 0){
    cout <<"WARNING - generateRawSpectra::ScaleTofSpectrum() - No overlap between TPC and TOF Spectra!\n"
	 <<"          Can not apply momentum independent scaling to to TOF Spectrum!\n";

    //If there are no overlapping points to determine what the correction factor
    //should be for the tof spectrum, then fit an exponential to the last three points
    //of the tpc spectrum, extrapolated the fit to the first value of the tof spectrum,
    //take the ratio of the function to the tof yield, and use that as the correction factor.

    Double_t min = tpcSpectrum->GetX()[tpcSpectrum->GetN()-3] - mTm0BinWidth/2.0;
    
    TF1 expoExtrapolation("","expo",min,2);
    tpcSpectrum->Fit(&expoExtrapolation,"RNQ");

    Double_t firstTofVal = tofSpectrum->GetY()[0];

    correctionFactor = firstTofVal / expoExtrapolation.Eval(tofSpectrum->GetX()[0]);
    
    //return;
  }
  //If ther are overlapping points then simply use the average ratio
  else {
    correctionFactor = TMath::Mean(tofToTpcRatio.size(),&tofToTpcRatio.at(0));
  }

  //Apply the Correction
  TGraphScale(tofSpectrum,1.0/correctionFactor,false);

}

//____________________________________________________________
void RemovePointsAbove(TGraphErrors *graph, Double_t xVal){

  if (graph->GetN() < 1)
    return;
  
  //Remove Points in the graph above xVal
  for (int iPoint=graph->GetN(); iPoint>=0; iPoint--){

    if (graph->GetX()[iPoint] > xVal)
      graph->RemovePoint(iPoint);
  }
  

}

//____________________________________________________________
void RemovePointsBelow(TGraphErrors *graph, Double_t xVal){

  if (graph->GetN() < 1)
    return;
  
  //Remove Points in the graph below xVal
  for (int iPoint=graph->GetN(); iPoint>=0; iPoint--){

    if (graph->GetX()[iPoint] <= xVal)
      graph->RemovePoint(iPoint);
  }
  

}

//___________________________________________________________
void ConstructSpectraRatio(TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
			   TGraphErrors *spectraRatio){

  const int nPoints = TMath::Min(spectraPlus->GetN(),spectraMinus->GetN());
  for (int iPoint=0; iPoint<nPoints; iPoint++){

    int iPointMinus = TMath::BinarySearch(spectraMinus->GetN(),
					  spectraMinus->GetX(),
					  spectraPlus->GetX()[iPoint]);
    if (fabs(spectraPlus->GetX()[iPoint]-spectraMinus->GetX()[iPointMinus]) > mTm0BinWidth/2.0)
      continue;
    
    
    Double_t yieldPlus = spectraPlus->GetY()[iPoint];    
    Double_t yieldMinus = spectraMinus->GetY()[iPointMinus];
    Double_t yieldPlusErr = spectraPlus->GetEY()[iPoint];
    Double_t yieldMinusErr = spectraMinus->GetEY()[iPointMinus];
    
    Double_t ratio = yieldPlus/yieldMinus;
    Double_t ratioErr = ratio * sqrt(pow((yieldPlusErr/yieldPlus),2) +
				     pow((yieldMinusErr/yieldMinus),2));

    Double_t meanX = (spectraPlus->GetX()[iPoint]+spectraMinus->GetX()[iPoint])/2.0;
    
    spectraRatio->SetPoint(spectraRatio->GetN(),meanX,ratio);
    spectraRatio->SetPointError(spectraRatio->GetN()-1,0,ratioErr);
  }
  
}

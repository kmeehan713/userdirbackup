#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunFullAnalysisChain.bash"
    echo "    PURPOSE:"
    echo "        This runs the full analysis chain to produce the rapidity density distributions"
    echo "        for collider events from the Beam Energy Scan. It performs several steps: "
    echo "            1: ToF Matching Efficiency Study - Fits efficiency curves"
    echo "            2: Pid Calibration - Binning of dEdx in rapidity and mass assumption"
    echo "            3: Pid Calibration - Fitting the above dEdx"
    echo "            4: Creates zTPC and zTOF Histograms for yield extraction"
    echo "            5: Parameterize the Means and Widths of zTPC, zTOF Distributions"
    echo "            6: Generates uncorrected spectra from yield histograms"
    echo "            NOTE: At this point embedding must be processed (see StMiniMcReader package)"
    echo "            7: TPC Tracking Efficiency Study - Fits efficiency curves"
    echo "            8: Tracking Efficiency Correction"
    echo "            NOTE: At this point background/feed down studies must be done (see StURQMD package)"
    echo "            9: Apply background/feed down corrections"
    echo "            NOTE: See the drawingmacros/ directory for plot making"
    echo "    USAGE:"
    echo "        ./RunFullAnalysisChain.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -b - Beginning Step - The analysis chain will start at this step"
    echo "        -s - Stop Step - The analysis chain will be run up to and including step number s"
    echo ""
    echo ""

}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "b:s:h" opts; do
        case "$opts" in
	    b) minStepUser="${OPTARG}"; shift;;
            s) maxStepUser="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#If the user has specified a minimum step number then use it
if [ ! -z "$minStepUser" ]; then
    minStep=$minStepUser
else
    minStep=0
fi

#If the user has specified a maximum step number then use it
if [ ! -z "$maxStepUser" ]; then
    maxStep=$maxStepUser
else
    maxStep=100
fi

#### STEP 1 - TOF Matching Efficiency Study
####     Create 2D histograms of nTracksTPC and nTracksTOF for all centrality, rapidity, and mass assumptions 
####     Create TOF matching efficiency graphs by dividing histograms nTracksTOF/nTrakcsTPC using Bayesian Errors
####     Fit the TOF matching efficiency graphs to create the efficiency curves
if [[ "$minStep" -le 1 && "$maxStep" -ge 1 ]]; then
    echo "Running Step 1 - ToF Matching Efficiency for: " $name $eventConfig $( date )
    ./RunTofEfficiencyStudy.bash $configFile
fi

#### STEP 2 - PID Calibration
####     Bin dE/dx in mass assumption, rapidity, and mT-m0 bins
####     Create a Tree of average kinematic variables for each bin
if [[ "$minStep" -le 2 && "$maxStep" -ge 2 ]]; then
    echo "Running Step 2 - PID Calibration (dEdx Binning) for: " $name $eventConfig $( date )
    ./RunBindEdxInRapidityMtM0.bash $configFile
fi

#### STEP 3 - PID Calibration
####     Fit Bichsel curve model as function of Beta*Gamma to graphs from above
if [[ "$minStep" -le 3 && "$maxStep" -ge 3 ]]; then
    echo "Running Step 3 - PID Calibration (Bichsel Curve Fitting) for: " $name $eventConfig $( date )
    ./RunFitdEdxBichselCurves.bash $configFile
fi

#### STEP 4 - Create Yield Hitograms
####     Create zTPC and zTOF Histograms 
if [[ "$minStep" -le 4 && "$maxStep" -ge 4 ]]; then
    echo "Running Step 4 - Creating Yield Histograms (zTPC and zTOF) for: " $name $eventConfig $( date )
    ./RunTofSkimmerAndBinner.bash $configFile
fi

#### STEP 5 - PID Calibration
####     Parameterize the Means and Widths of the particles
if [[ "$minStep" -le 5 && "$maxStep" -ge 5 ]]; then
    echo "Running Step 5 - Extracting and Parameterizing Means and Widths of Particles for: " $name $eventConfig $( date )
    ./RunExtractMeansAndWidths.bash $configFile
fi

#### STEP 6 - Yield Extraction
####     Generate Raw Spectra from zTPC and zTOF Yield Histograms
if [[ "$minStep" -le 6 && "$maxStep" -ge 6 ]]; then
    echo "Running Step 6 - Generating the Raw Spectra for: " $name $eventConfig $( date )
    ./RunGenerateRawSpectra.bash $configFile
fi

#### STEP 7 - TPC Tracking Study
####     Generate the tpc tracking efficieny curves by fitting the embedding data
#if [[ "$minStep" -le 7 && "$maxStep" -ge 7 ]]; then
#    echo "Running Step 7 - Generating TPC Tracking Correction Curves from Embedding: " $name $eventConfig $( date )
#    ./RunMakeEmbeddingCorrectionCurves.bash $configFile
#fi

#### STEP 8 - Apply TPC Tracking Corrections
####     Apply the correction curves from step 7 to the spectra
if [[ "$minStep" -le 8 && "$maxStep" -ge 8 ]]; then
    echo "Running Step 8 - Applying the correction corves from embedding to the spectra: " $name $eventConfig $( date )
    ./RunCorrectSpectra.bash $configFile
fi



exit 0

#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include <TRandom3.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

void MakeTofMatchEfficiencyHistos(TString inputFile, TString outputFile, Long64_t nEvents=-1){

  //Get the Z-Vertex Cuts
  std::pair<double,double> zVertexCuts = GetZVertexCuts();

  //Read the input files
  //If there are valid z Vertex cuts then use them in the constructor of the davisdstreader
  DavisDstReader *davisDst = NULL;
  if (zVertexCuts.first != -999 && zVertexCuts.second != 999)
    davisDst = new DavisDstReader(inputFile,zVertexCuts.first,zVertexCuts.second);
  else
    davisDst = new DavisDstReader(inputFile);
  
  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  
  //Create the output file
  TFile *outFile = new TFile(outputFile,"RECREATE");
  
  //Create an instance of particle info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true);
  
  //Vector of Particle types that will be considered
  std::vector<int> particles;
  particles.push_back((int)PION);
  particles.push_back((int)KAON);
  particles.push_back((int)PROTON);
  particles.push_back((int)ELECTRON);
  /*
  particles.push_back((int)DEUTERON);
  particles.push_back((int)TRITON);
  particles.push_back((int)HELION);
  particles.push_back((int)ALPHA);
  particles.push_back((int)MUON);
  */
  
  const unsigned int nCentralityBins = GetNCentralityBins();
  const unsigned int nParticles = particles.size();
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();
  
  //------- Histograms and Graph ---------
  std::vector<std::vector<TH2F *> > tpcTracks(nCentralityBins,std::vector<TH2F *> (nParticles,(TH2F *)NULL));
  std::vector<std::vector<TH2F *> > tofTracks(nCentralityBins,std::vector<TH2F *> (nParticles,(TH2F *)NULL));
  std::vector<std::vector<TH2F *> > tofMisMatches(nCentralityBins,std::vector<TH2F *> (nParticles,(TH2F *)NULL));

  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
      
      int particleSpecies = particles.at(iParticle);
      
      tpcTracks.at(iCentBin).at(iParticle) =
	new TH2F(Form("tpcTracks_Cent%d_%s",iCentBin,particleInfo->GetParticleName(particleSpecies).Data()),
		 Form("Tracks Measured in the TPC | CentIndex:%d PID:%s;y_{%s};m_{T}-m_{%s}",
		      iCentBin,particleInfo->GetParticleName(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data()),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins*2,mTm0Min,mTm0Max);

      tofTracks.at(iCentBin).at(iParticle) =
	new TH2F(Form("tofTracks_Cent%d_%s",iCentBin,particleInfo->GetParticleName(particleSpecies).Data()),
		 Form("Tracks Measured in the TOF | CentIndex:%d PID:%s;y_{%s};m_{T}-m_{%s}",
		      iCentBin,particleInfo->GetParticleName(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data()),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins*2,mTm0Min,mTm0Max);

      tofMisMatches.at(iCentBin).at(iParticle) =
	new TH2F(Form("tofMismatches_Cent%d_%s",iCentBin,particleInfo->GetParticleName(particleSpecies).Data()),
		 Form("TOF Mismatches | CentIndex:%d PID:%s;y_{%s};m_{T}-m_{%s}",
		      iCentBin,particleInfo->GetParticleName(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data(),
		      particleInfo->GetParticleSymbol(particleSpecies).Data()),
		 nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins*2,mTm0Min,mTm0Max);
      
    }//End Loop Over Particles

  }//End Loop Over Centrality Bins


  //Set the number of entries to read
  Long64_t nEntries = davisDst->GetEntries();
  Bool_t useRandomSample = false;
  TRandom3 rand(0);
  if (nEvents > 0 && nEvents < nEntries){
    nEntries = nEvents;
    useRandomSample = true;
  }
  
  //----- Loop Over the Entries in the Tree -----
  for (unsigned int iEntry = 0; iEntry < nEntries; iEntry++){

    //Get the Event entry from the DavisDst and make sure its good
    unsigned int useEntry = iEntry;
    if (useRandomSample)
      useEntry = rand.Uniform(0,davisDst->GetEntries());
    event = davisDst->GetEntry(useEntry);
    if (!IsGoodEvent(event))
      continue;
    
    //Loop Over Vertices
    for (int iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){
      
      //Get the ith vertex and check if it is good
      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;

      //Compute the Centrality Variable
      int centVariable = GetCentralityVariable(primaryVertex);
      
      //Get the Centrality Bin (skip if bad)
      int iCentBin = GetCentralityBin(centVariable,
				      event->GetTriggerID(&allowedTriggers),
				      primaryVertex->GetZVertex());
      if (iCentBin < 0)
	continue;
      
      //Loop Over the Tracks
      for (int iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){
	
	//Get the ith track and check if it is good
	track = primaryVertex->GetPrimaryTrack(iTrack);
	if (!IsGoodTrack(track))
	  continue;
	
	//Fill the dEdx Histograms for each particle spcies
	for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
	  
	  //Get track information
	  int particleSpecies  = particles.at(iParticle);
	  float massAssumption = particleInfo->GetParticleMass(particleSpecies);
	  float rapidity       = track->GetRapidity(massAssumption);
	  float mTm0           = track->GetmTm0(massAssumption);
	  float dEdx           = track->GetdEdx()*1000000;
	  float inverseBeta    = 1.0/track->GetTofBeta();
	  float pTotal         = track->GetPTotal();

	  //Fill the TPC Histogram
	  tpcTracks.at(iCentBin).at(iParticle)->Fill(rapidity,mTm0);
	  
	  //Check if this is a good tof Track
	  if (!IsGoodTofTrack(track))
	    continue;

	  //Fill the TOF Histogram
	  tofTracks.at(iCentBin).at(iParticle)->Fill(rapidity,mTm0);

	  //Fill the Mis-Match Histogram. These are tracks that are of one species
	  //in the TPC and of a different species in the TOF
	  if (fabs(particleInfo->ComputeZTPC(dEdx,track->GetPt(),track->GetPz(),particleSpecies)/particleInfo->GetTPCWidth()) < 1.0 &&
	      fabs(particleInfo->ComputeZTOF(inverseBeta,track->GetPt(),track->GetPz(),particleSpecies)/particleInfo->GetTOFWidth()) > 3.0)
	    tofMisMatches.at(iCentBin).at(iParticle)->Fill(rapidity,mTm0);
	  
	}//End Loop Over Particles
	
      }//End Loop Over Tracks
      
    }//End Loop Over Vertices
    
  }//End Loop Over Events

  
  //Make Directories in Output File
  outFile->mkdir("TrackHistos");
  
  //Save Histograms
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

      outFile->cd();
      outFile->cd("TrackHistos");
 
      tpcTracks.at(iCentBin).at(iParticle)->Write();
      tofTracks.at(iCentBin).at(iParticle)->Write();
      tofMisMatches.at(iCentBin).at(iParticle)->Write();

    }//End Loop Over Particles

  }//End Loop Over Centrality Bins

  outFile->cd();
  outFile->Close();
  
}

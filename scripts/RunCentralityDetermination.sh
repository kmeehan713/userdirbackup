#!/bin/bash

#This script is used to find the centrality binning of data
#It needs a Glauber Simulation to have been completed first.

#USER DEFINED VARIABLES
dataFileName=../userfiles/AuAu_4_5GeV_2015/officialProduction/centTest/CentralityVariableDistributions.root #File containing histogram to be matched
#dataFileName=../userfiles/AlAu_4_9GeV_2015/analysis/VertexIndex0/CentralityVariableDistributions.root #File containing histogram to be matched
dataHistoName=nGoodTracks
glauberFileName=../userfiles/AuAu_4_5GeV_2015/officialProduction/centTest/Glauber_197_197_27mb_WoodsSaxon.root
#glauberFileName=../userfiles/AlAu_4_9GeV_2015/analysis/VertexIndex0/Glauber_197_27_30mb_WoodsSaxon.root
outputFileName=../userfiles/AuAu_4_5GeV_2015/officialProduction/centTest/CentralityBins.root                #outputfile (will get re-written if it already exists!)
#outputFileName=../userfiles/AlAu_4_9GeV_2015/analysis/VertexIndex0/CentralityBins.root                #outputfile (will get re-written if it already exists!)
normStartBinCenter=100                                                   #The bin center value to begin the chi^2 matching/optimization routine
normStopBinCenter=210                                                    #The bin center value to end the chi^2 matching/optimization routine Use -1 to use the last bin of the data histogram. NOTE: This value must be larger than normStartBinCenter to make any sense.
#normStartBinCenter=40                                                   #The bin center value to begin the chi^2 matching/optimization routine
#normStopBinCenter=100                                                    #The bin center value to end the chi^2 matching/optimization routine Use -1 to use the last bin of the data histogram. NOTE: This value must be larger than normStartBinCenter to make any sense.

root -l -b -q ../macros/RunCentralityDetermination.C\(\"$dataFileName\",\"$dataHistoName\",\"$glauberFileName\",\"$outputFileName\",$normStartBinCenter,$normStopBinCenter\)


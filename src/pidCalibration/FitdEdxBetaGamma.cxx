#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TGraphErrors.h>
#include <TFitResultPtr.h>
#include <TVirtualFitter.h>
#include <TThread.h>
#include <Fit/Fitter.h>
#include <Math/WrappedMultiTF1.h>
#include <Math/WrappedTF1.h>
#include <Fit/UnBinData.h>
#include <Fit/FitResult.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "utilityFunctions.h"

//______________________________________________________________________________
void FitdEdxBetaGamma(TString inputFile){

  TFile *file = new TFile(inputFile,"UPDATE");

  TFile *outFile = file;
  outFile->mkdir("dEdxBetaGammaFits");
  
  double minFitRange(.1);
  double maxFitRange(1000);
  int npx(1000000);
  int minGraphPoints(100); //Minimum number of points the graph must contain to be fit
  int minSpeciesCount(10); //Minimum number of each species (except electrons) that must exist for fit
  double percentConfInterval(.95);

  ParticleInfo particleInfo;

  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);

  const unsigned int nParticles = particles.size();

  //Create graphs and fits functions for each particle and rapidity bin combination
  outFile->cd();
  std::vector<std::vector<TF1 *> > dEdxFits (nParticles,std::vector<TF1 *> (nRapidityBins, (TF1 *)NULL));
  std::vector<std::vector<TMultiGraph *> >
    dEdxMultiGraph (nParticles,std::vector<TMultiGraph *> (nRapidityBins, (TMultiGraph *)NULL));
  std::vector<std::vector<TGraphErrors *> >
    dEdxFitConfidence(nParticles,std::vector<TGraphErrors *> (nRapidityBins, (TGraphErrors *)NULL));
  
  //Loop Over the Particle Species and Rapidity bins to do the fits
  TF1 *fitFunc;                   //Pointer to current fit function
  TGraphErrors *confGraph;        //Pointer to current confidence interval graph
  TMultiGraph *dEdxGraph;         //Pointer to current dEdx graph that will be fit
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      int currentSpecies = particles.at(iParticle);

      //Create the MultiGraph for the current species
      dEdxMultiGraph.at(iParticle).at(yIndex) = new TMultiGraph();
      dEdxGraph = dEdxMultiGraph.at(iParticle).at(yIndex);
      dEdxGraph->SetName(Form("bichselBetaGammaGraph_%s_%d_Combined",
			      particleInfo.GetParticleName(currentSpecies).Data(),
			      yIndex));
            
      //Get the Graphs and add them to the multip graph
      int totalPoints(0);
      std::vector<int> nSpeciesCount(particleInfo.GetNumberOfDefinedParticles(),0);
      file->cd();
      file->cd("BichselBetaGammaGraphs");
      for (int subSpeciesIndex=0; subSpeciesIndex<particleInfo.GetNumberOfDefinedParticles(); subSpeciesIndex++){
	TGraph *temp = (TGraph *)gDirectory->Get(Form("bichselBetaGamma_%s_%d_%s",
						      particleInfo.GetParticleName(currentSpecies).Data(),
						      yIndex,
						      particleInfo.GetParticleName(subSpeciesIndex).Data()));
	//Confirm the graph exists
	if (!temp)
	  continue;

	//Count the number of each species
	nSpeciesCount.at(subSpeciesIndex) = temp->GetN();
	
	temp->SetMarkerColor(particleInfo.GetParticleColor(subSpeciesIndex));
	
	//Add the Graph
	totalPoints += temp->GetN();
	dEdxGraph->Add((TGraph *)temp->Clone(temp->GetName()),"P");
	//delete temp;
			     
      }//End Loop Over subspecies index to add graphs to multi graph

      //If the graph doesn't exist then skip
      if (!dEdxGraph){
	continue;
      }

      //If the graph doesn't have any points then skip
      if (totalPoints < minGraphPoints){	
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s has fewer than %d points. SKIPPING!\n",
		    dEdxGraph->GetName(),minGraphPoints); 
	continue;
      }

      //Require that there are at least a few of each pion, kaon, proton, and electron
      if (nSpeciesCount.at(ELECTRON) < 1 && currentSpecies != PROTON){
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s does not have enough electrons. SKIPPING\n",
		    dEdxGraph->GetName());
	continue;
      }
      if (nSpeciesCount.at(KAON) < minSpeciesCount){
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s does not have enough kaons. SKIPPING\n",
		    dEdxGraph->GetName());
	continue;
      }
      if (nSpeciesCount.at(PROTON) < minSpeciesCount){
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s does not have enough protons. SKIPPING\n",
		    dEdxGraph->GetName());
	continue;
      }
      if (nSpeciesCount.at(PION) < minSpeciesCount){
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s does not have enough pions. SKIPPING\n",
		    dEdxGraph->GetName());
	continue;
      }

      cout <<Form("INFO - FitdEdxBetaGamma() - Fitting %s.\n",dEdxGraph->GetName());
      
      //Create the Fit Function
      outFile->cd();
      outFile->cd("dEdxBetaGammaFits");
      dEdxFits.at(iParticle).at(yIndex) =
	new TF1(Form("dEdxBetaGammaFit_%s_%d",particleInfo.GetParticleName(currentSpecies).Data(),yIndex),
		&particleInfo,&ParticleInfo::BichselEmpiricalFitPrototype,minFitRange,maxFitRange,7);
      dEdxFits.at(iParticle).at(yIndex)->
	SetTitle(Form("dEdx Vs. #beta#gamma : MassAssumption=%s : y=%.03g;#beta#gamma;dE/dx (KeV/cm)",
		     particleInfo.GetParticleName(currentSpecies).Data(),GetRapidityRangeCenter(yIndex)));
      fitFunc = dEdxFits.at(iParticle).at(yIndex);

      //Set/Fix The Parameters (Seeds are from Roppon Picha's Thesis - page 72)
      fitFunc->SetNpx(npx);
      /*
      fitFunc->FixParameter(0,1.2403);
      fitFunc->FixParameter(1,0.31426);
      fitFunc->SetParameter(2,12.428);
      fitFunc->SetParameter(3,0.41799);
      fitFunc->FixParameter(4,1.6385);
      fitFunc->FixParameter(5,0.72059);
      fitFunc->SetParameter(6,2.3503);
      */

      //---------------------
      fitFunc->FixParameter(0,1.2403);
      fitFunc->SetParameter(1,0.31426);
      fitFunc->SetParameter(2,12.428);
      fitFunc->SetParameter(3,0.41799);
      fitFunc->FixParameter(4,1.6385);
      fitFunc->FixParameter(5,0.72059);
      fitFunc->SetParameter(6,2.3503);

      //fitFunc->SetParLimits(0,0.8,1.5);
      fitFunc->SetParLimits(1,0.1,0.6);
      fitFunc->SetParLimits(2,11,13);
      fitFunc->SetParLimits(3,.3,.5);
      //fitFunc->SetParLimits(4,1,3);
      //fitFunc->SetParLimits(5,0.2,2.0);
      fitFunc->SetParLimits(6,1.0,3.0);
      //-----------------------

      
      //Limits of the non-fixed parameters
      //fitFunc->SetParLimits(4,1.4,1.8);
      //fitFunc->SetParLimits(5,.5,.8);
      /*
      fitFunc->SetParLimits(3,.3,.5);
      fitFunc->SetParLimits(2,5.0,15.0);
      fitFunc->SetParLimits(6,1.0,2.5);
      */
      
      //Do The Fit
      TFitResultPtr fitResult;
      int fitStatus(0);
      int iters(0);
      do {
	fitResult = dEdxGraph->Fit(fitFunc,"RS");
	fitStatus = fitResult;
      }while (fitStatus != 0 && iters < 5); 
	
      //If the fit was unsuccessful delete the function and proceed to the next bin.
      if (fitStatus != 0){
	cout <<Form("WARNING - FitdEdxBetaGamma() - %s fit was unsuccessful.\n",dEdxGraph->GetName());
	delete fitFunc;
	continue;
      }
            
      //Get the confidence Interval
      dEdxFitConfidence.at(iParticle).at(yIndex) = new TGraphErrors(npx);
      confGraph = dEdxFitConfidence.at(iParticle).at(yIndex);      
      confGraph->SetName(Form("dEdxFitConfidenceInterval_%s_%d",
			      particleInfo.GetParticleName(currentSpecies).Data(),yIndex));

      
      for (int i=0; i<npx; i++){
	confGraph->SetPoint(i,minFitRange + (pow(i,2)*(maxFitRange-minFitRange))/pow(npx,2),0);
      }
      (TVirtualFitter::GetFitter())->GetConfidenceIntervals(confGraph,percentConfInterval);
      confGraph->SetFillColor(kGray);
      confGraph->SetFillStyle(3001);

      //Save the Fit and Confidence Interval
      fitFunc->Write();
      confGraph->Write();
      
    }//End Loop Over rapidity bins

  }//End Loop Over particles
  
  file->Close();
  outFile->Close();
 
}

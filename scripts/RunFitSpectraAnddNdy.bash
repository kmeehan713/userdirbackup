#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunFitSpectra.bash"
    echo "    PURPOSE:"
    echo "        This fits the spectra with various functional forms depending "
    echo "        on the particle species to extract the Rapidity Density Distributions."
    echo "    USAGE:"
    echo "        ./RunFitSpectra.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -p - only do this particle ID (pion=0, kaon=1, proton=2)" 
    echo "        -q - only do this charge (-1 or 1)"
    echo "        -c - only do this centrality bin index"
    echo "        -y - only do this rapidity value (real number not index)"
    echo ""
    echo ""
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:q:c:y:h" opts; do
	case "$opts" in
	    p) userSpecies="${OPTARG}"; shift;;
	    q) userCharge="${OPTARG}"; shift;;
	    c) userCentrality="${OPTARG}"; shift;;
	    y) userRapidity="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#If the user has not set a rapidity value then set it to -999 meaning do all rapidity bins
if [ -z $userRapidity ]; then
    userRapidity=-999
fi

#If the user has not set a centrality bin then set it to -999 meaning do all centrality bins
if [ -z $userCentrality ]; then
    userCentrality=-999
fi

#If the User has specfied a particular species and/or charge then only do that species
if [ ! -z $userSpecies ]; then

    #If the User has specified a particular charge then only do that charge
    if [ ! -z $userCharge ]; then
        root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",$userSpecies,$userCharge,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) #>> $logDir/SpectraFitAnddNdy.log 2>&1
    else
	root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",$userSpecies,-1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
	root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",$userSpecies,1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
    fi

    exit 0
fi

#If the user has specified only a particular charge
if [ ! -z $userCharge ]; then

    root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",0,$userCharge,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
    root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",1,$userCharge,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
    root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",2,$userCharge,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1

    exit 0
fi

#If the user has specified neither charge nor species (the default)
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",0,-1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",0,1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",1,-1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",1,1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",2,-1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$spectraFile\",\"$resultsFile\",2,1,\"$starLib\",$energy,\"$eventConfig\",$userCentrality,$userRapidity\) >> $logDir/SpectraFitAnddNdy.log 2>&1
    

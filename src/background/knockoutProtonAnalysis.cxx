//This code uses the histograms in the output file created by knockoutProtons.cxx
//to determine the correction factor curves for the knockout protons.

#include <iostream>
#include <vector>
#include <utility>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLegend.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

TH1D *protonPlus, *protonMinus;

//____________________________________________________________________
Double_t AntiProtons(Double_t *x, Double_t *par){

  Double_t dca = x[0];
  Double_t r = par[0];
  
  return protonMinus->GetBinContent(protonMinus->FindBin(dca)) / r;
  
}

//_____________________________________________________________________
Double_t KnockoutFunc(Double_t *x, Double_t *par){

  Double_t dca = x[0];
  Double_t A   = par[0];
  Double_t d0  = par[1];
  Double_t a   = par[2];
  
  return A * TMath::Power(1.0 - TMath::Exp(-dca/d0),a);
}

//____________________________________________________________________
Double_t KnockoutProtonFit(Double_t *x, Double_t *par){

  //From PHYSICAL REVIEW C79, 034909 (2009)
  Double_t dca = x[0];
  Double_t r   = par[0]; //pBar to p ratio
  Double_t A   = par[1]; //Magnitude
  Double_t d0  = par[2]; //Slope
  Double_t a   = par[3]; //alpha exponent

  Double_t vals[1] = {dca};
  
  Double_t pars[1] = {r};
  Double_t antiProtons = AntiProtons(vals, pars);

  Double_t backgroundPars[3] = {A,d0,a};
  Double_t background = KnockoutFunc(vals, backgroundPars);

  return antiProtons + background;
}

//______________________________________________________________________
void knockoutProtonAnalysis(TString inputFile, TString outputFile){

  //Range of DCA over which the integral should be computed to determine
  //the knockout background fraction
  Double_t minRange(0), maxRange(1);
  
  //Create an instance of the Particle Info Class which will use the empirical fits
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion());

  //Get the Number of Centrality Bins
  const int nCentralityBins = GetNCentralityBins();
  
  //Open the inputFile and load the histograms
  TFile *inFile = new TFile(inputFile,"READ");
  std::vector< std::pair<TH3D *, TH3D *> > dcaDistribution3D(nCentralityBins);
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    dcaDistribution3D.at(iCentBin).first = (TH3D *)inFile->
      Get(Form("glDCA_ProtonPlus_CentBin%d",iCentBin));
    dcaDistribution3D.at(iCentBin).second = (TH3D *)inFile->
      Get(Form("glDCA_ProtonMinus_CentBin%d",iCentBin));

  }//End Loop Over iCentBin to Load Histograms

  //One Dimensional Histograms
  std::vector< std::vector < std::vector< std::pair<TH1D *,TH1D *> > > > dcaDistribution
    (nCentralityBins,std::vector< std::vector< std::pair<TH1D *, TH1D *> > >
     (nRapidityBins, std::vector< std::pair<TH1D *, TH1D *> >
      (nmTm0Bins)));

  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	double rapidity = GetRapidityRangeCenter(yIndex);
	double mTm0     = GetmTm0RangeCenter(mTm0Index);
	int yBin    = dcaDistribution3D.at(iCentBin).first->GetXaxis()->FindBin(rapidity);
	int mTm0Bin = dcaDistribution3D.at(iCentBin).first->GetYaxis()->FindBin(mTm0);
	
	dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).first =
	  (dcaDistribution3D.at(iCentBin).first->ProjectionZ(Form("%s_%d_%d",
								  dcaDistribution3D.at(iCentBin).first->GetName(),
								  yIndex,mTm0Index),yBin,yBin,mTm0Bin,mTm0Bin));

	dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second =
	  (dcaDistribution3D.at(iCentBin).second->ProjectionZ(Form("%s_%d_%d",
								   dcaDistribution3D.at(iCentBin).second->GetName(),
								   yIndex,mTm0Index),yBin,yBin,mTm0Bin,mTm0Bin));

      }//End Loop Over mTm0 Bins
    }//End Loop Over Rapidity Bins
  }//End Loop Over iCentBin

  /*
  //Combine all of the anti-proton DCA histogram into a centrality integrated
  //histogram for each rapidity and mTm0 combination
  std::vector< std::vector< TH1D *> > centIntAntiProtonDCA
    (nRapidityBins, std::vector<TH1D *> (nmTm0Bins,(TH1D *)NULL));
  for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
    for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
      for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

	if (!centIntAntiProtonDCA.at(yIndex).at(mTm0Index)) {
	  centIntAntiProtonDCA.at(yIndex).at(mTm0Index) =
	    new TH1D(*dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second);
	  centIntAntiProtonDCA.at(yIndex).at(mTm0Index)->Sumw2();
	}
	else {
	  centIntAntiProtonDCA.at(yIndex).at(mTm0Index)->Add(dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second);
	  
	}	
      }//End Loop Over iCentBin

      //Loop Over the cent bins one more time to replace the antiproton dca distribution pointer
      //with the centrality integrated one.
      if (GetCollisionEnergy() < 8.0){
	for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	  dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second = centIntAntiProtonDCA.at(yIndex).at(mTm0Index);
	}
      }
    }//End Loop Over mTm0Index
  }//End Loop Over yIndex
  */
  
  //Create a Directory in the outputFile to write to
  TFile *outFile = new TFile(outputFile,"UPDATE");
  if (!outFile->cd("ProtonPlus")){
    outFile->mkdir("ProtonPlus");
    outFile->cd("ProtonPlus");
  }
  gDirectory->mkdir("KnockoutBackgroundGraphs");
  gDirectory->mkdir("KnockoutBackgroundFits");

  //Background Graphs as a function of mTm0
  std::vector< std::vector<TGraphErrors *> > backgroundGraph(nCentralityBins,std::vector<TGraphErrors *>
							     (nRapidityBins,(TGraphErrors *)NULL));
  std::vector<std::vector<TF1 * > > backgroundGraphFit(nCentralityBins,std::vector<TF1 *>
						       (nRapidityBins,(TF1 *)NULL));
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      backgroundGraph.at(iCentBin).at(yIndex) = new TGraphErrors();
      backgroundGraph.at(iCentBin).at(yIndex)->SetMarkerStyle(kFullCircle);
      backgroundGraph.at(iCentBin).at(yIndex)->SetMarkerColor(kBlack);
      backgroundGraph.at(iCentBin).at(yIndex)->SetName(Form("knockoutProton_%d_%d",iCentBin,yIndex));
      backgroundGraph.at(iCentBin).at(yIndex)->SetTitle(Form("#splitline{Au+Au #sqrt{s_{NN}} = %.03g | Centrality=[%d,%d]%%}{y_{p}=[%.03g,%.03g]};m_{T}-m_{p} (GeV/c^{2});#int_{%g}^{%g}Background(DCA) #/ #int_{%g}^{%g}Protons(DCA)",
							     GetCollisionEnergy(),
							     iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
							     (int)GetCentralityPercents().at(iCentBin),
							     GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
							     minRange,maxRange,
							     minRange,maxRange));
      backgroundGraph.at(iCentBin).at(yIndex)->GetYaxis()->SetRangeUser(-.2,.3);

      backgroundGraphFit.at(iCentBin).at(yIndex) =
	new TF1(Form("%s_Fit",backgroundGraph.at(iCentBin).at(yIndex)->GetName()),
		"expo",0,2.0);
      backgroundGraphFit.at(iCentBin).at(yIndex)->SetNpx(1000);
      backgroundGraphFit.at(iCentBin).at(yIndex)->SetParameter(1,-50);
      backgroundGraphFit.at(iCentBin).at(yIndex)->SetParLimits(1,-100,-1);
    }//End Loop Over yIndex
  }//End Loop Over iCentBin
  
  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  canvas->SetRightMargin(.05);
  canvas->SetTopMargin(.05);
  canvas->SetLeftMargin(.12);
  canvas->SetTicks();

  TCanvas *paramCanvas = new TCanvas("parameterCanvas","parameterCanvas",20,700,1800,400);
  paramCanvas->Divide(4,1);
  paramCanvas->Update();
  
  TCanvas *backgroundCanvas = new TCanvas("backgroundCanvas","backgroundCanvas",900,20,800,600);
  backgroundCanvas->SetTopMargin(.05);
  backgroundCanvas->SetRightMargin(.02);
  backgroundCanvas->SetLeftMargin(.16);
  backgroundCanvas->SetTicks();
    
  std::vector<TGraphErrors *> parGraph(4,(TGraphErrors *)NULL);
  for (unsigned int iPar=0; iPar<parGraph.size(); iPar++){
    parGraph.at(iPar) = new TGraphErrors();
    parGraph.at(iPar)->SetMarkerStyle(kFullCircle);
    parGraph.at(iPar)->SetTitle(";m_{T}-m_{p} (GeV/c^{2})");
  }
  
  TF1 *fit = new TF1("fit",KnockoutProtonFit,0,3,4);
  fit->SetNpx(10000);
  fit->SetLineColor(kRed);
  fit->SetLineWidth(3);
  fit->SetLineStyle(1);

  TF1 *signal = new TF1("signal",AntiProtons,0,3,1);
  signal->SetNpx(10000);
  signal->SetLineColor(kBlue);
  signal->SetLineWidth(3);
  signal->SetLineStyle(1);
  
  TF1 *background = new TF1("background",KnockoutFunc,0,3,3);
  background->SetNpx(10000);
  background->SetLineColor(kBlack);
  background->SetLineWidth(3);
  background->SetLineStyle(7);

  for (int iCentBin=nCentralityBins-1; iCentBin>=0; iCentBin--){
    //if (iCentBin != 0) continue;
    //Clear the Parameter Graphs
    for (unsigned int iGraph=0; iGraph<parGraph.size(); iGraph++){
      for (int iPoint=parGraph.at(iGraph)->GetN(); iPoint>=0; iPoint--)
	if (parGraph.at(iGraph)->GetN() > 0)
	  parGraph.at(iGraph)->RemovePoint(iPoint);
    }
    
    for (int yIndex=GetMinRapidityIndexOfInterest(PROTON); yIndex<=GetMaxRapidityIndexOfInterest(PROTON); yIndex++){
      
      //if (yIndex != 20) continue;

      //The fit is performed iteravely.
      //In the zeroth round all the parameters are left free
      //In the first round the pbar/p ratio is fixed
      const int nRounds(2);
      for (int iRound=0; iRound<nRounds; iRound++){
	cout <<"STARTING ROUND: " <<iRound <<endl;

	//Set Fit Parameters
	fit->SetParameters(.4,250,.3,2);
	fit->SetParLimits(0,.00001,1);
	fit->SetParLimits(1,.01,100000);
	//fit->SetParLimits(2,0,100000);
	fit->FixParameter(2,.3);
	//fit->SetParLimits(3,1,1000);
	fit->FixParameter(3,1);
	
	//If Round >0 Fix the pbar/p ratio
	if (iRound > 0){
	  Double_t meanRatio = TMath::Mean(parGraph.at(0)->GetN(),parGraph.at(0)->GetY());
	  fit->SetParameter(0,meanRatio);
	  cout <<"Mean Ratio: " <<meanRatio <<endl;
	  
	}

	//Clear the Background graph
	for (int iPoint=backgroundGraph.at(iCentBin).at(yIndex)->GetN(); iPoint>=0; iPoint--){
	  backgroundGraph.at(iCentBin).at(yIndex)->RemovePoint(iPoint);
	}
	
	//Clear the Parameter Graphs
	for (unsigned int iGraph=iRound; iGraph<parGraph.size(); iGraph++){
	  for (int iPoint=parGraph.at(iGraph)->GetN(); iPoint>=0; iPoint--)
	    if (parGraph.at(iGraph)->GetN() > 0)
	      parGraph.at(iGraph)->RemovePoint(iPoint);
	}
	
	for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  
	  if (GetmTm0RangeCenter(mTm0Index) < .05 || GetmTm0RangeCenter(mTm0Index) > .3)
	    continue;
	  
	  protonPlus  = dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).first;
	  protonMinus = dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second;
	  
	  protonPlus->SetMarkerStyle(kFullSquare);
	  protonMinus->SetMarkerStyle(kOpenCircle);
	  
	  if (!protonPlus || !protonMinus)
	    continue;
	  
	  if (protonPlus->GetEntries() <75 || protonMinus->GetEntries() < 75)
	    continue;
	  
	  //Do the Fit
	  int status(-1);
	  int nAttempts(0);
	  while (status != 0 && nAttempts <5 )
	    status = protonPlus->Fit(fit,"RQ");
	  
	  if (status != 0){
	    cout <<"FAIL!" <<endl;
	    continue;
	  }
	  
	  //Add the parameters to the par graph
	  for (unsigned int iPar=iRound; iPar<parGraph.size(); iPar++){
	    parGraph.at(iPar)->SetPoint(parGraph.at(iPar)->GetN(),GetmTm0RangeCenter(mTm0Index),fit->GetParameter(iPar));
	    parGraph.at(iPar)->SetPointError(parGraph.at(iPar)->GetN()-1,0,fit->GetParError(iPar));
	  }

	  //Move the Signal Parameters to the Signal Function
	  signal->SetParameter(0,fit->GetParameter(0));
	  
	  //Move the Background Parameters to the background function
	  background->SetParameters(fit->GetParameter(1),fit->GetParameter(2),fit->GetParameter(3));
	  background->SetParError(0,fit->GetParError(1));
	  background->SetParError(1,fit->GetParError(2));
	  background->SetParError(2,fit->GetParError(3));
	  
	  Double_t binWidth = protonPlus->GetBinWidth(1);
	  
	  //Integrate the Fit function from 0 to 1 and get the integral error
	  Double_t fitIntegral = fit->Integral(minRange,maxRange)/binWidth;
	  Double_t fitIntegralErr = fit->IntegralError(minRange,maxRange)/binWidth;
	  
	  //Integrate the signal Function
	  Double_t signalIntegral = signal->Integral(minRange,maxRange)/binWidth;
	  Double_t signalIntegralErr(0);
	  
	  //The Difference between the Fit and signal integrals is the background integral
	  Double_t backgroundIntegral = fitIntegral - signalIntegral;
	  Double_t backgroundIntegralErr = sqrt(pow(fitIntegralErr,2) + pow(signalIntegralErr,2));
	  
	  //Get the Proton Histogram Integral and Error
	  Double_t protonIntegralErr(0);
	  Double_t protonIntegral = protonPlus->IntegralAndError(protonPlus->FindBin(minRange),
								 protonPlus->FindBin(maxRange),protonIntegralErr); //Bin Indices
	  
	  //Compute the Background Ratio and Error
	  Double_t backgroundRatio = backgroundIntegral / protonIntegral;
	  Double_t backgroundRatioErr = backgroundRatio * sqrt(pow(backgroundIntegralErr/backgroundIntegral,2) +
							       pow(protonIntegralErr/protonIntegral,2));
	  
	  if (TMath::IsNaN(backgroundRatioErr))
	    continue;

	  /*
	  cout <<GetmTm0RangeCenter(mTm0Index) <<" "
	       <<ConvertmTm0ToPt(GetmTm0RangeCenter(mTm0Index),particleInfo->GetParticleMass(PROTON))
	       <<" " <<backgroundRatio <<" +- " <<backgroundRatioErr <<endl;
	  */
	  
	  //Integrate the background function and store the integral in the in the background graph
	  //Note that the integral is done from 0 to 1 because the DCA cut in the analysis is 1
	  backgroundGraph.at(iCentBin).at(yIndex)->SetPoint(backgroundGraph.at(iCentBin).at(yIndex)->GetN(),
							    GetmTm0RangeCenter(mTm0Index),
							    backgroundRatio);
	  backgroundGraph.at(iCentBin).at(yIndex)->SetPointError(backgroundGraph.at(iCentBin).at(yIndex)->GetN()-1,
								 0,backgroundRatioErr);
	  
	  //Draw the Histograms and Fits
	  canvas->cd();
	  TH1F *frame = canvas->DrawFrame(0,.1,3,1.15*protonPlus->GetBinContent(protonPlus->GetMaximumBin()));
	  frame->SetTitle(Form("#splitline{Au+Au #sqrt{s_{NN}} = %.03g GeV | Centrality=[%d,%d]%%}{y_{%s}=[%.03g,%.03g] | m_{T}-m_{%s}=[%.03g,%.03g] (GeV/c^{2})}",
			       GetCollisionEnergy(),
			       iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			       (int)GetCentralityPercents().at(iCentBin),
			       particleInfo->GetParticleSymbol(PROTON,1).Data(),
			       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			       particleInfo->GetParticleSymbol(PROTON,1).Data(),
			       GetmTm0RangeLow(mTm0Index),GetmTm0RangeHigh(mTm0Index)));
	  frame->GetYaxis()->SetTitle("dN/d(glDCA)");
	  frame->GetXaxis()->SetTitle("Global DCA (cm)");
	  frame->GetYaxis()->SetTitleOffset(1.4);
	  frame->GetYaxis()->SetTitleFont(63);
	  frame->GetYaxis()->SetTitleSize(22);
	  frame->GetXaxis()->SetTitleFont(63);
	  frame->GetXaxis()->SetTitleSize(22);
	  protonPlus->Draw("E SAME");
	  protonMinus->Draw("E SAME");
	  signal->Draw("SAME");
	  background->SetLineColor(kBlack);
	  background->SetLineStyle(7);
	  background->SetLineWidth(3);
	  background->Draw("SAME");
	  canvas->Update();

	  TPaveText *title = (TPaveText *)canvas->FindObject("title");
	  title->SetTextFont(63);
	  title->SetTextSize(25);
	  title->SetX1NDC(.38);
	  title->SetY1NDC(.80);
	  title->SetX2NDC(.88);
	  title->SetY2NDC(.92);

	  TLegend *leg = new TLegend(.58,.61,.92,.80);
	  leg->SetBorderSize();
	  leg->SetFillColor(kWhite);
	  leg->SetNColumns(2);
	  leg->SetTextFont(43);
	  leg->SetTextSize(24);
	  leg->AddEntry(protonPlus,"p","P");
	  leg->AddEntry(fit,"p Fitted","L");
	  leg->AddEntry(protonMinus,"#bar{p}","P");
	  leg->AddEntry(signal,"#bar{p} Scaled","L");
	  leg->AddEntry((TObject *)NULL,"","");
	  leg->AddEntry(background,"Knockout","L");
	  leg->Draw("SAME");
	  
	  canvas->Modified();
	  canvas->Update();
	  
	  //Draw the Background Graph
	  backgroundCanvas->cd();
	  backgroundGraph.at(iCentBin).at(yIndex)->Draw("APZ");
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleOffset(1.75);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleFont(63);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleSize(22);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetXaxis()->SetTitleFont(63);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetXaxis()->SetTitleSize(22);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetYaxis()->SetRangeUser(-.1,.3);
	  backgroundCanvas->Update();

	  TPaveText *bTitle = (TPaveText *)backgroundCanvas->FindObject("title");
	  bTitle->SetTextFont(63);
	  bTitle->SetTextSize(25);
	  bTitle->SetX1NDC(.44);
	  bTitle->SetY1NDC(.80);
	  bTitle->SetX2NDC(.94);
	  bTitle->SetY2NDC(.92);

	  backgroundCanvas->Modified();
	  backgroundCanvas->Update();
	  
	  //Draw the Parameter Graphs
	  paramCanvas->cd();
	  for (unsigned int iPar=0; iPar<parGraph.size(); iPar++){
	    paramCanvas->cd(iPar+1);
	    parGraph.at(iPar)->Draw("APZ");
	    //gPad->Update();
	  }
	  paramCanvas->Update();
	  
	  //gSystem->Sleep(200);
	  /*
	  if (iCentBin == 0 && mTm0Index == 4 && iRound == 1){
	    canvas->SaveAs("SingleBackgroundFit.C");
	  }
	  */	  
	  
	}//End Loop Over mTm0Index
      }//End Loop Over Rounds

      //Fit the Backgroudn Graph
      backgroundGraph.at(iCentBin).at(yIndex)->Fit(backgroundGraphFit.at(iCentBin).at(yIndex),"REX0");

      backgroundCanvas->Modified();
      backgroundCanvas->Update();

      if (iCentBin == 0){
	backgroundCanvas->SaveAs("backgroundCanvas.C");
      }

      //Save
      outFile->cd();
      outFile->cd("ProtonPlus/KnockoutBackgroundGraphs");
      backgroundGraph.at(iCentBin).at(yIndex)->Write("",1);
      
      outFile->cd();
      outFile->cd("ProtonPlus/KnockoutBackgroundFits");
      backgroundGraphFit.at(iCentBin).at(yIndex)->Write("",1);
      
    }//End Loop Over yIndex
  }//End Loop Over Centrality Bin


  
}

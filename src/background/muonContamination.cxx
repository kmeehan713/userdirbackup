//This code uses the output of the background simulations to determine
//the fractional background of muons in the pion spectra.

#include <iostream>
#include <vector>
#include <utility>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TH2.h>
#include <TH3.h>
#include <TMath.h>
#include <TF1.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFitter.h>
#include <TDirectory.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "StyleSettings.h"

Bool_t draw=false;
Bool_t save=true;

//____MAIN___________________________________________________________
void muonContamination(TString inputFile, TString outputFile){

  //Create an instance of the ParticleInfo Class
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion());

  //Get the Number of Centrality Bins
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Open the inputFile and Load the Histograms
  TFile *inFile = new TFile(inputFile,"READ");
  std::vector < std::pair<TH2F *, TH2F *> > pionHisto
    (nCentralityBins,std::make_pair((TH2F *)NULL,(TH2F *)NULL));
  std::vector < std::pair<TH2F *, TH2F *> > muonHisto
    (nCentralityBins,std::make_pair((TH2F *)NULL,(TH2F *)NULL));
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    pionHisto.at(iCentBin).first = (TH2F *)inFile->
      Get(Form("PionMuonContamination/pionCount_PionMinus_cent%02d",iCentBin));
    pionHisto.at(iCentBin).second = (TH2F *)inFile->
      Get(Form("PionMuonContamination/pionCount_PionPlus_cent%02d",iCentBin));

    muonHisto.at(iCentBin).first = (TH2F *)inFile->
      Get(Form("PionMuonContamination/muonCount_PionMinus_cent%02d",iCentBin));
    muonHisto.at(iCentBin).second = (TH2F *)inFile->
      Get(Form("PionMuonContamination/muonCount_PionPlus_cent%02d",iCentBin));

    cout <<pionHisto.at(iCentBin).first <<" " <<pionHisto.at(iCentBin).second <<" "
	 <<muonHisto.at(iCentBin).first <<" " <<muonHisto.at(iCentBin).second <<endl;
    
  }//End Loop Over Cent Bins to get the histograms from the file

  //Open the output file for writing
  TFile *outFile = new TFile(outputFile,save ? "UPDATE":"READ");
  outFile->cd();

  //Make the Graphs to hold the Background Fraction Plots and functions to fit them
  std::vector <std::vector< std::pair<TGraphAsymmErrors *, TGraphAsymmErrors *> > > muonBackground
    (nCentralityBins, std::vector< std::pair<TGraphAsymmErrors *, TGraphAsymmErrors *> >
     (nRapidityBins, std::make_pair((TGraphAsymmErrors *)NULL,(TGraphAsymmErrors *)NULL)));
  std::vector <std::vector< std::pair<TF1 *, TF1 *> > > muonBackgroundFit
    (nCentralityBins, std::vector< std::pair<TF1 *, TF1 *> >
     (nRapidityBins, std::make_pair((TF1 *)NULL,(TF1 *)NULL)));
  std::vector <std::vector< std::pair<TGraphErrors *, TGraphErrors *> > > muonBackgroundFitSys
    (nCentralityBins, std::vector< std::pair<TGraphErrors *, TGraphErrors *> >
     (nRapidityBins, std::make_pair((TGraphErrors *)NULL,(TGraphErrors *)NULL)));
  
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Background Graphs
      muonBackground.at(iCentBin).at(yIndex).first = new TGraphAsymmErrors();
      muonBackground.at(iCentBin).at(yIndex).second = new TGraphAsymmErrors();

      muonBackground.at(iCentBin).at(yIndex).first->
	SetName(Form("muonBackground_%s_cent%02d_yIndex%02d",
		     particleInfo->GetParticleName(PION,-1).Data(),iCentBin,yIndex));
      muonBackground.at(iCentBin).at(yIndex).first->
	SetTitle(Form("%s Contamination Fraction | Cent=[%d,%d]%% | y_{%s}^{Reco}=[%.2f,%.2f];(m_{T}-m_{%s})^{Reco} (GeV/c^{2}); #mu Contamination Fraction",
		      particleInfo->GetParticleSymbol(MUON,-1).Data(),
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      particleInfo->GetParticleSymbol(PION,-1).Data(),
		      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
		      particleInfo->GetParticleSymbol(PION,-1).Data()));

      muonBackground.at(iCentBin).at(yIndex).second->
	SetName(Form("muonBackground_%s_cent%02d_yIndex%02d",
		     particleInfo->GetParticleName(PION,1).Data(),iCentBin,yIndex));
      muonBackground.at(iCentBin).at(yIndex).second->
	SetTitle(Form("%s Contamination Fraction | Cent=[%d,%d]%% | y_{%s}^{Reco}=[%.2f,%.2f];(m_{T}-m_{%s})^{Reco} (GeV/c^{2}); #mu Contamination Fraction",
		      particleInfo->GetParticleSymbol(MUON,1).Data(),
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      particleInfo->GetParticleSymbol(PION,1).Data(),
		      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
		      particleInfo->GetParticleSymbol(PION,1).Data()));

      muonBackground.at(iCentBin).at(yIndex).first->SetMarkerColor(GetCentralityColor(iCentBin));
      muonBackground.at(iCentBin).at(yIndex).first->SetMarkerStyle(particleInfo->GetParticleMarker(PION,-1));

      muonBackground.at(iCentBin).at(yIndex).second->SetMarkerColor(GetCentralityColor(iCentBin));
      muonBackground.at(iCentBin).at(yIndex).second->SetMarkerStyle(particleInfo->GetParticleMarker(PION,1));

      //Background Fits
      muonBackgroundFit.at(iCentBin).at(yIndex).first =
	new TF1(Form("%s_Fit",muonBackground.at(iCentBin).at(yIndex).first->GetName()),"expo",0,2);
      muonBackgroundFit.at(iCentBin).at(yIndex).second =
	new TF1(Form("%s_Fit",muonBackground.at(iCentBin).at(yIndex).second->GetName()),"expo",0,2);

      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetParLimits(0,-10,0);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetParLimits(1,-20,0);

      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetParLimits(0,-10,0);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetParLimits(1,-20,0);

      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineStyle(1);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineWidth(2);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineColor(GetCentralityColor(iCentBin));

      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineStyle(7);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineWidth(2);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineColor(GetCentralityColor(iCentBin));

      //Confidence Intervals
      int npx = muonBackgroundFit.at(iCentBin).at(yIndex).first->GetNpx();
      Double_t fitMin(0),fitMax(0);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->GetRange(fitMin,fitMax);
      Double_t stepSize = fabs(fitMax-fitMin)/(double)npx;
      muonBackgroundFitSys.at(iCentBin).at(yIndex).first = new TGraphErrors(npx);
      muonBackgroundFitSys.at(iCentBin).at(yIndex).second = new TGraphErrors(npx);
      for (int i=0; i<npx; i++){
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first->SetPoint(i,fitMin+i*stepSize,0);
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second->SetPoint(i,fitMin+i*stepSize,0);
      }

      muonBackgroundFitSys.at(iCentBin).at(yIndex).first->
	SetName(Form("%s_Sys",muonBackgroundFit.at(iCentBin).at(yIndex).first->GetName()));
      muonBackgroundFitSys.at(iCentBin).at(yIndex).first->SetFillColor(GetCentralityColor(iCentBin));
      muonBackgroundFitSys.at(iCentBin).at(yIndex).first->SetFillStyle(3001);

      muonBackgroundFitSys.at(iCentBin).at(yIndex).second->
	SetName(Form("%s_Sys",muonBackgroundFit.at(iCentBin).at(yIndex).second->GetName()));
      muonBackgroundFitSys.at(iCentBin).at(yIndex).second->SetFillColor(GetCentralityColor(iCentBin));
      muonBackgroundFitSys.at(iCentBin).at(yIndex).second->SetFillStyle(3001);
      
    }//End Loop Over Rapidity Bins
  }//End Loop Over Centraltiy bins to create the background graphs
  
  //Compute the Muon Contamination Background Fraction
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    for (int iBinX=1; iBinX<pionHisto.at(iCentBin).first->GetNbinsX(); iBinX++){

      //Get the Rapidity Bin Index
      int yIndex = GetRapidityIndex(pionHisto.at(iCentBin).first->GetXaxis()->GetBinCenter(iBinX));

      //Negative
      muonBackground.at(iCentBin).at(yIndex).first->
	BayesDivide(muonHisto.at(iCentBin).first->ProjectionY("muonminus",iBinX,iBinX)->Rebin(2),
		    pionHisto.at(iCentBin).first->ProjectionY("pionminus",iBinX,iBinX)->Rebin(2));

      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).first,.1,true);
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).first,1.0,false);

      //Fit and Confidence Interval
      if (muonBackground.at(iCentBin).at(yIndex).first->GetN() > 2){
	muonBackground.at(iCentBin).at(yIndex).first->Fit(muonBackgroundFit.at(iCentBin).at(yIndex).first,"R");
	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(muonBackgroundFitSys.at(iCentBin).at(yIndex).first,.683);
      }
      else {
	delete muonBackground.at(iCentBin).at(yIndex).first;
	delete muonBackgroundFit.at(iCentBin).at(yIndex).first;
	delete muonBackgroundFitSys.at(iCentBin).at(yIndex).first;

	muonBackground.at(iCentBin).at(yIndex).first = NULL;
	muonBackgroundFit.at(iCentBin).at(yIndex).first = NULL;
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first = NULL;
      }
      
      //Positive
      muonBackground.at(iCentBin).at(yIndex).second->
	BayesDivide(muonHisto.at(iCentBin).second->ProjectionY("muonplus",iBinX,iBinX)->Rebin(2),
		    pionHisto.at(iCentBin).second->ProjectionY("pionplus",iBinX,iBinX)->Rebin(2));
      
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).second,.1,true);
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).second,1.0,false);

      //Fit and Confidience Interval
      if (muonBackground.at(iCentBin).at(yIndex).second->GetN() > 2){
	muonBackground.at(iCentBin).at(yIndex).second->Fit(muonBackgroundFit.at(iCentBin).at(yIndex).second,"R");
	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(muonBackgroundFitSys.at(iCentBin).at(yIndex).second,.683);
      }
      else {
	delete muonBackground.at(iCentBin).at(yIndex).second;
	delete muonBackgroundFit.at(iCentBin).at(yIndex).second;
	delete muonBackgroundFitSys.at(iCentBin).at(yIndex).second;
	
	muonBackground.at(iCentBin).at(yIndex).second = NULL;
	muonBackgroundFit.at(iCentBin).at(yIndex).second = NULL;
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second = NULL;
	
      }

    }//End Loop Over Xaxis (rapidity Axis)

    
  }//End Loop Over centrality bins to compute background


  //Make a canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    canvas->SetTicks();
    muonBackground.at(8).at(20).second->Draw("APZ");
    muonBackgroundFitSys.at(8).at(20).second->Draw("3");
  }

  //Make the Directories if necessary
  outFile->cd();
  outFile->mkdir("PionMinus");
  outFile->mkdir("PionPlus");
  
  //Save 
  outFile->cd();
  outFile->cd("PionMinus");
  gDirectory->mkdir("MuonBackgroundGraphs");
  gDirectory->mkdir("MuonBackgroundFits");

  outFile->cd();
  outFile->cd("PionPlus");
  gDirectory->mkdir("MuonBackgroundGraphs");
  gDirectory->mkdir("MuonBackgroundFits");

  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundGraphs",particleInfo->GetParticleName(PION,-1).Data()));
      if (muonBackground.at(iCentBin).at(yIndex).first){
	muonBackground.at(iCentBin).at(yIndex).first->Write();
      }
      else {
	continue;
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundFits",particleInfo->GetParticleName(PION,-1).Data()));
      if (muonBackgroundFit.at(iCentBin).at(yIndex).first){
	cout <<muonBackgroundFit.at(iCentBin).at(yIndex).first <<endl;
	muonBackgroundFit.at(iCentBin).at(yIndex).first->Write();
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first->Write();
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundGraphs",particleInfo->GetParticleName(PION,1).Data()));
      if (muonBackground.at(iCentBin).at(yIndex).second){
	muonBackground.at(iCentBin).at(yIndex).second->Write();
      }
      else {
	continue;
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundFits",particleInfo->GetParticleName(PION,1).Data()));
      if (muonBackgroundFit.at(iCentBin).at(yIndex).second){
	cout <<muonBackgroundFit.at(iCentBin).at(yIndex).second <<endl;
	muonBackgroundFit.at(iCentBin).at(yIndex).second->Write();
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second->Write();
      }
      
    }//End Loop Over yIndex
  }//End Loop Over centIndex
  
  
}

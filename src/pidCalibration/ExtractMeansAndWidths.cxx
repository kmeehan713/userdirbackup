#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TF1.h>
#include <TString.h>
#include <TTree.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "utilityFunctions.h"
#include "fitZTPCUtilities.h"


//Choose to draw or not
bool draw=false;

//______MAIN________________________________________________
void ExtractMeansAndWidths(TString inputYieldHistoFile, TString pidCalibrationFile){

  gStyle->SetOptFit(1);
  
  //Create an instance of Particle Info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibrationFile);

  //Create new Directory in the pidCalibration File
  TFile *pidFile = new TFile(pidCalibrationFile,"UPDATE");
  pidFile->mkdir("MeanAndWidthGraphs");
  pidFile->mkdir("MeanAndWidthFits");
  
  //Open the input Yield Histogram file
  TFile *yieldHistoFile = new TFile(inputYieldHistoFile,"READ");
  
  //Create a Canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,1600,900);
    canvas->Divide(3,2);
    canvas->cd(3);
    gPad->SetLogy();
    canvas->cd(6);
    gPad->SetLogy();
    
  }
  
  //Particles Whose means and widths should be extracted
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);

  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Containers for Yield Histograms
  std::vector< TH1D *> zTPCHistos(nmTm0Bins, (TH1D *) NULL);
  std::vector< TH1D *> zTOFHistos(nmTm0Bins, (TH1D *) NULL);
  std::vector< std::vector<TH1D *> > tofOptZTPCHistos(nParticles,std::vector< TH1D *>
						      (nmTm0Bins, (TH1D *) NULL));

  //Containers for Graphs, fits, and confidence intervals
  std::vector< TGraphErrors *> meanGraphTPC(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTPC(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> meanGraphTOF(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTOF(nParticles, (TGraphErrors *)NULL);

  std::vector< TGraphErrors *> meanGraphTPCFit(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTPCFit(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> meanGraphTOFFit(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTOFFit(nParticles, (TGraphErrors *)NULL);
  
  std::vector< TGraphErrors *> meanGraphTPCConf(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTPCConf(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> meanGraphTOFConf(nParticles, (TGraphErrors *)NULL);
  std::vector< TGraphErrors *> widthGraphTOFConf(nParticles, (TGraphErrors *)NULL);

  

  //Single Guassian fit used for all mean and width extractions
  Double_t rangeEntries(0);
  Double_t minFitRange = -100;
  Double_t maxFitRange = 100;
  TF1 tpcFit("","gaus(0)",-100,100);
  TF1 tofFit("","gaus(0)",-100,100);
  
  //For each particle species of interest and each rapidity bin,
  //parameterize each particle species means and widths using
  //either to full distributions or the tof optimized distributions
  //depending on the proximity of the particles to each other
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    int particleSpecies = particles.at(iParticle);

    //Load the 3D Histograms from the File
    yieldHistoFile->cd();
    yieldHistoFile->cd("YieldHistograms");

    //Combine all centrality bins and charges into one 3D histo just like zTOFHisto3D
    TH3D *zTPCHisto3D = NULL;
    //for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    if (!zTPCHisto3D){
      zTPCHisto3D = (TH3D *)gDirectory->
	Get(Form("zTPC_%s_Cent%d_Total",particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
		 nCentralityBins-1));
      TH3D *temp = (TH3D *)gDirectory->
	Get(Form("zTPC_%s_Cent%d_Total",particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
		 nCentralityBins-1));
      zTPCHisto3D->Add(temp);
      //delete temp;
    }
      /*
      else{
	TH3D *temp = (TH3D *)gDirectory->
	  Get(Form("zTPC_%s_Cent%d_Total",particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
		   nCentralityBins-1));
	zTPCHisto3D->Add(temp);
	//delete temp;

	temp = (TH3D *)gDirectory->
	  Get(Form("zTPC_%s_Cent%d_Total",particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
		   nCentralityBins-1));
	zTPCHisto3D->Add(temp);
	//delete temp;	
      }
      */
      //}
    
    TH3D *zTOFHisto3D = (TH3D *)gDirectory->
      Get(Form("zTOF_AllCentCharge_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data()));
    
    std::vector<TH3D *> tofOptZTPCHisto3D (nParticles, (TH3D *) NULL);
    for (unsigned int iTofOptIndex=0; iTofOptIndex<nParticles; iTofOptIndex++){
      tofOptZTPCHisto3D.at(iTofOptIndex) = (TH3D *)gDirectory->
	Get(Form("zTPC_%s_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data(),
		 particleInfo->GetParticleName(particles.at(iTofOptIndex)).Data()));
      
    }//End Loop Over Tof Opt Particles

    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Only do rapidity bins of interest
      if ((int)yIndex < GetMinRapidityIndexOfInterest(particleSpecies) ||
	  (int)yIndex > GetMaxRapidityIndexOfInterest(particleSpecies))
	continue;

      //Compute the Rapidity value for this bin and set the bichsel curve
      Double_t rapidity = GetRapidityRangeCenter(yIndex);
      if (!particleInfo->SetEmpiricalBichselFunction(particleSpecies,yIndex))
	continue;

      //Create the Means and Width Graphs for the Sub Species
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	int subSpecies = particles.at(iSubSpecies);

	//Graphs
	meanGraphTPC.at(iSubSpecies) = new TGraphErrors();
	widthGraphTPC.at(iSubSpecies) = new TGraphErrors();
	meanGraphTOF.at(iSubSpecies) = new TGraphErrors();
	widthGraphTOF.at(iSubSpecies) = new TGraphErrors();


	//Graphs
	meanGraphTPC.at(iSubSpecies)->SetName(Form("meanGraph_%s_%s_%d",
				       particleInfo->GetParticleName(particleSpecies).Data(),
				       particleInfo->GetParticleName(subSpecies).Data(),
				       yIndex));
	widthGraphTPC.at(iSubSpecies)->SetName(Form("widthGraph_%s_%s_%d",
					particleInfo->GetParticleName(particleSpecies).Data(),
					particleInfo->GetParticleName(subSpecies).Data(),
					yIndex));
	
	meanGraphTPC.at(iSubSpecies)->SetTitle(Form("zTPC Mean : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};ZTPC_{%s}^{%s}",
					particleInfo->GetParticleName(particleSpecies).Data(),
					particleInfo->GetParticleName(subSpecies).Data(),
					rapidity,
					particleInfo->GetParticleSymbol(particleSpecies).Data(),
					particleInfo->GetParticleSymbol(particleSpecies).Data(),
					particleInfo->GetParticleSymbol(subSpecies).Data()));
	widthGraphTPC.at(iSubSpecies)->SetTitle(Form("zTPC Width : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};#sigma_{%s}^{%s}",
					 particleInfo->GetParticleName(particleSpecies).Data(),
					 particleInfo->GetParticleName(subSpecies).Data(),
					 rapidity,
					 particleInfo->GetParticleSymbol(particleSpecies).Data(),
					 particleInfo->GetParticleSymbol(particleSpecies).Data(),
					 particleInfo->GetParticleSymbol(subSpecies).Data()));
	
	meanGraphTOF.at(iSubSpecies)->SetName(Form("meanGraphTOF_%s_%s_%d",
					  particleInfo->GetParticleName(particleSpecies).Data(),
					  particleInfo->GetParticleName(subSpecies).Data(),
					  yIndex));
	widthGraphTOF.at(iSubSpecies)->SetName(Form("widthGraphTOF_%s_%s_%d",
					   particleInfo->GetParticleName(particleSpecies).Data(),
					   particleInfo->GetParticleName(subSpecies).Data(),
					   yIndex));
	
	meanGraphTOF.at(iSubSpecies)->SetTitle(Form("zTOF Mean : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};ZTOF_{%s}^{%s}",
					   particleInfo->GetParticleName(particleSpecies).Data(),
					   particleInfo->GetParticleName(subSpecies).Data(),
					   rapidity,
					   particleInfo->GetParticleSymbol(particleSpecies).Data(),
					   particleInfo->GetParticleSymbol(particleSpecies).Data(),
					   particleInfo->GetParticleSymbol(subSpecies).Data()));
	widthGraphTOF.at(iSubSpecies)->SetTitle(Form("zTOF Width : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};#sigma_{%s}^{%s}",
					    particleInfo->GetParticleName(particleSpecies).Data(),
					    particleInfo->GetParticleName(subSpecies).Data(),
					    rapidity,
					    particleInfo->GetParticleSymbol(particleSpecies).Data(),
					    particleInfo->GetParticleSymbol(particleSpecies).Data(),
					    particleInfo->GetParticleSymbol(subSpecies).Data()));

	//Graphs
	meanGraphTPC.at(iSubSpecies)->SetMarkerStyle(kFullCircle);
	widthGraphTPC.at(iSubSpecies)->SetMarkerStyle(kFullCircle);
	meanGraphTOF.at(iSubSpecies)->SetMarkerStyle(kFullCircle);
	widthGraphTOF.at(iSubSpecies)->SetMarkerStyle(kFullCircle);
	
	meanGraphTPC.at(iSubSpecies)->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	widthGraphTPC.at(iSubSpecies)->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	meanGraphTOF.at(iSubSpecies)->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	widthGraphTOF.at(iSubSpecies)->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));

	
      }
      
      
      //Load the 1D Yield Histograms
      LoadYieldHistograms(zTPCHisto3D,&zTPCHistos,"TPC",yIndex);
      LoadYieldHistograms(zTOFHisto3D,&zTOFHistos,"TOF",yIndex);
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++)
	LoadYieldHistograms(tofOptZTPCHisto3D.at(iSubSpecies),&tofOptZTPCHistos.at(iSubSpecies),
			    "TPC",yIndex);

      if (draw){
	canvas->cd(1);
	gPad->DrawFrame(0.0,-1,1.5,2);
	gPad->Update();
	canvas->cd(2);
	gPad->DrawFrame(0.0,.06,1.5,.12);
	canvas->cd(4);
	gPad->DrawFrame(0.0,-.8,1.5,.8);
	gPad->Update();
	canvas->cd(5);
	gPad->DrawFrame(0.0,.008,1.5,.02);
	gPad->Update();
      }
      
      for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	//Bin Kinematic Info
	Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
	Double_t mass = particleInfo->GetParticleMass(particleSpecies);
	Double_t pT   = ConvertmTm0ToPt(mTm0,mass);
	Double_t rapidity = GetRapidityRangeCenter(yIndex);
	Double_t pZ   = ComputepZ(mTm0+mass,rapidity);
	Double_t pTotal = ComputepTotal(pT,pZ);

	//Only Do parameterization in mTm0 Range
	if (mTm0 < .1)
	  continue;
	if (mTm0 > 1.5)
	  break;

	for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){

	  int subSpecies = particles.at(iSubSpecies);

	  tpcFit.SetLineColor(particleInfo->GetParticleColor(subSpecies));
	  tofFit.SetLineColor(particleInfo->GetParticleColor(subSpecies));

	  //---- TPC ------
	  if (!zTPCHistos.at(mTm0Index)){
	    cout <<"No TPC Histogram...Skipping" <<endl;
	    continue;
	  }

	  //Mean Prediction
	  Double_t tpcWidth = 0.08;
	  Double_t tpcMeanPrediction = particleInfo->PredictZTPC(pTotal,particleSpecies,subSpecies);
	  
	  //Determine if the Current subspecies is overlapping with another species
	  //if not then fit the full histogram, if yes then fit the tof optimized histo
	  Bool_t isOverlapping = false;
	  for (unsigned int i=0; i<nParticles; i++){

	    //Skip Current Sub Species
	    if (iSubSpecies == i)
	      continue;

	    //If the Difference between the predicted zTPC values is less than 2*width
	    //then it is overlapping
	    Double_t confoundMean = particleInfo->PredictZTPC(pTotal,particleSpecies,i);
	    if (fabs(tpcMeanPrediction - confoundMean) < 4.0 * tpcWidth){
	      isOverlapping = true;
	    }    
	  }//End Check for overlapping

	  //Determine which histogram should be fit
	  TH1D *histoToFit = NULL;

	  //Always perfer the TOF Optimized Distribution if it has enough entries
	  if (tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index)->GetEntries() > 500)
	    histoToFit = tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index);
	  
	  //If Overlapping fit and enough entries in the tof optimized histos
	  else if (isOverlapping ){
	    if (tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index)->GetEntries() > 100)
	      histoToFit = tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index);
	    else
	      histoToFit = NULL;//zTPCHistos.at(mTm0Index);
	  }
	  //If NOT Overlapping fit full histo
	  else
	    histoToFit = zTPCHistos.at(mTm0Index);

	  if (histoToFit){
	    if (histoToFit->Integral(histoToFit->FindBin(tpcMeanPrediction-2*tpcWidth),
				     histoToFit->FindBin(tpcMeanPrediction+2*tpcWidth)) > 100){
	      
	      //Fit Range
	      minFitRange = FindMinBinCenter(histoToFit,tpcMeanPrediction - 3*tpcWidth,tpcMeanPrediction);
	      maxFitRange = FindMinBinCenter(histoToFit,tpcMeanPrediction,tpcMeanPrediction + 3*tpcWidth);
	      
	      //Find the Max bin in the range and set the mean prediction to it
	      //recenter the fit range
	      tpcMeanPrediction = FindMaxBinCenter(histoToFit,
						   minFitRange,maxFitRange);
	      minFitRange = FindMinBinCenter(histoToFit,tpcMeanPrediction - 2*tpcWidth,tpcMeanPrediction);
	      maxFitRange = FindMinBinCenter(histoToFit,tpcMeanPrediction,tpcMeanPrediction + 2*tpcWidth);
	      
	      //Check for enough Entries
	      rangeEntries = histoToFit->Integral(histoToFit->FindBin(minFitRange),
						  histoToFit->FindBin(maxFitRange));
	      if (rangeEntries > 100){
		
		//Perform the Fit
		tpcFit.SetRange(minFitRange,maxFitRange);
		tpcFit.SetParameter(0,histoToFit->
				    GetBinContent(histoToFit->
						  FindBin(tpcMeanPrediction)));
		tpcFit.SetParameter(1,tpcMeanPrediction);
		tpcFit.SetParameter(2,tpcWidth);
		
		tpcFit.SetParLimits(0,tpcFit.GetParameter(0)*.8,tpcFit.GetParameter(0)*1.2);
		tpcFit.SetParLimits(1,minFitRange,maxFitRange);
		tpcFit.SetParLimits(2,.065,tpcWidth*2);
		
		histoToFit->GetXaxis()->SetRangeUser(minFitRange-.5,minFitRange+.5);
		int status = histoToFit->Fit(&tpcFit,"RNQ");
		
		//Add the Parameters to the Mean and Width Graphs
		if (status == 0 && mTm0 < 1.0 && fabs((tpcFit.GetParameter(1)/tpcMeanPrediction)-1) < .5){
		  meanGraphTPC.at(iSubSpecies)->SetPoint(meanGraphTPC.at(iSubSpecies)->GetN(),
							 mTm0,tpcFit.GetParameter(1));
		  meanGraphTPC.at(iSubSpecies)->SetPointError(meanGraphTPC.at(iSubSpecies)->GetN()-1,
							      mTm0BinWidth/2.0,tpcFit.GetParError(1));
		  widthGraphTPC.at(iSubSpecies)->SetPoint(widthGraphTPC.at(iSubSpecies)->GetN(),
							  mTm0,tpcFit.GetParameter(2));
		  widthGraphTPC.at(iSubSpecies)->SetPointError(widthGraphTPC.at(iSubSpecies)->GetN()-1,
							       mTm0BinWidth/2.0,tpcFit.GetParError(2));
		}
		
		if (draw){
		  canvas->cd(1);
		  meanGraphTPC.at(iSubSpecies)->Draw("PZ");
		  gPad->Update();
		  canvas->cd(2);
		  widthGraphTPC.at(iSubSpecies)->Draw("PZ");
		  gPad->Update();
		  canvas->cd(3);
		  histoToFit->Draw("E");
		  tpcFit.Draw("SAME");	    
		  gPad->Update();
		  gSystem->Sleep(500);
		}
	      }
	    }
	  }//End TPC
	    
	  //---- TOF ------
	  if (!zTOFHistos.at(mTm0Index)){
	    cout <<"No TOF Histogram...Skipping" <<endl;
	    continue;
	  }
	  if (zTOFHistos.at(mTm0Index)->GetEntries() < 100){
	    continue;
	  }

	  //Mean Prediction
	  Double_t meanPrediction = particleInfo->PredictZTOF(pTotal,particleSpecies,subSpecies);

	  //Fit Range
	  Double_t tofWidth = .012;
	  minFitRange = FindMinBinCenter(zTOFHistos.at(mTm0Index),meanPrediction - 3*tofWidth,meanPrediction);
	  maxFitRange = FindMinBinCenter(zTOFHistos.at(mTm0Index),meanPrediction,meanPrediction + 3*tofWidth);

	  //Find the Max Bin in the range and use it as the mean prediction and recenter fit
	  meanPrediction = FindMaxBinCenter(zTOFHistos.at(mTm0Index),minFitRange,maxFitRange);
	  minFitRange = FindMinBinCenter(zTOFHistos.at(mTm0Index),meanPrediction-2*tofWidth,meanPrediction);
	  maxFitRange = FindMinBinCenter(zTOFHistos.at(mTm0Index),meanPrediction,meanPrediction + 2*tofWidth);

	  //Make Sure there are enough entries in the range
	  rangeEntries = zTOFHistos.at(mTm0Index)->
	    Integral(zTOFHistos.at(mTm0Index)->FindBin(minFitRange),
		     zTOFHistos.at(mTm0Index)->FindBin(maxFitRange));
	  if (rangeEntries < 25){
	    continue;
	  }

	  //Perform the Fit
	  tofFit.SetRange(minFitRange,maxFitRange);
	  tofFit.SetParameter(0,zTOFHistos.at(mTm0Index)->
			   GetBinContent(zTOFHistos.at(mTm0Index)->FindBin(meanPrediction)));
	  tofFit.SetParameter(1,meanPrediction);
	  tofFit.SetParameter(2,tofWidth);

	  tofFit.SetParLimits(0,tofFit.GetParameter(0)*.8,tofFit.GetParameter(0)*1.2);
	  tofFit.SetParLimits(1,minFitRange,maxFitRange);
	  tofFit.SetParLimits(2,tofWidth*.2,tofWidth*2);

	  zTOFHistos.at(mTm0Index)->GetXaxis()->SetRangeUser(minFitRange-.1,maxFitRange+.1);
	  zTOFHistos.at(mTm0Index)->Fit(&tofFit,"RNQ");

	  //Add the Parameters to the Mean and Width Graphs
	  meanGraphTOF.at(iSubSpecies)->SetPoint(meanGraphTOF.at(iSubSpecies)->GetN(),
						 mTm0,tofFit.GetParameter(1));
	  meanGraphTOF.at(iSubSpecies)->SetPointError(meanGraphTOF.at(iSubSpecies)->GetN()-1,
						      mTm0BinWidth/2.0,tofFit.GetParError(1));
	  widthGraphTOF.at(iSubSpecies)->SetPoint(widthGraphTOF.at(iSubSpecies)->GetN(),
						 mTm0,tofFit.GetParameter(2));
	  widthGraphTOF.at(iSubSpecies)->SetPointError(widthGraphTOF.at(iSubSpecies)->GetN()-1,
						      mTm0BinWidth/2.0,tofFit.GetParError(2));

	  if (draw){
	    canvas->cd(4);
	    meanGraphTOF.at(iSubSpecies)->Draw("PZ");
	    gPad->Update();
	    canvas->cd(5);
	    widthGraphTOF.at(iSubSpecies)->Draw("PZ");
	    gPad->Update();
	    canvas->cd(6);
	    zTOFHistos.at(mTm0Index)->Draw("E");
	    tofFit.Draw("SAME");	    
	    gPad->Update();
	    gSystem->Sleep(500);
	  }
	  
	}//End Loop Over Sub Species
	
      }//End Loop Over mTm0Index

      //Do Some outlier rejection - Reject all width points that are greater than .085
      //and less than .065 for TPC
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	for (int iPoint=widthGraphTPC.at(iSubSpecies)->GetN()-1; iPoint>=0; iPoint--){
	  if (widthGraphTPC.at(iSubSpecies)->GetY()[iPoint] > .085 ||
	      widthGraphTPC.at(iSubSpecies)->GetY()[iPoint] < .065)
	    widthGraphTPC.at(iSubSpecies)->RemovePoint(iPoint);
	}
      }
      
      //Compute the moving averages of the means and widths and their confidence intervals
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){

	meanGraphTPCFit.at(iSubSpecies) = MovingAverageGraphFB(meanGraphTPC.at(iSubSpecies),.075);
	widthGraphTPCFit.at(iSubSpecies) = MovingAverageGraphFB(widthGraphTPC.at(iSubSpecies),.075);

	meanGraphTOFFit.at(iSubSpecies) = MovingAverageGraphFB(meanGraphTOF.at(iSubSpecies),.05);
	widthGraphTOFFit.at(iSubSpecies) = MovingAverageGraphFB(widthGraphTOF.at(iSubSpecies),.05);

	meanGraphTPCConf.at(iSubSpecies) = GetMovingAverageErrorBand(meanGraphTPCFit.at(iSubSpecies));
	widthGraphTPCConf.at(iSubSpecies) = GetMovingAverageErrorBand(widthGraphTPCFit.at(iSubSpecies));

	meanGraphTOFConf.at(iSubSpecies) = GetMovingAverageErrorBand(meanGraphTOFFit.at(iSubSpecies));
	widthGraphTOFConf.at(iSubSpecies) = GetMovingAverageErrorBand(widthGraphTOFFit.at(iSubSpecies));

	//Fits
	meanGraphTPCFit.at(iSubSpecies)->SetName(TString(meanGraphTPC.at(iSubSpecies)->GetName())+"_Fit");
	widthGraphTPCFit.at(iSubSpecies)->SetName(TString(widthGraphTPC.at(iSubSpecies)->GetName())+"_Fit");
	meanGraphTOFFit.at(iSubSpecies)->SetName(TString(meanGraphTOF.at(iSubSpecies)->GetName())+"_Fit");
	widthGraphTOFFit.at(iSubSpecies)->SetName(TString(widthGraphTOF.at(iSubSpecies)->GetName())+"_Fit");
	
	//Confs
	meanGraphTPCConf.at(iSubSpecies)->SetName(TString(meanGraphTPC.at(iSubSpecies)->GetName())+"_Conf");
	widthGraphTPCConf.at(iSubSpecies)->SetName(TString(widthGraphTPC.at(iSubSpecies)->GetName())+"_Conf");
	meanGraphTOFConf.at(iSubSpecies)->SetName(TString(meanGraphTOF.at(iSubSpecies)->GetName())+"_Conf");
	widthGraphTOFConf.at(iSubSpecies)->SetName(TString(widthGraphTOF.at(iSubSpecies)->GetName())+"_Conf");

      }



      //Save Everything
      pidFile->cd();
      pidFile->cd("MeanAndWidthGraphs");
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	if (meanGraphTPC.at(iSubSpecies)->GetN() > 5){
	  meanGraphTPC.at(iSubSpecies)->Write();
	  widthGraphTPC.at(iSubSpecies)->Write();
	}
	if (meanGraphTOF.at(iSubSpecies)->GetN() > 5){
	  meanGraphTOF.at(iSubSpecies)->Write();
	  widthGraphTOF.at(iSubSpecies)->Write();
	}
      }

      pidFile->cd();
      pidFile->cd("MeanAndWidthFits");
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	if (meanGraphTPCFit.at(iSubSpecies)->GetN() > 5){
	  meanGraphTPCFit.at(iSubSpecies)->Write();
	  widthGraphTPCFit.at(iSubSpecies)->Write();
	  meanGraphTPCConf.at(iSubSpecies)->Write();
	  widthGraphTPCConf.at(iSubSpecies)->Write();
	}
	if (meanGraphTOFFit.at(iSubSpecies)->GetN() > 5){
	  meanGraphTOFFit.at(iSubSpecies)->Write();
	  widthGraphTOFFit.at(iSubSpecies)->Write();
	  
	  meanGraphTOFConf.at(iSubSpecies)->Write();
	  widthGraphTOFConf.at(iSubSpecies)->Write();
	}
      }
      
      //Clean Up
      for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	if (zTPCHistos.at(mTm0Index)){
	  delete zTPCHistos.at(mTm0Index);
	  zTPCHistos.at(mTm0Index) = NULL;
	}
	if (zTOFHistos.at(mTm0Index)){
	  delete zTOFHistos.at(mTm0Index);
	  zTOFHistos.at(mTm0Index) = NULL;
	}

	for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	  if (tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index)){
	    delete tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index);
	    tofOptZTPCHistos.at(iSubSpecies).at(mTm0Index) = NULL;
	  }	  
	}
      }
      for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
	if (meanGraphTPC.at(iSubSpecies)){
	  delete meanGraphTPC.at(iSubSpecies);
	  meanGraphTPC.at(iSubSpecies) = NULL;
	  delete meanGraphTPCFit.at(iSubSpecies);
	  meanGraphTPCFit.at(iSubSpecies) = NULL;
	  delete meanGraphTPCConf.at(iSubSpecies);
	  meanGraphTPCConf.at(iSubSpecies) = NULL;
	}
	if (widthGraphTPC.at(iSubSpecies)){
	  delete widthGraphTPC.at(iSubSpecies);
	  widthGraphTPC.at(iSubSpecies) = NULL;
	  delete widthGraphTPCFit.at(iSubSpecies);
	  widthGraphTPCFit.at(iSubSpecies) = NULL;
	  delete widthGraphTPCConf.at(iSubSpecies);
	  widthGraphTPCConf.at(iSubSpecies) = NULL;
	}
	if (meanGraphTOF.at(iSubSpecies)){
	  delete meanGraphTOF.at(iSubSpecies);
	  meanGraphTOF.at(iSubSpecies) = NULL;
	  delete meanGraphTOFFit.at(iSubSpecies);
	  meanGraphTOFFit.at(iSubSpecies) = NULL;
	  delete meanGraphTOFConf.at(iSubSpecies);
	  meanGraphTOFConf.at(iSubSpecies) = NULL;
	}
	
	if (widthGraphTOF.at(iSubSpecies)){
	  delete widthGraphTOF.at(iSubSpecies);
	  widthGraphTOF.at(iSubSpecies) = NULL;
	  delete widthGraphTOFFit.at(iSubSpecies);
	  widthGraphTOFFit.at(iSubSpecies) = NULL;
	  delete widthGraphTOFConf.at(iSubSpecies);
	  widthGraphTOFConf.at(iSubSpecies) = NULL;
	}
      }
      //End Clean Up
      

    }//End Loop Over yIndex

    //More Clean Up
    if (zTPCHisto3D){
      delete zTPCHisto3D;
      zTPCHisto3D = NULL;
    }
    if (zTOFHisto3D){
      delete zTOFHisto3D;
      zTOFHisto3D = NULL;
    }
    for (unsigned int iTofOptIndex=0; iTofOptIndex<nParticles; iTofOptIndex++){
      if (tofOptZTPCHisto3D.at(iTofOptIndex)){
	delete tofOptZTPCHisto3D.at(iTofOptIndex);
	tofOptZTPCHisto3D.at(iTofOptIndex) = NULL;
      }
    }
    //End More Clean Up

  }//End loop over iParticle (species of interest)
	

}//End Main
			   

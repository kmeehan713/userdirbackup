//Use a blast wave model to simultaneously fit the pi/k/p spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TMarker.h>
#include <Fit/Fitter.h>
#include <Fit/BinData.h>
#include <Fit/Chi2FCN.h>
#include <Math/WrappedMultiTF1.h>
#include <HFitInterface.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

bool draw = false;

//__Global Chi2 Structure_____________________________________________________________
struct GlobalChi2 {

  //Data Members
  const ROOT::Math::IMultiGenFunction *pionPlusF;
  const ROOT::Math::IMultiGenFunction *pionMinusF;
  const ROOT::Math::IMultiGenFunction *kaonPlusF;
  const ROOT::Math::IMultiGenFunction *kaonMinusF;
  const ROOT::Math::IMultiGenFunction *protonPlusF;
  const ROOT::Math::IMultiGenFunction *protonMinusF;

  //Constructor
  GlobalChi2(ROOT::Math::IMultiGenFunction *pionPlusFunc, ROOT::Math::IMultiGenFunction *pionMinusFunc,
	     ROOT::Math::IMultiGenFunction *kaonPlusFunc, ROOT::Math::IMultiGenFunction *kaonMinusFunc,
	     ROOT::Math::IMultiGenFunction *protonPlusFunc, ROOT::Math::IMultiGenFunction *protonMinusFunc){
    pionPlusF    = pionPlusFunc;
    pionMinusF   = pionMinusFunc;
    kaonPlusF    = kaonPlusFunc;
    kaonMinusF   = kaonMinusFunc;
    protonPlusF  = protonPlusFunc;
    protonMinusF = protonMinusFunc;    
  }

  //Set the parameters
  double operator() (const double *par) const {

    //The parameter array (par) should be 13 elements long    
    double PionPlusNorm    = par[0];
    double PionMinusNorm   = par[1];
    double KaonPlusNorm    = par[2];
    double KaonMinusNorm   = par[3];
    double ProtonPlusNorm  = par[4];
    double ProtonMinusNorm = par[5];
    double T_Kin           = par[6];
    double Beta_S          = par[7];
    double n               = par[8];
    double R               = par[9];
    double pionMass        = par[10];
    double kaonMass        = par[11];
    double protonMass      = par[12];
    
    double pionPlusPar[6]   = {PionPlusNorm,T_Kin,Beta_S,n,R,pionMass};
    double pionMinusPar[6]  = {PionMinusNorm,T_Kin,Beta_S,n,R,pionMass};
    double kaonPlusPar[6]   = {KaonPlusNorm,T_Kin,Beta_S,n,R,kaonMass};
    double kaonMinusPar[6]  = {KaonMinusNorm,T_Kin,Beta_S,n,R,kaonMass};
    double protonPlusPar[6] = {ProtonPlusNorm,T_Kin,Beta_S,n,R,protonMass};
    double protonMinusPar[6] ={ProtonMinusNorm,T_Kin,Beta_S,n,R,protonMass};
    
    return (*pionPlusF)(pionPlusPar) + (*pionMinusF)(pionMinusPar)
      + (*kaonPlusF)(kaonPlusPar) + (*kaonMinusF)(kaonMinusPar)
      + (*protonPlusF)(protonPlusPar) + (*protonMinusF)(protonMinusPar);
  }
  
  
};

//___MAIN_____________________________________________________________________________
void simultaneousBlastWaveFit(TString spectraFileName, TString resultFileName,
			      Int_t userCentBin = -999, Double_t userRapidity = -999){
  
 TVirtualFitter::SetMaxIterations(10000);
  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();

  //Get the User's rapidity bin
  Int_t userRapidityBin = GetRapidityIndex(userRapidity);
  if (userRapidity != -999 && userRapidityBin < 0){
    cout <<"ERROR: SimultaneousBlastWaveFit - Ivalid value of rapidity. EXITING!\n";
    exit (EXIT_FAILURE);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Create the Ouput Results File
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir("BlastWaveFits");

  //Create a canvas for drawing if needed
  TCanvas *canvas = NULL;
  TH1F *frame = NULL;
  TLegend *leg = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetLogy();
    canvas->SetRightMargin(.05);
    canvas->SetTopMargin(.05);
    canvas->SetLeftMargin(.12);
    canvas->SetTicks();

    //Create the Frame
    frame = canvas->DrawFrame(0,.000001,1.6,10000);

    //Create the Spectra Legend
    TMarker *piPlusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(PION,1));
    TMarker *piMinusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(PION,-1));
    TMarker *kPlusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(KAON,1));
    TMarker *kMinusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(KAON,-1));
    TMarker *pPlusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(PROTON,1));
    TMarker *pMinusMarker = new TMarker(0,0,particleInfo->GetParticleMarker(PROTON,-1));

    piPlusMarker->SetMarkerColor(particleInfo->GetParticleColor(PION));
    piMinusMarker->SetMarkerColor(particleInfo->GetParticleColor(PION));
    kPlusMarker->SetMarkerColor(particleInfo->GetParticleColor(KAON));
    kMinusMarker->SetMarkerColor(particleInfo->GetParticleColor(KAON));
    pPlusMarker->SetMarkerColor(particleInfo->GetParticleColor(PROTON));
    pMinusMarker->SetMarkerColor(particleInfo->GetParticleColor(PROTON));

    piPlusMarker->SetMarkerSize(1.5);
    piMinusMarker->SetMarkerSize(1.5);
    kPlusMarker->SetMarkerSize(1.8);
    kMinusMarker->SetMarkerSize(1.8);
    pPlusMarker->SetMarkerSize(1.5);
    pMinusMarker->SetMarkerSize(1.5);
				 

    TLegend *leg = new TLegend(.75,.63,.92,.80);
    leg->SetFillColor(kWhite);
    leg->SetBorderSize(0);
    leg->SetTextFont(63);
    leg->SetTextSize(25);
    leg->SetNColumns(2);
    leg->AddEntry(piMinusMarker,"#pi^{-}","P");
    leg->AddEntry(piPlusMarker,"#pi^{+}","P");
    leg->AddEntry(kMinusMarker,"K^{-}","P");
    leg->AddEntry(kPlusMarker,"K^{+}","P");
    leg->AddEntry(pMinusMarker,"#bar{p}","P");
    leg->AddEntry(pPlusMarker,"p","P");
    leg->Draw("SAME");
    
    dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,650,800,600);
  }

  //*********************************************************************************
  //**** Get All of the spectra from the file
  //*********************************************************************************
  const int nCharges(2);
  const int nSpecies(3);
  std::vector< std::vector < std::vector < std::vector < TGraphErrors *> > > >
    tpcSpectra(nCharges, std::vector< std::vector < std::vector <TGraphErrors *> > >
	       (nSpecies, std::vector< std::vector <TGraphErrors *> >
		(nCentBins, std::vector <TGraphErrors *>
		 (nRapidityBins, (TGraphErrors *)NULL))));
  std::vector< std::vector < std::vector < std::vector < TGraphErrors *> > > >
    tofSpectra(nCharges, std::vector< std::vector < std::vector <TGraphErrors *> > >
	       (nSpecies, std::vector< std::vector <TGraphErrors *> >
		(nCentBins, std::vector <TGraphErrors *>
		 (nRapidityBins, (TGraphErrors *)NULL))));
  std::vector< std::vector < std::vector < std::vector < TMultiGraph *> > > >
    spectra(nCharges, std::vector< std::vector < std::vector <TMultiGraph *> > >
	       (nSpecies, std::vector< std::vector <TMultiGraph *> >
		(nCentBins, std::vector <TMultiGraph *>
		 (nRapidityBins, (TMultiGraph *)NULL))));

  for (int iCharge=0; iCharge<nCharges; iCharge++){
    for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
      for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){

	//If the user has specified a valid centrality bin,
	//then skip all others
	if (userCentBin != -999 && userCentBin >= 0 &&
	    userCentBin < nCentBins && iCentBin != userCentBin)
	  continue;
	
	for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

	  //Skip Rapidity Bins not of interest
	  //We need all of the particles for the simulaneous fit so we can't do a rapidity bin
	  //beyond where the kaons exist
	  if (yIndex < GetMinRapidityIndexOfInterest(KAON) || yIndex > GetMaxRapidityIndexOfInterest(KAON))
	    continue;

	  //If the user has specified a valid rapidity value,
	  //then skip all other rapidity bins
	  if (userRapidityBin >=0 && userRapidityBin != yIndex)
	    continue;

	  //Compute the Rapidity
	  Double_t rapidity = GetRapidityRangeCenter(yIndex);
	  int pid = iSpecies;
	  int charge = iCharge == 0 ? -1 : 1;

	  //Get the Spectra
	  spectraFile->cd();
	  tpcSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex) =
	    (TGraphErrors *)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectra_%s_Cent%02d_yIndex%02d",
						 particleInfo->GetParticleName(pid,charge).Data(),
						 particleInfo->GetParticleName(pid,charge).Data(),
						 iCentBin,yIndex));
	  tofSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex) =
	    (TGraphErrors *)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
						 particleInfo->GetParticleName(pid,charge).Data(),
						 particleInfo->GetParticleName(pid,charge).Data(),
						 iCentBin,yIndex));

	  //Combine the tpc and tof spectra into a single multi graph spectra
	  spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex) = new TMultiGraph();

	  //Check to make the tpc spectrum exists
	  if (tpcSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)){
	    spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)->
	      Add(tpcSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex));
	  }
	  else{
	    delete spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex);
	    spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex) = NULL;
	    continue;
	  }

	  //Check to make sure the tof spectrum exists
	  if (tofSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)){
	    spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)->
	      Add(tofSpectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex));	    
	  }

	  TList *list = spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)->GetListOfGraphs();
	  for (int i=0; i<list->GetEntries(); i++){
	    ((TGraphErrors *)(list->At(i)))->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
	    ((TGraphErrors *)(list->At(i)))->SetMarkerSize(pid==KAON ? 1.8 : 1.3);
	  }
	  
	}//End Loop Over Rapidity Bins
      }//End Loop Over cent bins
    }//End Loop Over Species
  }//End Loop Over Charge

  //*********************************************************************************
  //**** Construct six fits - one for each particle species and charge combo
  //*********************************************************************************
  std::vector <TF1 *> spectraFit(6,(TF1 *)NULL);
  std::vector <ROOT::Math::WrappedMultiTF1 *> wrappedFit(6,(ROOT::Math::WrappedMultiTF1*)NULL);
  std::vector <ROOT::Fit::DataOptions *>      dataOption(6, (ROOT::Fit::DataOptions*)NULL);
  std::vector <ROOT::Fit::DataRange *>        dataRange(6, (ROOT::Fit::DataRange*)NULL);
  
  for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
    spectraFit.at(iFit) = new TF1("",BlastWaveModelFit,0.001,2,6);
    spectraFit.at(iFit)->SetLineWidth(2);
    spectraFit.at(iFit)->SetLineColor(particleInfo->GetParticleColor((int)iFit/2));
    if ((int)iFit%2==0)
      spectraFit.at(iFit)->SetLineStyle(7);
    wrappedFit.at(iFit) = new ROOT::Math::WrappedMultiTF1(*spectraFit.at(iFit),1);
    dataOption.at(iFit) = new ROOT::Fit::DataOptions();
    dataOption.at(iFit)->fUseRange = true;
    dataRange.at(iFit)  = new ROOT::Fit::DataRange();
  }

  
  //*********************************************************************************
  //**** Loop Over the centrality and rapidity bins to perform the fits
  //*********************************************************************************
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){

    //If the user has specified a valid centrality bin,
    //then skip all others
    if (userCentBin != -999 && userCentBin >= 0 &&
	userCentBin < nCentBins && iCentBin != userCentBin)
      continue;
    
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Skip Rapidity Bins not of interest
      //We need all of the particles for the simulaneous fit so we can't do a rapidity bin
      //beyond where the kaons exist
      if (yIndex < GetMinRapidityIndexOfInterest(KAON) || yIndex > GetMaxRapidityIndexOfInterest(KAON))
	continue;
      
      //If the user has specified a valid rapidity value,
      //then skip all other rapidity bins
      if (userRapidityBin >=0 && userRapidityBin != yIndex)
	continue;
      
      if (draw){
	canvas->cd();
	for (int iCharge=0; iCharge<nCharges; iCharge++){
	  for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
	    spectra.at(iCharge).at(iSpecies).at(iCentBin).at(yIndex)->Draw("PZ");
	  }
	}

	TPaveText *title = new TPaveText(.45,.80,.92,.92,"NDC");
	title->SetFillColor(kWhite);
	title->SetBorderSize(0);
	title->SetTextFont(63);
	title->SetTextAlign(32);
	title->AddText(Form("Au+Au at #sqrt{s_{NN}} = %.03g GeV",GetCollisionEnergy()));
	title->GetLine(0)->SetTextSize(30);
	title->AddText(Form("Centrality=[%d,%d]%% | y=[%.03g,%.03g]",
			    iCentBin!=nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			    (int)GetCentralityPercents().at(iCentBin),
			    GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex)));
	title->GetLine(1)->SetTextSize(23);
	title->Draw("SAME");

	frame->GetXaxis()->SetTitle("m_{T}-m_{0} (GeV/c^{2})");
	frame->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{2}");
	frame->GetXaxis()->SetTitleFont(63);
	frame->GetYaxis()->SetTitleFont(63);
	frame->GetXaxis()->SetTitleSize(25);
	frame->GetYaxis()->SetTitleSize(25);
	frame->GetXaxis()->SetLabelFont(63);
	frame->GetYaxis()->SetLabelFont(63);
	frame->GetXaxis()->SetLabelSize(20);
	frame->GetYaxis()->SetLabelSize(20);

	frame->GetYaxis()->SetTitleOffset(1.23);
	
	canvas->Update();
      }

      //------
      //Set Up the Global Fit
      dataRange.at(0)->SetRange(0.,2); //Pion Plus
      dataRange.at(1)->SetRange(0.,2); //Pion Minus
      dataRange.at(2)->SetRange(0,2);  //Kaon Plus
      dataRange.at(3)->SetRange(0,2);  //Kaon Minus
      dataRange.at(4)->SetRange(0,2);  //Proton Plus
      dataRange.at(5)->SetRange(0,2);  //Proton Minus
      
      ROOT::Fit::BinData pionPlusData(*dataOption.at(0),*dataRange.at(0));
      ROOT::Fit::BinData pionMinusData(*dataOption.at(1),*dataRange.at(1));
      ROOT::Fit::BinData kaonPlusData(*dataOption.at(2),*dataRange.at(2));
      ROOT::Fit::BinData kaonMinusData(*dataOption.at(3),*dataRange.at(3));
      ROOT::Fit::BinData protonPlusData(*dataOption.at(4),*dataRange.at(4));
      ROOT::Fit::BinData protonMinusData(*dataOption.at(5),*dataRange.at(5));

      ROOT::Fit::FillData(pionPlusData,    spectra.at(1).at(PION).at(iCentBin).at(yIndex));
      ROOT::Fit::FillData(pionMinusData,   spectra.at(0).at(PION).at(iCentBin).at(yIndex));
      ROOT::Fit::FillData(kaonPlusData,    spectra.at(1).at(KAON).at(iCentBin).at(yIndex));
      ROOT::Fit::FillData(kaonMinusData,   spectra.at(0).at(KAON).at(iCentBin).at(yIndex));
      ROOT::Fit::FillData(protonPlusData,  spectra.at(1).at(PROTON).at(iCentBin).at(yIndex));
      ROOT::Fit::FillData(protonMinusData, spectra.at(0).at(PROTON).at(iCentBin).at(yIndex));

      ROOT::Fit::Chi2Function pionPlusChi2(pionPlusData,*wrappedFit.at(0));
      ROOT::Fit::Chi2Function pionMinusChi2(pionMinusData,*wrappedFit.at(1));
      ROOT::Fit::Chi2Function kaonPlusChi2(kaonPlusData,*wrappedFit.at(2));
      ROOT::Fit::Chi2Function kaonMinusChi2(kaonMinusData,*wrappedFit.at(3));
      ROOT::Fit::Chi2Function protonPlusChi2(protonPlusData,*wrappedFit.at(4));
      ROOT::Fit::Chi2Function protonMinusChi2(protonMinusData,*wrappedFit.at(5));

      GlobalChi2 globalChi2(&pionPlusChi2,&pionMinusChi2,
			    &kaonPlusChi2,&kaonMinusChi2,
			    &protonPlusChi2,&protonMinusChi2);

      //Parameter Initializations
      const int nPars(13);
      double fitPars[nPars] = {
	100000,100000,100000,100000,100000,100000,
	.1,.5,1.0,1.0,
	particleInfo->GetParticleMass(PION),
	particleInfo->GetParticleMass(KAON),
	particleInfo->GetParticleMass(PROTON)};

      //Simultaneous Fitter
      ROOT::Fit::Fitter fitter;
      fitter.Config().SetParamsSettings(13,fitPars);

      //Fix These Parameters
      fitter.Config().ParSettings(8).Fix();  //n
      fitter.Config().ParSettings(9).Fix();  //R
      fitter.Config().ParSettings(10).Fix(); //Pion Mass
      fitter.Config().ParSettings(11).Fix(); //Kaon Mass
      fitter.Config().ParSettings(12).Fix(); //Proton Mass

      //Set Limits On These Parameters
      //fitter.Config().ParSettings(0).SetLimits(10000,5000000); //Pion Plus Norm
      //fitter.Config().ParSettings(1).SetLimits(10000,5000000); //Pion Minus Norm
      //fitter.Config().ParSettings(2).SetLimits(10000,5000000); //Kaon Plus Norm
      //fitter.Config().ParSettings(3).SetLimits(10000,5000000); //Kaon Minus Norm
      //fitter.Config().ParSettings(4).SetLimits(10000,5000000); //Proton Plus Norm
      //fitter.Config().ParSettings(5).SetLimits(10000,5000000); //Proton Minus Norm
      fitter.Config().ParSettings(6).SetLimits(0.05,.5); //T_Kin
      fitter.Config().ParSettings(7).SetLimits(0.01,1.0); //Beta_S

      //Minimizer Options
      fitter.Config().MinimizerOptions().SetPrintLevel(10);
      fitter.Config().SetMinimizer("Minuit2","Migrad");

      //Do the Fit
      fitter.FitFCN(nPars,globalChi2,0,
		    pionPlusData.Size()+pionMinusData.Size()+kaonPlusData.Size()+kaonMinusData.Size()+protonPlusData.Size()+protonMinusData.Size(),true);
      ROOT::Fit::FitResult fitResult = fitter.Result();
      fitResult.Print(std::cout);

      //Transfer the Results to the Functions
      for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
	spectraFit.at(iFit)->SetParameter(0,fitResult.Parameter(iFit));
	spectraFit.at(iFit)->SetParError(0,fitResult.ParError(iFit));
	spectraFit.at(iFit)->SetParameter(1,fitResult.Parameter(6));
	spectraFit.at(iFit)->SetParError(1,fitResult.ParError(6));
	spectraFit.at(iFit)->SetParameter(2,fitResult.Parameter(7));
	spectraFit.at(iFit)->SetParError(2,fitResult.ParError(7));
	spectraFit.at(iFit)->SetParameter(3,fitResult.Parameter(8));
	spectraFit.at(iFit)->SetParError(3,fitResult.ParError(8));
	spectraFit.at(iFit)->SetParameter(4,fitResult.Parameter(9));
	spectraFit.at(iFit)->SetParError(4,fitResult.ParError(9));
      }
      spectraFit.at(0)->SetParameter(5,fitResult.Parameter(10));
      spectraFit.at(0)->SetParError(5,fitResult.ParError(10));
      spectraFit.at(1)->SetParameter(5,fitResult.Parameter(10));
      spectraFit.at(1)->SetParError(5,fitResult.ParError(10));
      spectraFit.at(2)->SetParameter(5,fitResult.Parameter(11));
      spectraFit.at(2)->SetParError(5,fitResult.ParError(11));
      spectraFit.at(3)->SetParameter(5,fitResult.Parameter(11));
      spectraFit.at(3)->SetParError(5,fitResult.ParError(11));
      spectraFit.at(4)->SetParameter(5,fitResult.Parameter(12));
      spectraFit.at(4)->SetParError(5,fitResult.ParError(12));
      spectraFit.at(5)->SetParameter(5,fitResult.Parameter(12));
      spectraFit.at(5)->SetParError(5,fitResult.ParError(12));
      
      //Compute the dN/dy Values
      std::vector<std::pair<double,double> > dNdy(6);
      for (unsigned int iFit=0; iFit<dNdy.size(); iFit++){
	Double_t integral = InvertBlastWaveFit(spectraFit.at(iFit)->GetParameter(2),
					       spectraFit.at(iFit)->GetParameter(3),
					       spectraFit.at(iFit)->GetParameter(1),
					       spectraFit.at(iFit)->GetParameter(5),
					       spectraFit.at(iFit)->GetParameter(4));
	dNdy.at(iFit).first = integral * spectraFit.at(iFit)->GetParameter(0);
	dNdy.at(iFit).second = integral * spectraFit.at(iFit)->GetParError(0);
      }

      //Compute the Mean Beta
      std::pair<double,double> meanBeta;
      meanBeta.first = (2.0*spectraFit.at(0)->GetParameter(2)) /
	(spectraFit.at(0)->GetParameter(3)+2.0);
      meanBeta.second = (2.0 *spectraFit.at(0)->GetParError(2)) /
	(spectraFit.at(0)->GetParameter(3)+2.0);
      

      TString pidSymbol[6] = {particleInfo->GetParticleSymbol(PION,1),
			      particleInfo->GetParticleSymbol(PION,-1),
			      particleInfo->GetParticleSymbol(KAON,1),
			      particleInfo->GetParticleSymbol(KAON,-1),
			      particleInfo->GetParticleSymbol(PROTON,1),
			      particleInfo->GetParticleSymbol(PROTON,-1)};

      cout <<Form("SPECIES\tdN/dy\tdNdyErr\tTkin\tTErr\tbS\tbSErr\tn") <<endl;
      for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
	cout <<Form("%s\t%.03g\t%.03g\t%.03g\t%.03g\t%.03g\t%.03g\t%.03g",
		    pidSymbol[iFit].Data(),dNdy.at(iFit).first,dNdy.at(iFit).second,
		    spectraFit.at(iFit)->GetParameter(1)*1000,spectraFit.at(iFit)->GetParError(1)*1000,
		    spectraFit.at(iFit)->GetParameter(2),spectraFit.at(iFit)->GetParError(2),
		    spectraFit.at(iFit)->GetParameter(3)) <<endl;
      }

      TPaveText *pidCol = new TPaveText(.154,.134,.204,.48,"NDC");
      pidCol->SetFillColor(kWhite);
      pidCol->SetBorderSize(0);
      pidCol->SetTextAlign(12);
      pidCol->SetTextFont(43);
      pidCol->SetTextSize(20);
      pidCol->AddText(Form("#color[%d]{dN/dy}",kWhite));
      pidCol->GetLine(0)->SetTextSize(23);
      pidCol->GetLine(0)->SetTextFont(63);
      for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
	pidCol->AddText(Form("%s",pidSymbol[iFit].Data()));
      }
      pidCol->Draw("SAME");
      	    
      TPaveText *dNdyVals = new TPaveText(.181,.134,.382,.48,"NDC");
      dNdyVals->SetFillColor(kWhite);
      dNdyVals->SetBorderSize(0);
      dNdyVals->SetTextFont(43);
      dNdyVals->SetTextSize(20);
      dNdyVals->SetTextAlign(12);
      dNdyVals->AddText(Form("dN/dy"));
      dNdyVals->GetLine(0)->SetTextSize(23);
      dNdyVals->GetLine(0)->SetTextFont(63);
      for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
	dNdyVals->AddText(Form("%.02f #pm %.02f",
			       dNdy.at(iFit).first,dNdy.at(iFit).second));
      }
      dNdyVals->Draw("SAME");


      TPaveText *fitResultPars = new TPaveText(.34,.134,.66,.378,"NDC");
      fitResultPars->SetFillColor(kWhite);
      fitResultPars->SetBorderSize(0);
      fitResultPars->SetTextFont(43);
      fitResultPars->SetTextSize(20);
      fitResultPars->SetTextAlign(12);
      fitResultPars->AddText("Blast Wave Fit");
      fitResultPars->GetLine(0)->SetTextFont(63);
      fitResultPars->GetLine(0)->SetTextSize(23);
      fitResultPars->AddText(Form("n = %.02g, R = %.02g (fixed)",
				  spectraFit.at(0)->GetParameter(3),
				  spectraFit.at(0)->GetParameter(4)));
      fitResultPars->AddText(Form("T_{kin} = %.01f #pm %.01f",
				  spectraFit.at(0)->GetParameter(1)*1000,
				  spectraFit.at(0)->GetParError(1)*1000));
      fitResultPars->AddText(Form("#beta_{S} = %.03f #pm %.03f",
				  spectraFit.at(0)->GetParameter(2),
				  spectraFit.at(0)->GetParError(2)));
      fitResultPars->AddText(Form("<#beta_{T}> = %.03f #pm %.03f",
				  meanBeta.first,meanBeta.second));
      fitResultPars->Draw("SAME");

      TPaveText *chi2Text = new TPaveText(.59,.134,.81,.234,"NDC");
      chi2Text->SetFillColor(kWhite);
      chi2Text->SetBorderSize(0);
      chi2Text->SetTextSize(20);
      chi2Text->SetTextFont(43);
      chi2Text->SetTextAlign(12);
      chi2Text->AddText("#chi^{2}/NDF");
      chi2Text->GetLine(0)->SetTextFont(63);
      chi2Text->GetLine(0)->SetTextSize(23);
      chi2Text->AddText(Form("%.04g/%d = %.03g",
			     fitResult.Chi2(), fitResult.Ndf(),
			     fitResult.Chi2()/(double)fitResult.Ndf()));
      chi2Text->Draw("SAME");
      
      /*
      //Set up the Fit
      TF1 *blastWaveFit = new TF1("",BlastWaveModelFit,0.001,2,6);
      blastWaveFit->SetParameter(0,10000);  //Normalization
      blastWaveFit->SetParameter(1,.1);    //Temp
      blastWaveFit->SetParameter(2,0.5);   //Surface Velocity
      blastWaveFit->SetParameter(3,2);     //Power in Transverse Velocity
      blastWaveFit->FixParameter(4,1);     //Upper Limit of Radial Integral
      blastWaveFit->FixParameter(5,particleInfo->GetParticleMass(PROTON)); //Mass (should be fixed)

      blastWaveFit->SetParLimits(0,10000,500000);
      blastWaveFit->SetParLimits(1,.05,.5);
      blastWaveFit->SetParLimits(2,0.0,1.0);
      blastWaveFit->SetParLimits(3,0,3);
      //blastWaveFit->SetParLimits(4,0,5);

      blastWaveFit->SetParNames("Norm","T_{kin}","#beta_{s}","n","R","mass");
      

      //Do the Fit
      spectra.at(0).at(PROTON).at(iCentBin).at(yIndex)->Fit(blastWaveFit,"REX0");

      //Compute the dNdy
      Double_t Integral = InvertBlastWaveFit(blastWaveFit->GetParameter(2),
			   blastWaveFit->GetParameter(3),
			   blastWaveFit->GetParameter(1),
			   blastWaveFit->GetParameter(5),
			   blastWaveFit->GetParameter(4));
      Double_t dNdy = blastWaveFit->GetParameter(0) * Integral;
	
      Double_t meanBeta = (2*blastWaveFit->GetParameter(2)) / (blastWaveFit->GetParameter(3)+2);
      
      cout <<"dNdy: " <<dNdy <<" +- " <<blastWaveFit->GetParError(0)*Integral <<endl;
      cout <<"<beta>: " <<meanBeta <<endl;
      */
      
      if (draw){

	for (unsigned int iFit=0; iFit<spectraFit.size(); iFit++){
	  spectraFit.at(iFit)->Draw("SAME");
	}
	
	canvas->Update();
	gSystem->Sleep(3000);
      }
      
    }//End Loop Over Rapidity Bins
  }//End Loop Over CentBins

}

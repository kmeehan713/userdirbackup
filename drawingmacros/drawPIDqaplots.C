#include "TLatex.h"

void drawPIDqaplots(TString starlibrary, TString qafile, TString tpcPlotName, TString tofPlotName=NULL){

  //---------------------------------------------------------------------------------------------
  //---------------------------------------------------------------------------------------------
  // Set default pretty draw parameters
	gStyle->SetPalette(1,0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetOptDate(0);
  gStyle->SetPadGridX(0);
  gStyle->SetPadGridY(0);

  const Int_t NRGBs = 5;
  const Int_t NCont = 255;

  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
  //---------------------------------------------------------------------------------------------
  //---------------------------------------------------------------------------------------------
	
	// Initializing variables
	TCanvas *cTPC, *cTOF;
	TH2D *h2dEdx, *h2invbeta;
	TLegend *legTPC, *legTOF;

  // Setting up particle info class
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  ParticleInfo *particleInfo = new ParticleInfo(starlibrary, true, MOSTPROBABLE);

  // Obtain the qa plot from the file
  TFile *fqa = new TFile(qafile,"R");
  if (tpcPlotName) h2dEdx = (TH2D*) fqa->Get(tpcPlotName);	
  if (tofPlotName) h2invbeta = (TH2D*) fqa->Get(tofPlotName);	

  if (h2dEdx)	{

		// Draw the qa plot
		cTPC = new TCanvas("cTPC","cTPC",600,600);
		cTPC->cd();
		gPad->SetLogz();
		h2dEdx->SetStats(0);
		h2dEdx->Draw("colz");
		h2dEdx->SetTitle("PID with the STAR TPC for #sqrt{s_{NN}} = 4.5 GeV Au + Au FXT Collisions");
		h2dEdx->GetXaxis()->SetTitle("p/|q| (GV/c)");
		h2dEdx->GetXaxis()->SetTitleSize(0.05);
		h2dEdx->GetXaxis()->SetTitleOffset(0.9);
		h2dEdx->GetYaxis()->SetTitle("dE/dx (keV/cm)");
		h2dEdx->GetYaxis()->SetTitleSize(0.05);
		h2dEdx->GetYaxis()->SetTitleOffset(0.9);

    // Setup legend
		legTPC = new TLegend(.54,.69,.86,.85);
		legTPC->SetBorderSize(0);
		legTPC->SetFillColor(kWhite);
		legTPC->SetTextSize(.030);
		legTPC->SetNColumns(2);

    // Draw the Bichsel Functions
    const int nParticles = particleInfo->GetNumberOfDefinedParticles();
    for (int iParticle=0; iParticle<nParticles; iParticle++){

			if (iParticle == MUON || iParticle == ALPHA || iParticle == HELION)
				continue;

			legTPC->AddEntry(particleInfo->GetBichselFunction(iParticle),
				Form("%s",particleInfo->GetParticleName(iParticle).Data()),"L");
			
			cTPC->cd();
			particleInfo->GetBichselFunction(iParticle)->SetNpx(100000);
			particleInfo->GetBichselFunction(iParticle)->SetLineWidth(3);
			particleInfo->GetBichselFunction(iParticle)->Draw("SAME");

     }//end loop over particles

		cTPC->cd();
		legTPC->Draw("SAME");

	}//end if TPC

	if (h2invbeta){

    // Draw the qa plot
    cTOF = new TCanvas("cTOF","cTOF",600,600);
    cTOF->cd();
    gPad->SetLogz();
    h2invbeta->SetStats(0);
		h2invbeta->GetXaxis()->SetRangeUser(0.1,3);
		h2invbeta->GetYaxis()->SetRangeUser(0.8,2);
    h2invbeta->Draw("colz");
    h2invbeta->GetXaxis()->SetTitle("p/|q| (GV/c)");
    h2invbeta->GetXaxis()->SetTitleSize(0.05);
    h2invbeta->GetXaxis()->SetTitleOffset(0.9);
    h2invbeta->GetYaxis()->SetTitle("1/#beta");
    h2invbeta->GetYaxis()->SetTitleSize(0.05);
    h2invbeta->GetYaxis()->SetTitleOffset(0.9);

    // Setup legend
		legTOF = new TLegend(.59,.78,.90,.89);
		legTOF->SetBorderSize(0);
		legTOF->SetFillColor(kWhite);
		legTOF->SetTextSize(.030);
		legTOF->SetNColumns(2);

    const int nParticles = particleInfo->GetNumberOfDefinedParticles();
		for (int iParticle=0; iParticle<nParticles; iParticle++){

			if (iParticle == MUON || iParticle == ALPHA || iParticle == HELION)
				continue;

      legTOF->AddEntry(particleInfo->GetBichselFunction(iParticle),
        Form("%s",particleInfo->GetParticleName(iParticle).Data()),"L");  

      cTOF->cd();
			particleInfo->GetInverseBetaFunction(iParticle)->SetLineWidth(3);
			particleInfo->GetInverseBetaFunction(iParticle)->Draw("SAME");
			
		}//End of loop over particles

		cTOF->cd();
		legTOF->Draw("SAME");

	} //end if TOF

}

#include <iostream>
#include <vector>
#include <utility>
#include <numeric>

#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TRandom3.h>
#include <TLatex.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"

using namespace std;

void prepareSpectra(TString spectraFileName, TString preparedSpectraFileName, Int_t pid, Int_t charge, Int_t yIndex){

  //Set Manual cutOffPoints
	Double_t cutOffPoints[21] = {999,999,999,999,999,999,999,999,999,999,1.37,1.25,1.22,1.17,1.15,1.13,1,0.87,0.8,999,999};

	ParticleInfo *particleInfo = new ParticleInfo();
  TGraphErrors *tpcSpectrum, *tofSpectrum, *preparedSpectrum;

	//Create output file
	TFile *preparedSpectraFile = new TFile(preparedSpectraFileName, "UPDATE");
  
  //Load spectra file
	TFile *spectraFile = new TFile(spectraFileName, "READ");

  //Loop over all rapidities
  Int_t yMin = GetMinRapidityIndexOfInterest(pid);
  if (pid==0 && charge==1) yMin = 4;
  Int_t yMax = GetMaxRapidityIndexOfInterest(pid);
  for (Int_t y=yMin; y<yMax; y++){
  
		if (yIndex >=0 && y != yIndex) continue;

    spectraFile->cd();
    tpcSpectrum = (TGraphErrors*)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),
          particleInfo->GetParticleName(pid,charge).Data(),0,y));

    if (!tpcSpectrum){
      cout<<"WARNING: TPC Spectrum for y "<<y<<" does not exist"<<endl;
      continue;
    }

    //Load momentum-dependent corrected tof spectra
    spectraFile->cd();
    tofSpectrum = (TGraphErrors*)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),
          particleInfo->GetParticleName(pid,charge).Data(),0,y));

    if (!tofSpectrum){
      cout<<"WARNING: TOF Spectrum for yIndex "<<y<<" does not exist"<<endl;
    }

    preparedSpectrum = new TGraphErrors();
    preparedSpectrum->SetName(Form("preparedSpectra_%s_Cent%02d_yIndex%02d",
          particleInfo->GetParticleName(pid,charge).Data(),0,y));

    //Loop to get tofOffset
    Int_t iPointTofOffset(0);
    Double_t mTm0TPC(0), mTm0TOF(0); 
    for (Int_t iPoint=0; iPoint<tpcSpectrum->GetN(); iPoint++){

      mTm0TPC  = tpcSpectrum->GetX()[iPoint];
      if (mTm0TPC < 0.3) continue;
			iPointTofOffset = iPoint;
      break;

 		}//End loop over tpc points

    //Loop to create combined spectra
    Int_t totalPoints = tpcSpectrum->GetN();
    if (tofSpectrum) totalPoints = iPointTofOffset+tofSpectrum->GetN(); 
		Int_t jPoint=0;
    for (Int_t iPoint=0; iPoint<totalPoints; iPoint++){

      //Get all TPC Points
      if (iPoint < tpcSpectrum->GetN()){
			  
        mTm0TPC  = tpcSpectrum->GetX()[iPoint];
				Double_t yTPC = tpcSpectrum->GetY()[iPoint];
				Double_t yError = tpcSpectrum->GetEY()[iPoint];
				cout<<"i: "<<iPoint<<" x: "<<mTm0TPC<<" y: "<<yTPC<<endl;
				cout<<"jPoint: "<<jPoint<<endl;
				cout<<"yError: "<<tpcSpectrum->GetEY()[iPoint]<<endl;
				if (yError > 10 || TMath::IsNaN(yError)) continue;
        preparedSpectrum->SetPoint(jPoint,tpcSpectrum->GetX()[iPoint],tpcSpectrum->GetY()[iPoint]);
        preparedSpectrum->SetPointError(jPoint,tpcSpectrum->GetEX()[iPoint],tpcSpectrum->GetEY()[iPoint]);
				++jPoint;
        continue;

      }

      //Check Indexing
      Int_t iPointTof = iPoint - iPointTofOffset;
      if (tofSpectrum && !tofSpectrum->GetY()[iPointTof]){
        cout<<"ERROR: Incorrect indexing when making combined tgraph !!!"<<endl;
      }
     
      //Get TOF Points using cutoff to account for TOF acceptance 
      cout<<"Rap. Index: "<<y<<" CutOff: "<<cutOffPoints[y]<<endl;
      if (iPoint >= tpcSpectrum->GetN()){
        mTm0TOF = tofSpectrum->GetX()[iPointTof];
				cout<<"mTm0TOF: "<<mTm0TOF<<" CutOff "<<cutOffPoints[y]<<endl;  
        if (mTm0TOF > cutOffPoints[y]) break;
        preparedSpectrum->SetPoint(jPoint,tofSpectrum->GetX()[iPointTof],tofSpectrum->GetY()[iPointTof]);
        preparedSpectrum->SetPointError(jPoint,tofSpectrum->GetEX()[iPointTof],tofSpectrum->GetEY()[iPointTof]);
				++jPoint;
      }

    }//End loop over total points

		preparedSpectrum->SetMarkerStyle(20);

		preparedSpectraFile->cd();
		preparedSpectrum->Write();

		preparedSpectrum->Delete();

	}//End loop over rapidity bins

}//End of fnxn

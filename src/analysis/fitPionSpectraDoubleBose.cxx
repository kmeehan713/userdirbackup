//Fit the Spectra

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include <TAxis.h>
#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TLatex.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

using namespace std;

Bool_t draw = false;

//________________________________________________________________________________________________________
void RemovePointsWithLargeErrors(TGraphErrors *spectrum, Double_t maxRelativeError=.1){

    //Loop Over the Points of the spectrum. Remove any point which is found
    //to have a relativeError larger than maxRelativeError
    for (int iPoint=spectrum->GetN()-1; iPoint>=0; iPoint--){
        if (spectrum->GetEY()[iPoint] / spectrum->GetY()[iPoint] > maxRelativeError)
              spectrum->RemovePoint(iPoint);
    }   
}

//________________________________________________________________________________________________________
void fitPionSpectraDoubleBose(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge, Double_t midY){

  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = 1;//GetNCentralityBins();
  Double_t mTm0min, mTm0max, dNdy, dNdyErr, dNdy1, dNdy2, dNdy1Err, dNdy2Err, dNdySystErr;
  Int_t nPoints;
  TGraphErrors *tpcCorrectedSpectrum, *tpcRawSpectrum, *tpcSysErrSpectrum;
  TF1 *fitBoseEinstein, *fitBoseEinsteinSystOnly, *tSlopeFit1, *tSlopeFit2, *tSlopeFitSystOnly, *fitDoubleExponential, *fitBEextra, *fitBEForIntegral;
	vector <TGraphErrors *> tSlopeGraph1(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> tSlopeGraph2(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> tSlopeGraphSystOnly(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph1(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph2(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph(nCentBins, (TGraphErrors *)NULL);

  //Create the OutputFile
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
		dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    gPad->DrawFrame(-2,0,2,150);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
   
	  //Create the temperature/inverse slope parameter graph and fit
	  tSlopeGraph1.at(iCentBin) = new TGraphErrors();
	  tSlopeGraphSystOnly.at(iCentBin) = new TGraphErrors();
	  tSlopeGraph1.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph1.at(iCentBin)->SetName(Form("Slope1Parameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));
	  tSlopeGraph2.at(iCentBin) = new TGraphErrors();
	  tSlopeGraph2.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph2.at(iCentBin)->SetName(Form("Slope2Parameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));

		//tSlopeFit = new TF1("tSlopeFit","pol2",-1.8,0.2);
		tSlopeFit1 = new TF1("tSlopeFit1","gaus(0)",-1.8,0.2);
		tSlopeFit1->SetNpx(10000);
		tSlopeFit1->SetName(Form("SlopeParameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit1->SetParameter(0,0.057);//0.22
		//tSlopeFit->FixParameter(0,0.158);//0.22
		tSlopeFit1->FixParameter(1,midY); //SetParameter(1,0.1);
		tSlopeFit1->SetParameter(2,3.1); //0.04 //0.033
		//tSlopeFit->FixParameter(2,1.691); //0.04 //0.033
		tSlopeFit1->SetParNames("Amp", "#mu", "#sigma");

		tSlopeFit2 = new TF1("tSlopeFit2","gaus(0)",-1.8,0.2);
		tSlopeFit2->SetNpx(10000);
		tSlopeFit2->SetName(Form("Slope2Parameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit2->SetParameter(0,0.139);//0.22
		//tSlopeFit2->SetParLimits(0,0.02,0.1);
		tSlopeFit2->FixParameter(1,midY); //SetParameter(1,0.1);
		tSlopeFit2->SetParameter(2,1.68); //0.04 //0.033
		tSlopeFit2->SetParLimits(2,0.0,3.0); //0.04 //0.033
		tSlopeFit2->SetParNames("Amp", "#mu", "#sigma");

		tSlopeFitSystOnly = new TF1("tSlopeFitSystOnly","gaus(0)",-1.8,0.2);
		tSlopeFitSystOnly->SetNpx(10000);
		tSlopeFitSystOnly->SetParameter(0,0.158);
		tSlopeFitSystOnly->FixParameter(1,midY); 
		tSlopeFitSystOnly->SetParameter(2,1.691); 

		//Create the dN/dy Graphs
		dNdyGraph.at(iCentBin) = new TGraphErrors();
		dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensityTotal_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		dNdyGraph1.at(iCentBin) = new TGraphErrors();
		dNdyGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph1.at(iCentBin)->SetName(Form("RapidityDensity1_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		dNdyGraph2.at(iCentBin) = new TGraphErrors();
		dNdyGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph2.at(iCentBin)->SetName(Form("RapidityDensity2_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));

		for(Int_t iRound = 0;iRound<2;iRound++){

			//for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++)
			for (int yIndex=2; yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

				cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
				particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

				//Compute Rapidity
				Double_t rapidity = GetRapidityRangeCenter(yIndex);

				//Get Spectra
				spectraFile->cd();
				tpcCorrectedSpectrum = (TGraphErrors *)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
				cout<<"tpcCorrectedSpectrum "<<tpcCorrectedSpectrum<<endl;
				if (!tpcCorrectedSpectrum) continue;
				tpcRawSpectrum = (TGraphErrors *)gDirectory->Get(Form("RawSpectra_%s/rawSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));

        RemovePointsWithLargeErrors(tpcCorrectedSpectrum,0.5);
				nPoints = tpcCorrectedSpectrum->GetN();
				if (nPoints <= 5) continue;
        mTm0min = 0.08;
				if (tpcCorrectedSpectrum->GetX()[nPoints-1] > 1.4) mTm0max = 1.4;
        else mTm0max = tpcCorrectedSpectrum->GetX()[nPoints-1];
        TGraphChop(tpcCorrectedSpectrum,mTm0min,true);
        TGraphChop(tpcCorrectedSpectrum,mTm0max,false);
        
				//Get the fit function
				if (yIndex < 17) fitBoseEinstein = new TF1("",DoubleBoseEinsteinFitFunc,mTm0min,1.4,6);
				else if (yIndex == 17) fitBoseEinstein = new TF1("",DoubleBoseEinsteinFitFunc,mTm0min,0.95,6);//0.95
				else if (yIndex == 18) fitBoseEinstein = new TF1("",DoubleBoseEinsteinFitFunc,mTm0min,0.82,6);//0.82
				fitBEextra = new TF1("",DoubleBoseEinsteinFitFunc,0,mTm0max,6);
				fitBEForIntegral = new TF1("",DoubleBoseEinsteinFitFunc,0,5,6);
				fitBoseEinsteinSystOnly = new TF1("",BoseEinsteinFitFunc,0.08,0.4,6);
				//BoseEinsteinFitFunc,mTm0min,mTm0max,4
				fitBoseEinstein->SetName(Form("spectrumFitBoseEinstein_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
				fitBoseEinstein->SetLineStyle(7);
				fitBEextra->SetName(Form("spectrumFitBEextra_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
				fitBEForIntegral->SetName(Form("spectrumFitBEForIntegral_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
			  			
				fitBoseEinstein->SetParameter(0,65);//65 40
				fitBoseEinstein->SetParameter(1,0.057);//0.16 0.05
				fitBoseEinstein->FixParameter(2,particleInfo->GetParticleMass(pid));
				fitBoseEinstein->FixParameter(3,rapidity+1.52);
				fitBoseEinstein->SetParameter(4,10);
				fitBoseEinstein->SetParameter(5,0.139);//0.16 0.15
				fitBoseEinstein->SetParLimits(0,1.,75.);
				fitBoseEinstein->SetParLimits(1,0.040,0.100);
				fitBoseEinstein->SetParLimits(4,1.,50.);
				fitBoseEinstein->SetParLimits(5,0.120,0.200);
				//fitBoseEinstein->SetParLimits(1,0.1,0.5);
				fitBoseEinstein->SetParNames("dN/dy_{1}","T_{InverseSlope,1}","#pi mass","y_{CM}","dN/dy_{2}","T_{InverseSlope,2}");
				fitBEextra->SetParNames("dN/dy_{1}","T_{InverseSlope,1}","#pi mass","y_{CM}","dN/dy_{2}","T_{InverseSlope,2}");

				if (iRound > 0){
						fitBoseEinstein->FixParameter(1,tSlopeFit1->Eval(rapidity));
						fitBoseEinstein->FixParameter(5,tSlopeFit2->Eval(rapidity));
						//fitBEextra->FixParameter(1,tSlopeFit1->Eval(rapidity));
						//fitBEextra->FixParameter(5,tSlopeFit2->Eval(rapidity));
						fitBoseEinsteinSystOnly->FixParameter(1,tSlopeFitSystOnly->Eval(rapidity));
						//fitDoubleExponential->FixParameter(3,tSlopeFit->Eval(rapidity));
				}

				if (iRound == 2){

					fitDoubleExponential->ReleaseParameter(2);
					fitDoubleExponential->SetParameter(2,0.060);

				}

				//Perform the fit
				tpcCorrectedSpectrum->Fit(fitBoseEinstein,"REXO");

				fitBEextra->FixParameter(0,fitBoseEinstein->GetParameter(0));
				fitBEextra->FixParameter(1,fitBoseEinstein->GetParameter(1));
				fitBEextra->FixParameter(2,fitBoseEinstein->GetParameter(2));
				fitBEextra->FixParameter(3,fitBoseEinstein->GetParameter(3));
				fitBEextra->FixParameter(4,fitBoseEinstein->GetParameter(4));
				fitBEextra->FixParameter(5,fitBoseEinstein->GetParameter(5));
				tpcCorrectedSpectrum->Fit(fitBEextra,"REXO+");//EXO+,"",0.08,1.4);
				if (yIndex==17) tpcCorrectedSpectrum->Fit(fitBEextra,"REXO+");//EXO+,"",0.08,0.95);
				if (yIndex==18) tpcCorrectedSpectrum->Fit(fitBEextra,"REXO+");//EXO+,"",0.08,0.82);
				fitBEForIntegral->FixParameter(0,fitBoseEinstein->GetParameter(0));
				fitBEForIntegral->FixParameter(1,fitBoseEinstein->GetParameter(1));
				fitBEForIntegral->FixParameter(2,fitBoseEinstein->GetParameter(2));
				fitBEForIntegral->FixParameter(3,fitBoseEinstein->GetParameter(3));
				fitBEForIntegral->FixParameter(4,fitBoseEinstein->GetParameter(4));
				fitBEForIntegral->FixParameter(5,fitBoseEinstein->GetParameter(5));
				tpcCorrectedSpectrum->Fit(fitBEForIntegral,"REXO+");

				if (iRound==0){
				 
				  cout<<"Rapidity: "<<rapidity<<" dN/dy1: "<<fitBoseEinstein->GetParameter(0)<<endl;
				  cout<<"Rapidity: "<<rapidity<<" dN/dy2: "<<fitBoseEinstein->GetParameter(4)<<endl;
	       	
					tSlopeGraph1.at(iCentBin)->SetPoint(tSlopeGraph1.at(iCentBin)->GetN(),
										rapidity, fitBoseEinstein->GetParameter(1));								
					tSlopeGraph1.at(iCentBin)->SetPointError(tSlopeGraph1.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitBoseEinstein->GetParError(1));								
					tSlopeGraphSystOnly.at(iCentBin)->SetPoint(tSlopeGraphSystOnly.at(iCentBin)->GetN(),
										rapidity, fitBoseEinsteinSystOnly->GetParameter(1));								
					tSlopeGraphSystOnly.at(iCentBin)->SetPointError(tSlopeGraphSystOnly.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitBoseEinsteinSystOnly->GetParError(1));								

					tSlopeGraph2.at(iCentBin)->SetPoint(tSlopeGraph2.at(iCentBin)->GetN(),
										rapidity, fitBoseEinstein->GetParameter(5));								
					tSlopeGraph2.at(iCentBin)->SetPointError(tSlopeGraph2.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitBoseEinstein->GetParError(5));								

				}

        if (iRound==1){

				  
					dNdy = fitBoseEinstein->GetParameter(0)+fitBoseEinstein->GetParameter(4);
					dNdy1 = fitBoseEinstein->GetParameter(0);
					dNdy2 = fitBoseEinstein->GetParameter(4);
					//Below line is prob wrong since error between dndy1 and dndy2 will prob be correlated
					dNdyErr = sqrt(fitBoseEinstein->GetParError(0)*fitBoseEinstein->GetParError(0) + fitBoseEinstein->GetParError(4)*fitBoseEinstein->GetParError(4));
					dNdy1Err = fitBoseEinstein->GetParError(0);
					dNdy2Err = fitBoseEinstein->GetParError(4);
					dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),rapidity-midY,dNdy);
					dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdyErr);
					dNdySystErr = fitBoseEinsteinSystOnly->GetParError(0);

					dNdyGraph1.at(iCentBin)->SetPoint(dNdyGraph1.at(iCentBin)->GetN(),rapidity-midY,dNdy1);
					dNdyGraph1.at(iCentBin)->SetPointError(dNdyGraph1.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdy1Err);

					dNdyGraph2.at(iCentBin)->SetPoint(dNdyGraph2.at(iCentBin)->GetN(),rapidity-midY,dNdy2);
					dNdyGraph2.at(iCentBin)->SetPointError(dNdyGraph2.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdy2Err);
        }

				if (draw){

					canvas->cd();
					canvas->DrawFrame(0,0.01*TMath::MinElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()),
								2.0,20*TMath::MaxElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()));
					canvas->SetLogy();
					tpcCorrectedSpectrum->Draw("APZ");	
					tpcCorrectedSpectrum->GetXaxis()->SetTitle("m_{T}-m_{0} (GeV)");
					
					fitBoseEinstein->SetLineColor(2);
					fitBoseEinstein->SetLineWidth(2);
					//fitBoseEinstein->SetLineStyle(7);
					fitBoseEinstein->Draw("SAME");
					fitBEextra->SetLineStyle(7);
					fitBEextra->SetLineColor(2);
					fitBEextra->SetLineWidth(2);
					fitBEextra->Draw("SAME");
				  /*	
					fitDoubleExponential->SetLineColor(2);
					fitDoubleExponential->SetLineWidth(2);
					fitDoubleExponential->SetLineStyle(7);
					fitDoubleExponential->Draw("SAME");
					*/

				}

				tpcCorrectedSpectrum->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{-2}");
				tpcCorrectedSpectrum->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				             particleInfo->GetParticleSymbol(pid).Data()));
				tpcCorrectedSpectrum->GetYaxis()->SetTitleFont(63);
				tpcCorrectedSpectrum->GetYaxis()->SetTitleSize(25);
        tpcCorrectedSpectrum->GetXaxis()->SetTitleFont(63);
				tpcCorrectedSpectrum->GetXaxis()->SetTitleSize(25);
				tpcCorrectedSpectrum->GetXaxis()->SetTitleOffset(1.3);
				TF1 *extrap = tpcCorrectedSpectrum->GetFunction(Form("spectrumFitBoseEinstein_%s_Cent%02d_yIndex%02d",
				            particleInfo->GetParticleName(pid,charge).Data(),
										iCentBin,yIndex));

				cout<<"Saving Spectra"<<endl;
				//Save Spectra
				if (iRound==1){
					resultsFile->cd();
					resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
					if (tpcCorrectedSpectrum) tpcCorrectedSpectrum->Write();
					resultsFile->cd();
					resultsFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
				  if (tpcCorrectedSpectrum)	tpcRawSpectrum->Write();
					resultsFile->cd();
				}

				cout<<"SAVED"<<endl;

			}//End of rapidity loop

		  if (iRound==0) {
				tSlopeGraph1.at(iCentBin)->Fit(tSlopeFit1,"EX0","",-1.8,-0.18);//-0.48
				tSlopeGraph2.at(iCentBin)->Fit(tSlopeFit2,"EX0","",-1.8,-0.18);
				tSlopeGraphSystOnly.at(iCentBin)->Fit(tSlopeFitSystOnly,"REX0");
			}

		}//End of loop over fitting rounds

		if (draw){
			dNdyCanvas->cd();
			dNdyGraph.at(iCentBin)->Draw("APZ");
		}

    //Save Plots
		tSlopeGraph1.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph1.at(iCentBin)->GetYaxis()->SetTitle("T/inverseSlope MeV");
		tSlopeGraph2.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph2.at(iCentBin)->GetYaxis()->SetTitle("T/inverseSlope MeV");


		resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
		if (tSlopeGraph1.at(iCentBin)->GetN() > 0){
			tSlopeGraph1.at(iCentBin)->Write();
			tSlopeGraphSystOnly.at(iCentBin)->Write();
		}
		if (tSlopeGraph2.at(iCentBin)->GetN() > 0){
			tSlopeGraph2.at(iCentBin)->Write();
		}

		resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0) dNdyGraph.at(iCentBin)->Write();
    if (dNdyGraph1.at(iCentBin)->GetN() > 0) dNdyGraph1.at(iCentBin)->Write();
    if (dNdyGraph2.at(iCentBin)->GetN() > 0) dNdyGraph2.at(iCentBin)->Write();
	

	}//End of centrality loop

}//End of function

//This code uses the histograms in the output file created by knockoutProtons.cxx
//to determine the correction factor curves for the knockout protons.

#include <iostream>
#include <vector>
#include <utility>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

TH1D *protonPlus, *protonMinus;

//____________________________________________________________________
Double_t AntiProtons(Double_t *x, Double_t *par){

  Double_t dca = x[0];
  Double_t r = par[0];
  
  return protonMinus->GetBinContent(protonMinus->FindBin(dca)) / r;
  
}

//_____________________________________________________________________
Double_t KnockoutFunc(Double_t *x, Double_t *par){

  Double_t dca = x[0];
  Double_t A   = par[0];
  Double_t d0  = par[1];
  Double_t a   = par[2];
  
  return A * TMath::Power(1.0 - TMath::Exp(-dca/d0),a);
}

//____________________________________________________________________
Double_t KnockoutProtonFit(Double_t *x, Double_t *par){

  //From PHYSICAL REVIEW C79, 034909 (2009)
  Double_t dca = x[0];
  Double_t r   = par[0]; //pBar to p ratio
  Double_t A   = par[1]; //Magnitude
  Double_t d0  = par[2]; //Slope
  Double_t a   = par[3]; //alpha exponent

  Double_t vals[1] = {dca};
  
  Double_t pars[1] = {r};
  Double_t antiProtons = AntiProtons(vals, pars);

  Double_t backgroundPars[3] = {A,d0,a};
  Double_t background = KnockoutFunc(vals, backgroundPars);

  return antiProtons + background;
}

//______________________________________________________________________
void knockoutProtonAnalysis(TString inputFile, TString outputFile){

  //Range of DCA over which the integral should be computed to determine
  //the knockout background fraction
  Double_t minRange(0), maxRange(1);
  
  //Create an instance of the Particle Info Class which will use the empirical fits
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion());

  //Get the Number of Centrality Bins
  const int nCentralityBins = GetNCentralityBins();
  
  //Open the inputFile and load the histograms
  TFile *inFile = new TFile(inputFile,"READ");
  std::vector< std::pair<TH3D *, TH3D *> > dcaDistribution3D(nCentralityBins);
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    dcaDistribution3D.at(iCentBin).first = (TH3D *)inFile->
      Get(Form("glDCA_ProtonPlus_CentBin%d",iCentBin));
    dcaDistribution3D.at(iCentBin).second = (TH3D *)inFile->
      Get(Form("glDCA_ProtonMinus_CentBin%d",iCentBin));

  }//End Loop Over iCentBin to Load Histograms

  //One Dimensional Histograms
  std::vector< std::vector < std::vector< std::pair<TH1D *,TH1D *> > > > dcaDistribution
    (nCentralityBins,std::vector< std::vector< std::pair<TH1D *, TH1D *> > >
     (nRapidityBins, std::vector< std::pair<TH1D *, TH1D *> >
      (nmTm0Bins)));

  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	double rapidity = GetRapidityRangeCenter(yIndex);
	double mTm0     = GetmTm0RangeCenter(mTm0Index);
	int yBin    = dcaDistribution3D.at(iCentBin).first->GetXaxis()->FindBin(rapidity);
	int mTm0Bin = dcaDistribution3D.at(iCentBin).first->GetYaxis()->FindBin(mTm0);
	
	dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).first =
	  (dcaDistribution3D.at(iCentBin).first->ProjectionZ(Form("%s_%d_%d",
								  dcaDistribution3D.at(iCentBin).first->GetName(),
								  yIndex,mTm0Index),yBin,yBin,mTm0Bin,mTm0Bin));

	dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second =
	  (dcaDistribution3D.at(iCentBin).second->ProjectionZ(Form("%s_%d_%d",
								   dcaDistribution3D.at(iCentBin).second->GetName(),
								   yIndex,mTm0Index),yBin,yBin,mTm0Bin,mTm0Bin));

      }//End Loop Over mTm0 Bins
    }//End Loop Over Rapidity Bins
  }//End Loop Over iCentBin

  //Background Graphs as a function of mTm0
  std::vector< std::vector<TGraphErrors *> > backgroundGraph(nCentralityBins,std::vector<TGraphErrors *>
							     (nRapidityBins,(TGraphErrors *)NULL));
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      backgroundGraph.at(iCentBin).at(yIndex) = new TGraphErrors();
      backgroundGraph.at(iCentBin).at(yIndex)->SetMarkerStyle(kFullCircle);
      backgroundGraph.at(iCentBin).at(yIndex)->SetMarkerColor(kBlack);
      backgroundGraph.at(iCentBin).at(yIndex)->SetName(Form("knockoutProton_%d_%d",iCentBin,yIndex));
      backgroundGraph.at(iCentBin).at(yIndex)->SetTitle(Form("#splitline{Au+Au #sqrt{s_{NN}} = %.03g | Centrality=[%d,%d]%%}{y_{p}=[%.03g,%.03g]};m_{T}-m_{p} (GeV/c^{2});#int_{%g}^{%g}Background(DCA) #/ #int_{%g}^{%g}Protons(DCA)",
							     GetCollisionEnergy(),
							     iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
							     (int)GetCentralityPercents().at(iCentBin),
							     GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
							     minRange,maxRange,
							     minRange,maxRange));
    }//End Loop Over yIndex
  }//End Loop Over iCentBin
  
  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  canvas->SetRightMargin(.05);
  canvas->SetTopMargin(.05);
  canvas->SetLeftMargin(.12);
  canvas->SetTicks();

  TCanvas *paramCanvas = new TCanvas("parameterCanvas","parameterCanvas",20,700,400,400);
  //paramCanvas->Divide(4,1);
  //paramCanvas->Update();
  
  TCanvas *backgroundCanvas = new TCanvas("backgroundCanvas","backgroundCanvas",900,20,800,600);
  backgroundCanvas->SetTopMargin(.05);
  backgroundCanvas->SetRightMargin(.02);
  backgroundCanvas->SetLeftMargin(.16);
  backgroundCanvas->SetTicks();

  for (int iCentBin=nCentralityBins-1; iCentBin>=0; iCentBin--){

    for (int yIndex=GetMinRapidityIndexOfInterest(PROTON); yIndex<=GetMaxRapidityIndexOfInterest(PROTON); yIndex++){
      
      if (yIndex != 20) continue;

	for (int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  
	  if (GetmTm0RangeCenter(mTm0Index) < .05 || GetmTm0RangeCenter(mTm0Index) > .5)
	    continue;
	  
	  protonPlus  = dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).first;
	  protonMinus = dcaDistribution.at(iCentBin).at(yIndex).at(mTm0Index).second;

	  protonPlus->Sumw2();
	  protonMinus->Sumw2();
	  
	  protonPlus->SetMarkerStyle(kFullSquare);
	  protonMinus->SetMarkerStyle(kOpenCircle);
	  
	  if (!protonPlus || !protonMinus)
	    continue;
	  
	  if (protonPlus->GetEntries() <100 || protonMinus->GetEntries() < 100)
	    continue;

	  //Take the Ratio of the anti-protons to protons
	  std::vector<double> ratioVals(protonPlus->GetNbinsX());
	  std::vector<double> binCenters(ratioVals.size());
	  for (unsigned int iBin=1; iBin<=ratioVals.size(); iBin++){
	    ratioVals.at(iBin-1) = protonMinus->GetBinContent(iBin) / protonPlus->GetBinContent(iBin);
	    binCenters.at(iBin-1) = protonMinus->GetBinCenter(iBin);
	  }
	  
	  TGraph ratioGraph(ratioVals.size(),&binCenters.at(0),&ratioVals.at(0));

	  paramCanvas->cd();
	  ratioGraph.Draw("A*");
	  paramCanvas->Update();


	  //Determine the anti-proton to proton ratio
	  int iVal=0;
	  std::vector<double> vals;
	  double maxDCA=0.2;
	  while (binCenters.at(iVal) <= maxDCA){
	    vals.push_back(ratioVals.at(iVal));
	    iVal++;
	  }
	  
	  Double_t pBarpRatio = TMath::Mean(vals.size(),&vals.at(0));
	  //Double_t peakLocation = protonPlus->GetBinCenter(protonPlus->GetMaximumBin());
	  //Double_t pBarpRatio = protonMinus->GetBinContent(protonMinus->FindBin(peakLocation)) / protonPlus->GetBinContent(protonPlus->FindBin(peakLocation));
	  //Double_t pBarpRatio = protonMinus->GetBinContent(1) / protonPlus->GetBinContent(1);

	  TF1 r("","pol0",0,maxDCA);
	  r.SetParameter(0,pBarpRatio);
	  r.Draw("SAME");
	  paramCanvas->Update();

	  //Create a new Histogram to Hold the scaled AntiProtons
	  TH1D *scaledProtonMinus = (TH1D *)protonMinus->Clone("scaledProtonMinus");
	  scaledProtonMinus->Scale(1.0/pBarpRatio);
	  scaledProtonMinus->SetLineColor(kBlue);
	  
	  //Create a New Histogram which will be the Protons-AntiProtons*(pBarpRatio)
	  TH1D diffHisto("diffHisto","",protonPlus->GetNbinsX(),protonPlus->GetBinLowEdge(1),protonPlus->GetBinLowEdge(protonPlus->GetNbinsX())+protonPlus->GetBinWidth(1));
	  diffHisto.Add(protonPlus,protonMinus,1.0,-1.0/pBarpRatio);

	  backgroundCanvas->cd();
	  diffHisto.Draw("E");

	  //Fit the Diff Histogram
	  TF1 fit("fit",KnockoutFunc,0,3,3);
	  fit.SetParameters(1,1,1);
	  diffHisto.Fit(&fit,"R");
	  
	  backgroundCanvas->Update();
	  
	  //gSystem->Sleep(1000);
	  //continue;
	  
	  //Draw the Histograms and Fits
	  canvas->cd();
	  TH1F *frame = canvas->DrawFrame(0,.1,3,1.15*protonPlus->GetBinContent(protonPlus->GetMaximumBin()));
	  frame->SetTitle(Form("#splitline{Au+Au #sqrt{s_{NN}} = %.03g GeV | Centrality=[%d,%d]%%}{y_{%s}=[%.03g,%.03g] | m_{T}-m_{%s}=[%.03g,%.03g] (GeV/c^{2})}",
			       GetCollisionEnergy(),
			       iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			       (int)GetCentralityPercents().at(iCentBin),
			       particleInfo->GetParticleSymbol(PROTON,1).Data(),
			       GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			       particleInfo->GetParticleSymbol(PROTON,1).Data(),
			       GetmTm0RangeLow(mTm0Index),GetmTm0RangeHigh(mTm0Index)));
	  frame->GetYaxis()->SetTitle("dN/d(glDCA)");
	  frame->GetXaxis()->SetTitle("Global DCA (cm)");
	  frame->GetYaxis()->SetTitleOffset(1.4);
	  frame->GetYaxis()->SetTitleFont(63);
	  frame->GetYaxis()->SetTitleSize(22);
	  frame->GetXaxis()->SetTitleFont(63);
	  frame->GetXaxis()->SetTitleSize(22);
	  protonPlus->Draw("E SAME");
	  protonMinus->Draw("E SAME");
	  scaledProtonMinus->Draw("SAME");
	  canvas->Update();

	  TPaveText *title = (TPaveText *)canvas->FindObject("title");
	  title->SetTextFont(63);
	  title->SetTextSize(25);
	  title->SetX1NDC(.38);
	  title->SetY1NDC(.80);
	  title->SetX2NDC(.88);
	  title->SetY2NDC(.92);

	  /*
	  TLegend *leg = new TLegend(.58,.61,.92,.80);
	  leg->SetBorderSize();
	  leg->SetFillColor(kWhite);
	  leg->SetNColumns(2);
	  leg->SetTextFont(43);
	  leg->SetTextSize(24);
	  leg->AddEntry(protonPlus,"p","P");
	  leg->AddEntry(fit,"p Fitted","L");
	  leg->AddEntry(protonMinus,"#bar{p}","P");
	  leg->AddEntry(signal,"#bar{p} Scaled","L");
	  leg->AddEntry((TObject *)NULL,"","");
	  leg->AddEntry(background,"Knockout","L");
	  leg->Draw("SAME");
	  */
	  
	  canvas->Modified();
	  canvas->Update();

	  gSystem->Sleep(1000);
	  delete scaledProtonMinus;
	  continue;
	  
	  //Draw the Background Graph
	  backgroundCanvas->cd();
	  backgroundGraph.at(iCentBin).at(yIndex)->Draw("APZ");
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleOffset(1.75);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleFont(63);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetYaxis()->SetTitleSize(22);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetXaxis()->SetTitleFont(63);
	  backgroundGraph.at(iCentBin).at(yIndex)->GetHistogram()->GetXaxis()->SetTitleSize(22);
	  backgroundCanvas->Update();

	  TPaveText *bTitle = (TPaveText *)backgroundCanvas->FindObject("title");
	  bTitle->SetTextFont(63);
	  bTitle->SetTextSize(25);
	  bTitle->SetX1NDC(.44);
	  bTitle->SetY1NDC(.80);
	  bTitle->SetX2NDC(.94);
	  bTitle->SetY2NDC(.92);

	  backgroundCanvas->Modified();
	  backgroundCanvas->Update();
	  
	  //Draw the Parameter Graphs
	  
	  
	  gSystem->Sleep(200);

	  /*
	  if (iCentBin == 8 && mTm0Index == 4 && iRound == 1){
	    canvas->SaveAs("canvas.C");
	  }
	  if (iCentBin == 8 && mTm0Index == 10 && iRound == 1){
	    backgroundCanvas->SaveAs("backgroundCanvas.C");
	    return;
	  }
	  */
	  
	}//End Loop Over mTm0Index
     }//End Loop Over yIndex
  }//End Loop Over Centrality Bin

	
}

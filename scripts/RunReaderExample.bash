#!/bin/bash

#This runs the RunReader.C macro which runs the reader.cxx function over 
#the data specified in the data directory. Each file is run in parallel.
#The script then waits until all the files have been processed before
#hadding the files together

###########################################################
#SET THE DATA DIRECTORY HERE
dataDirectory=/scratch_menkar/FixedTargetData/AuAu_4_5GeV_2015/

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/readerExample/

#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1

#Set the star library version here
starLib=SL16a

#SET THE ENERGY OF THE COLLISION HERE
energy=4.5

#SET THE EVENT CONFIGURATION HERE
eventConfig=FixedTarget2015

###########################################################

#Array containing all the datafiles
dataFiles=( $dataDirectory/*.root )
processID=()
numberOfFiles=${#dataFiles[@]}
outFiles=()

for i in ${dataFiles[@]}
do
    echo "Running on dataFile: " $i
    
    outFile=$(basename $i .root)
    outFile=$outputDirectory/"$outFile"_Processed.root

    outFiles+=($outFile)

    nice root -l -q -b ../macros/RunReaderExample.C\(\"$i\",\"$outFile\",$nEvents,\"$starLib\",$energy,\"$eventConfig\"\) > /dev/null 2>&1 & 
    
    processID+=($!)
    echo ${processID[@]}
done 
wait ${processID[@]}

hadd $outputDirectory/CombinedPileUpTest.root ${outFiles[@]}

wait

rm ${outFiles[@]}


exit

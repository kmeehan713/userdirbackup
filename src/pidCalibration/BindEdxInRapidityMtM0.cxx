#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TGraph.h>
#include <TTree.h>
#include <TBranch.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "usefulDataStructs.h"
#include "UserCuts.h"

//____MAIN_________________________________________________________
void BindEdxInRapidityMtM0(TString inputFile, TString outputFile, Long64_t nEvents=-1){


  //Set the Maximum number of tracks needed
  const int maxTracks(25000);
  const int minTracks(10000);
  
  //Get the Z-Vertex Cuts
  std::pair<double,double> zVertexCuts = GetZVertexCuts();

  //Read the input files
  //If there are valid z Vertex cuts then use them in the constructor of the davisdstreader
  DavisDstReader *davisDst = NULL;
  if (zVertexCuts.first != -999 && zVertexCuts.second != -999)
    davisDst = new DavisDstReader(inputFile,zVertexCuts.first,zVertexCuts.second);
  else
    davisDst = new DavisDstReader(inputFile);

  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;

  //Create the output file
  TFile *outFile = new TFile(outputFile,"RECREATE");

  //Create an instance of particle info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true);

  //Vector of Particle types that will be considered
  std::vector<int> particles;
  particles.push_back((int)PION);
  particles.push_back((int)KAON);
  particles.push_back((int)PROTON);
  particles.push_back((int)ELECTRON);
  particles.push_back((int)DEUTERON);
  particles.push_back((int)TRITON);
  particles.push_back((int)HELION);
  particles.push_back((int)ALPHA);
  particles.push_back((int)MUON);

  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();

  outFile->cd();
  TotalMomentumTreeData *avgKinData = new TotalMomentumTreeData;
  TTree *kinematicTree = new TTree("avgKinematicTree","AverageKinematicTree");
  TBranch *kinematicBranch = kinematicTree->Branch("avgKinematics",&avgKinData,10000,1);
  
  //----- Vectors to keep track of average pTotal -----
  std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > >
    totalMomentum (nCentralityBins,std::vector<std::vector<std::vector<std::vector<double> > > >
		   (nParticles,std::vector<std::vector<std::vector<double> > >
		    (nRapidityBins,std::vector<std::vector<double> >
		     (nmTm0Bins,std::vector<double>(4,0))))); //The inner most vector is (counts,rapiditySum,mTm0Sum,pTotalSum)
  
  //----- Bichsel Graphs -----
  std::vector<std::vector<std::vector<TGraph *> > >
    bichselGraph(nParticles,std::vector<std::vector<TGraph *> >(nRapidityBins,
								std::vector<TGraph *> (nParticles,
										       (TGraph *)NULL)));
  
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

    int particleSpecies = particles.at(iParticle);
    
    //Loop Over Rapidity and Particle Bins to make the Bichsel Graphs
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      for (unsigned int subSpeciesIndex=0; subSpeciesIndex<nParticles; subSpeciesIndex++){
	
	int subSpecies = particles.at(subSpeciesIndex);
	
	bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex) = new TGraph();
	bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->
	  SetName(Form("bichselBetaGamma_%s_%d_%s",
		       particleInfo->GetParticleName(particleSpecies).Data(),
		       yIndex,
		       particleInfo->GetParticleName(subSpecies).Data()));
	bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->
	  SetTitle(Form("dEdx Vs #beta#gamma: Mass=%s : y_{%s}=%.03g : SubSpecies=%s; #beta#gamma; dE/dx (KeV/cm)",
			particleInfo->GetParticleName(particleSpecies).Data(),
			particleInfo->GetParticleSymbol(particleSpecies).Data(),
			GetRapidityRangeCenter(yIndex),
			particleInfo->GetParticleName(subSpecies).Data()));
	bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->
	  SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->
	  SetMarkerStyle(kDot);
	
      }//End Loop Over Sub Species
    }//End Loop Over rapidity bins    
  }//End Loop Over Particle Species
  
  //Set the number of entries to read
  Long64_t nEntries = davisDst->GetEntries();
  if (nEvents > 0 && nEvents < nEntries)
    nEntries = nEvents;
  
  //----- Loop Over the Entries in the Tree -----
  for (unsigned int iEntry = 0; iEntry < nEntries; iEntry++){

    //Only continue reading events if the number of entries in the graphs
    //for pions, kaons, protons, or electrons has not reached its max
    bool keepGoing = false;
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

	if ((int)yIndex < GetMinRapidityIndexOfInterest() || (int)yIndex > GetMaxRapidityIndexOfInterest())
	  continue;
	
	if (bichselGraph.at(iParticle).at(yIndex).at(PION)->GetN() < minTracks)
	  keepGoing = true;
	else if (bichselGraph.at(iParticle).at(yIndex).at(KAON)->GetN() < minTracks)
	  keepGoing = true;
	else if (bichselGraph.at(iParticle).at(yIndex).at(PROTON)->GetN() < minTracks)
	  keepGoing = true;
	else if (bichselGraph.at(iParticle).at(yIndex).at(ELECTRON)->GetN() < .25*minTracks)
	  keepGoing = true;
	
      }//End Loop Over rapidity bins
    }//End Loop Over particles

    if (!keepGoing)
      break;

    //Get the Event entry from the DavisDst and make sure its good
    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    //Loop Over Vertices
    for (int iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){

      //Get the ith vertex and check if it is good
      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;

      //Compute the Centrality Variable
      int centVariable = GetCentralityVariable(primaryVertex);
      
      //Get the Centrality Bin (skip if bad)
      int iCentBin = GetCentralityBin(centVariable,
				      event->GetTriggerID(&allowedTriggers),
				      primaryVertex->GetZVertex());
      if (iCentBin < 0)
	continue;
      
      //Loop Over the Tracks
      for (int iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){

	//Get the ith track and check if it is good
	track = primaryVertex->GetPrimaryTrack(iTrack);
	if (!IsGoodTrack(track))
	  continue;

	//Fill the dEdx Histograms for each particle spcies
	for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

	  //Get track information
	  int particleSpecies  = particles.at(iParticle);
	  float massAssumption = particleInfo->GetParticleMass(particleSpecies);
	  float rapidity       = track->GetRapidity(massAssumption);
	  float mTm0           = track->GetmTm0(massAssumption);
	  float dEdx           = track->GetdEdx()*1000000;
	  float inverseBeta    = 1.0/track->GetTofBeta();
	  float pTotal         = track->GetPTotal();

	  //Make Sure the indicies are in range
	  Int_t yIndex = GetRapidityIndex(rapidity);
	  Int_t mTm0Index = GetmTm0Index(mTm0);
	  if (yIndex < 0 || yIndex >= nRapidityBins)
	    continue;
	  if (mTm0Index < 0 || mTm0Index >= nmTm0Bins)
	    continue;

	  //Determine if this is a good tof track
	  Bool_t isGoodTofTrack = IsGoodTofTrack(track);

	  //Add to the total momentum of the tracks in this bin and increment the counters	  
	  totalMomentum.at(iCentBin).at(iParticle).at(yIndex).at(mTm0Index).at(0)++;            //Increment Counter
	  totalMomentum.at(iCentBin).at(iParticle).at(yIndex).at(mTm0Index).at(1) += rapidity;  //Add to rapiditySum
	  totalMomentum.at(iCentBin).at(iParticle).at(yIndex).at(mTm0Index).at(2) += mTm0;      //Add to mTm0Sum
	  totalMomentum.at(iCentBin).at(iParticle).at(yIndex).at(mTm0Index).at(3) += pTotal;    //Add to pTotalSum

	  //Since we do not have good seperation for pid for pTotal > 1.0 GeV
	  //kill particles with high momentum
	  if (pTotal > 1.0)
	    continue;
	  
	  //Fill the Bichsel Graphs with tracks that are of the sub species type
	  for (unsigned int subSpeciesIndex=0; subSpeciesIndex<nParticles; subSpeciesIndex++){

	    //Particle ID of Subspecies
	    int subSpecies = particles.at(subSpeciesIndex);

	    //Bool used to determine whether the track should be filled or not
	    Bool_t fillTrack = false;
	    
	    //If the Track is a good ToF Track then use ToF Info
	    if (isGoodTofTrack){
	      
	      if (particleInfo->GetPercentErrorTPC(dEdx,pTotal,subSpecies) < 20 &&
		  particleInfo->IsExclusivelyThisSpeciesTOF(inverseBeta,subSpecies,pTotal))
		fillTrack = true;

	      //A More strict check for electrons
	      if (subSpecies == ELECTRON){
		if (!particleInfo->DoTPCandTOFAgreeOnThisSpecies(dEdx,inverseBeta,pTotal,subSpecies))
		  fillTrack = false;
	      }
	      
	    }
	    //If this is a bad Tof Track, but also has low momentum
	    else if (!isGoodTofTrack && mTm0 < 0.4){
	      
	      if (particleInfo->GetPercentErrorTPC(dEdx,pTotal,subSpecies) < 20)
		fillTrack = true;

	      //A More strict check for electrons
	      if (subSpecies == ELECTRON){
		if (!particleInfo->IsExclusivelyThisSpeciesTPC(dEdx,ELECTRON,pTotal,20,true))
		  fillTrack = false;
	      }
	      
	    }

	    //Fill the Track
	    if (fillTrack){
	      double trackBetaGamma = track->GetBetaGamma(particleInfo->GetParticleMass(subSpecies));
	      
	      if (bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->GetN() < maxTracks)
		bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->
		  SetPoint(bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->GetN(),
			   trackBetaGamma,dEdx);
	    }//End Fill BetaGamma
	    
	  }//End Loop Over subSpecies
	}//End Loop Over Particle Species
      }//End Loop Over Tracks
    }//End Loop Over Vertices    
  }//End Loop Over Entries

  //Make Directories in output file
  outFile->cd();
  outFile->mkdir("BichselBetaGammaGraphs");
  outFile->mkdir("TotalMomentum");
  
  //---- Save the Average Kinematics Tree
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
      int particleSpecies = particles.at(iParticle);
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  
	  std::vector<double> *currentVec = &(totalMomentum.at(iCentBin).at(iParticle).at(yIndex).at(mTm0Index));
	  
	  //Make sure there are counts
	  if (currentVec->at(0) == 0)
	    continue;
	  
	  double avgRapidity = currentVec->at(1) / currentVec->at(0);
	  double avgmTm0     = currentVec->at(2) / currentVec->at(0);
	  double avgTotalP   = currentVec->at(3) / currentVec->at(0);
	  
	  //Set the Average Kinematic Data and Fill tree with it
	  avgKinData->centBinIndex = iCentBin;
	  avgKinData->particleSpeciesIndex = particleSpecies;
	  avgKinData->yIndex = yIndex;
	  avgKinData->mTm0Index = mTm0Index;
	  avgKinData->avgRapidity = avgRapidity;
	  avgKinData->avgmTm0 = avgmTm0;
	  avgKinData->avgTotalP = avgTotalP;
	  
	  kinematicTree->Fill();
	  
	}//End Loop Over mTm0Index
      }//End Loop Over yIndex      
    }//End Loop Over Particles
  }//End Loop Over Centrality Bins

  outFile->cd();
  outFile->cd("TotalMomentum");
  kinematicTree->Write();  
  
  //---- Save All the Bichsel Graphs
  outFile->cd();
  outFile->cd("BichselBetaGammaGraphs");
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      for (unsigned int subSpeciesIndex=0; subSpeciesIndex<nParticles; subSpeciesIndex++){
	if (bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->GetN() > 5)
	  bichselGraph.at(iParticle).at(yIndex).at(subSpeciesIndex)->Write();
      }//End Loop Over subspecies index
    }//End Loop Over rapidity bin
    
  }//End Loop Over Particles

  outFile->Close();
  
}


//The purpose of this code is to fit the hadronic + tpc efficiency corrected spectra with the Double Thermal function from Jenn Klay's paper arxiv:nucl-ex:0306033 26 Sept 2003. Results from this function will be compared to the results from a Double Bose-Einstein to obtain a systematic uncertainty on choice of fit function for dN/dy extrapolation

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#include <TAxis.h>
#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TLatex.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

using namespace std;

//________________________________________________________________________________________________________
void RemovePointsWithLargeErrors(TGraphErrors *spectrum, Double_t maxRelativeError=.1){

	  //Loop Over the Points of the spectrum. Remove any point which is found
		//to have a relativeError larger than maxRelativeError
		for (int iPoint=spectrum->GetN()-1; iPoint>=0; iPoint--){
		    if (spectrum->GetEY()[iPoint] / spectrum->GetY()[iPoint] > maxRelativeError)
		          spectrum->RemovePoint(iPoint);
		}
}

//________________________________________________________________________________________________________
void fitPionSpectraMixed(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge, Double_t midY){

  Bool_t draw = false;
  Bool_t drawJK = false;

  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = 1;//GetNCentralityBins();
  Double_t mTm0min, mTm0max, dNdy, dNdyErr, dNdy1, dNdy2, dNdy1Err, dNdy2Err, dNdySystErr;
  Int_t nPoints;
  TGraphErrors *tpcCorrectedSpectrum, *tpcRawSpectrum, *tpcSysErrSpectrum, *JKspectra;
  TF1 *fitMixed, *tSlopeFit1, *tSlopeFit2, *fitLowTempOnly, *fitHighTempOnly, *fitJKTotal, *fitJKTotalExtra, *fitJKLow, *fitJKHigh;
	vector <TGraphErrors *> tSlopeGraph1(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> tSlopeGraph2(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph1(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph2(nCentBins, (TGraphErrors *)NULL);
	vector <TGraphErrors *> dNdyGraph(nCentBins, (TGraphErrors *)NULL);

  //Create the OutputFile
  TFile *resultsFile = new TFile(resultFileName,"UPDATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
		//dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    //gPad->DrawFrame(-2,0,2,150);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");
	TFile *JKspectraFile;
  if (drawJK) JKspectraFile = new TFile("/scratch_menkar/kmeehan/TeamFXT/UserCode/davisdstanalysis/userfiles/AuAu_4_5GeV_2015/officialProduction/analysis/JennKlaySpectra/jkSpectra.root","READ");

  //Loop Over the Centrality Bins and Rapidity Bins and apply the Corrections
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
   
	  //Create the temperature/inverse slope parameter graph and fit
	  tSlopeGraph1.at(iCentBin) = new TGraphErrors();
	  tSlopeGraph1.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph1.at(iCentBin)->SetName(Form("Slope1Parameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));
	  tSlopeGraph2.at(iCentBin) = new TGraphErrors();
	  tSlopeGraph2.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
		tSlopeGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid, charge));
		tSlopeGraph2.at(iCentBin)->SetName(Form("Slope2Parameter_%s_Cent%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin));

		tSlopeFit1 = new TF1("tSlopeFit1","gaus(0)",-1.8,0.2);
		tSlopeFit1->SetNpx(10000);
		tSlopeFit1->SetName(Form("SlopeParameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit1->SetParameter(0,0.190);
		tSlopeFit1->FixParameter(1,midY); 
		tSlopeFit1->SetParameter(2,1.5); 
		tSlopeFit1->SetParNames("Amp", "#mu", "#sigma");

		tSlopeFit2 = new TF1("tSlopeFit2","gaus(0)",-1.8,0.2);
		tSlopeFit2->SetNpx(10000);
		tSlopeFit2->SetName(Form("Slope2Parameter_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
		tSlopeFit2->FixParameter(0,0.058);//Set
		tSlopeFit2->FixParameter(1,midY); 
		tSlopeFit2->FixParameter(2,2.77); //Set
		//tSlopeFit2->SetParLimits(2,2.0,3.0);
		tSlopeFit2->SetParNames("Amp", "#mu", "#sigma");

		//Create the dN/dy Graphs
		dNdyGraph.at(iCentBin) = new TGraphErrors();
		dNdyGraph.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph.at(iCentBin)->SetName(Form("RapidityDensityTotal_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));
		dNdyGraph.at(iCentBin)->GetXaxis()->SetTitle("y-y_{cm}");
		dNdyGraph.at(iCentBin)->GetYaxis()->SetTitle("dN/dy");

		dNdyGraph1.at(iCentBin) = new TGraphErrors();
		dNdyGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph1.at(iCentBin)->SetName(Form("RapidityDensity1_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));
		dNdyGraph1.at(iCentBin)->GetXaxis()->SetTitle("y-y_{cm}");
		dNdyGraph1.at(iCentBin)->GetYaxis()->SetTitle("dN/dy");

		dNdyGraph2.at(iCentBin) = new TGraphErrors();
		dNdyGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
		dNdyGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraph2.at(iCentBin)->SetName(Form("RapidityDensity2_%s_Cent%02d",
           particleInfo->GetParticleName(pid,charge).Data(),
           iCentBin));
		dNdyGraph2.at(iCentBin)->GetXaxis()->SetTitle("y-y_{cm}");
		dNdyGraph2.at(iCentBin)->GetYaxis()->SetTitle("dN/dy");

		for(Int_t iRound = 0;iRound<2;iRound++){

			for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){//min was 2


				cout <<Form("INFO: - Correcting Spectrum: %s CentIndex: %d yIndex %d",
				particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex) <<endl;

				//Compute Rapidity
				Double_t rapidity = GetRapidityRangeCenter(yIndex);

				//Get Spectra
				spectraFile->cd();
				tpcCorrectedSpectrum = (TGraphErrors *)gDirectory->Get(Form("CorrectedSpectra_%s/correctedSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));
				cout<<"tpcCorrectedSpectrum "<<tpcCorrectedSpectrum<<endl;
				if (!tpcCorrectedSpectrum) continue;
				tpcRawSpectrum = (TGraphErrors *)gDirectory->Get(Form("RawSpectra_%s/rawSpectra_%s_Cent%02d_yIndex%02d",
									particleInfo->GetParticleName(pid,charge).Data(),
									particleInfo->GetParticleName(pid,charge).Data(),iCentBin,yIndex));

				nPoints = tpcCorrectedSpectrum->GetN();
				if (nPoints <= 5) continue;

				mTm0min = 0.08;
				if (tpcCorrectedSpectrum->GetX()[nPoints-1] > 1.4)
          mTm0max = 1.4;
				else mTm0max = tpcCorrectedSpectrum->GetX()[nPoints-1];

        TGraphChop(tpcCorrectedSpectrum,mTm0max,false);
        TGraphChop(tpcCorrectedSpectrum,mTm0min,true);
				RemovePointsWithLargeErrors(tpcCorrectedSpectrum);
				nPoints = tpcCorrectedSpectrum->GetN();
				Double_t mTm0maxNew = tpcCorrectedSpectrum->GetX()[nPoints-1];
        Double_t mTm0minNew = tpcCorrectedSpectrum->GetX()[0];

				//Get the fit function
				if (yIndex < 17) fitMixed = new TF1("",BoseAndThermalFit,mTm0minNew,1.4,6);
				else if (yIndex == 17) fitMixed = new TF1("",BoseAndThermalFit,mTm0minNew,mTm0maxNew,6);
				else if (yIndex == 18) fitMixed = new TF1("",BoseAndThermalFit,mTm0minNew,mTm0maxNew,6);
				fitMixed->SetName(Form("spectrumFitBoseAndThermal_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
				fitMixed->SetLineStyle(1);
				fitMixed->SetLineColor(2);
				fitMixed->SetLineWidth(2);
			  			
				/*
				fitMixedExtra = new TF1("",DoubleThermalFit,0,mTm0maxNew,5);
				fitMixedExtra->SetName(Form("spectrumFitDoubleThermalExtra_%s_Cent%02d_yIndex%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin,yIndex));
				fitMixedExtra->SetLineColor(2);
				fitMixedExtra->SetLineWidth(2);
				*/
				//Set the parameters
				
				fitMixed->SetParameter(0, 14);
				fitMixed->SetParameter(1, 0.190);
				fitMixed->SetParameter(2, 40);
				fitMixed->SetParameter(3, 0.066);
				fitMixed->FixParameter(4, particleInfo->GetParticleMass(pid));
				fitMixed->FixParameter(5, rapidity+1.52);
				fitMixed->SetParNames("dN/dy_{1}", "T_{InverseSlope,1}", "dN/dy_{2}", "T_{InverseSlope,2}", "#pi mass", "y");
				//fitMixedExtra->SetParNames("dN/dy_{1}", "T_{InverseSlope,1}", "dN/dy_{2}", "T_{InverseSlope,2}", 
				//		"#pi mass");

				if (iRound > 0){
						fitMixed->FixParameter(1, tSlopeFit1->Eval(rapidity));
						fitMixed->FixParameter(3, tSlopeFit2->Eval(rapidity));
				}

				//Perform the fit
				tpcCorrectedSpectrum->Fit(fitMixed,"REXO");

				/*
				fitMixedExtra->FixParameter(0, fitMixed->GetParameter(0));
				fitMixedExtra->FixParameter(1, fitMixed->GetParameter(1));
				fitMixedExtra->FixParameter(2, fitMixed->GetParameter(2));
				fitMixedExtra->FixParameter(3, fitMixed->GetParameter(3));
				fitMixedExtra->FixParameter(4, fitMixed->GetParameter(4));
				

				if (iRound>0) tpcCorrectedSpectrum->Fit(fitMixedExtra,"REXO+");
        */

				//Draw each component
				fitLowTempOnly = new TF1("",ThermalFitFunc,0,2.0,3);
				fitLowTempOnly->SetLineColor(1);
				fitLowTempOnly->SetLineStyle(10);
				fitLowTempOnly->FixParameter(0, fitMixed->GetParameter(2));
				fitLowTempOnly->FixParameter(1, fitMixed->GetParameter(3));
				fitLowTempOnly->FixParameter(2, fitMixed->GetParameter(4));
				if (iRound > 0) tpcCorrectedSpectrum->Fit(fitLowTempOnly,"REXO+");

				fitHighTempOnly = new TF1("",BoseEinsteinFitFunc,0,2.0,4);
				fitHighTempOnly->SetLineColor(1);
				fitHighTempOnly->SetLineStyle(2);
				fitHighTempOnly->FixParameter(0, fitMixed->GetParameter(0));
				fitHighTempOnly->FixParameter(1, fitMixed->GetParameter(1));
				fitHighTempOnly->FixParameter(2, fitMixed->GetParameter(4));
				fitHighTempOnly->FixParameter(3, fitMixed->GetParameter(5));
				if (iRound > 0) tpcCorrectedSpectrum->Fit(fitHighTempOnly,"REXO+");

				if (iRound==0){
				 
					tSlopeGraph1.at(iCentBin)->SetPoint(tSlopeGraph1.at(iCentBin)->GetN(),
										rapidity, fitMixed->GetParameter(1));								
					tSlopeGraph1.at(iCentBin)->SetPointError(tSlopeGraph1.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitMixed->GetParError(1));								

					tSlopeGraph2.at(iCentBin)->SetPoint(tSlopeGraph2.at(iCentBin)->GetN(),
										rapidity, fitMixed->GetParameter(3));								
					tSlopeGraph2.at(iCentBin)->SetPointError(tSlopeGraph2.at(iCentBin)->GetN()-1,
										rapidityBinWidth/2.0, fitMixed->GetParError(3));								

				}

        if (iRound==1){

					dNdy1 = fitMixed->GetParameter(0);
					dNdy2 = fitMixed->GetParameter(2);
					dNdy = dNdy1 + dNdy2;

					//Below line is prob wrong since error between dndy1 and dndy2 will prob be correlated
					dNdyErr = sqrt(fitMixed->GetParError(0)*fitMixed->GetParError(0) + fitMixed->GetParError(2)*fitMixed->GetParError(2));
					dNdy1Err = fitMixed->GetParError(0);
					dNdy2Err = fitMixed->GetParError(2);
					dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),rapidity-midY,dNdy);
					dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdyErr);

					dNdyGraph1.at(iCentBin)->SetPoint(dNdyGraph1.at(iCentBin)->GetN(),rapidity-midY,dNdy1);
					dNdyGraph1.at(iCentBin)->SetPointError(dNdyGraph1.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdy1Err);

					dNdyGraph2.at(iCentBin)->SetPoint(dNdyGraph2.at(iCentBin)->GetN(),rapidity-midY,dNdy2);
					dNdyGraph2.at(iCentBin)->SetPointError(dNdyGraph2.at(iCentBin)->GetN()-1,
									rapidityBinWidth/2.0,dNdy2Err);
        }

				if (draw && yIndex == 5){

					canvas->cd();
					canvas->DrawFrame(0,0.01*TMath::MinElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()),
								2.0,20*TMath::MaxElement(tpcCorrectedSpectrum->GetN(),tpcCorrectedSpectrum->GetY()));
					canvas->SetLogy();
					tpcCorrectedSpectrum->Draw("APZ");	
					tpcCorrectedSpectrum->GetXaxis()->SetTitle("m_{T}-m_{0} (GeV)");
					
					fitMixed->SetLineColor(2);
					fitMixed->SetLineWidth(2);
					fitMixed->Draw("SAME");
					/*
					fitMixedExtra->SetLineStyle(7);
					fitMixedExtra->SetLineColor(2);
					fitMixedExtra->SetLineWidth(2);
					fitMixedExtra->Draw("SAME");
					fitMixedExtra->SetParNames("dN/dy_{1}", "T_{InverseSlope,1}", "dN/dy_{2}", "T_{InverseSlope,2}", 
							"#pi mass");
					*/

					//Draw Jenn Klay's spectra
					if (drawJK){
						JKspectraFile->cd();
						JKspectra = (TGraphErrors *)JKspectraFile->Get(Form("preparedSpectra_%s_Cent%02d_yIndex%02d",
											particleInfo->GetParticleName(pid,charge).Data(), iCentBin, yIndex));
						JKspectra->SetMarkerColor(4);
						JKspectra->Draw("PSAME");
						Double_t min = JKspectra->GetX()[0];
						Double_t max = JKspectra->GetX()[JKspectra->GetN()-1];
						fitJKTotal = new TF1("",DoubleThermalFit,min,max,5);
						fitJKTotal->SetName(Form("JKspectrumFitDoubleThermal_%s_Cent%02d_yIndex%02d",
								particleInfo->GetParticleName(pid,charge).Data(),
								iCentBin,yIndex));
						fitJKTotal->SetLineStyle(1);
						fitJKTotal->SetLineColor(4);
						fitJKTotal->SetLineWidth(2);

						fitJKTotalExtra = new TF1("",DoubleThermalFit,0,2,5);
						fitJKTotalExtra->SetName(Form("spectrumFitDoubleThermalExtra_%s_Cent%02d_yIndex%02d",
								particleInfo->GetParticleName(pid,charge).Data(),
								iCentBin,yIndex));
						fitJKTotalExtra->SetLineStyle(7);
						fitJKTotalExtra->SetLineColor(4);
						fitJKTotalExtra->SetLineWidth(2);

						fitJKTotal->FixParameter(0, 31.4547);
						fitJKTotal->FixParameter(1, 0.0555035);
						fitJKTotal->FixParameter(2, 30.6114);
						fitJKTotal->FixParameter(3, 0.137041);
						fitJKTotal->FixParameter(4, particleInfo->GetParticleMass(pid));
						fitJKTotal->SetParNames("dN/dy_{1}", "T_{InverseSlope,1}", "dN/dy_{2}", "T_{InverseSlope,2}", "#pi mass");
						JKspectra->Fit(fitJKTotal,"REXO");

						fitJKTotalExtra->FixParameter(0, fitJKTotal->GetParameter(0));
						fitJKTotalExtra->FixParameter(1, fitJKTotal->GetParameter(1));
						fitJKTotalExtra->FixParameter(2, fitJKTotal->GetParameter(2));
						fitJKTotalExtra->FixParameter(3, fitJKTotal->GetParameter(3));
						fitJKTotalExtra->FixParameter(4, fitJKTotal->GetParameter(4));

						//Draw each component
						fitJKLow = new TF1("",ThermalFitFunc,0,2.0,3);
						fitJKLow->SetLineColor(4);
						fitJKLow->SetLineStyle(10);
						fitJKLow->FixParameter(0, fitJKTotal->GetParameter(0));
						fitJKLow->FixParameter(1, fitJKTotal->GetParameter(1));
						fitJKLow->FixParameter(2, fitJKTotal->GetParameter(4));
						JKspectra->Fit(fitJKLow,"REXO+");

						fitJKHigh = new TF1("",ThermalFitFunc,0,2.0,3);
						fitJKHigh->SetLineColor(4);
						fitJKHigh->SetLineStyle(2);
						fitJKHigh->FixParameter(0, fitJKTotal->GetParameter(2));
						fitJKHigh->FixParameter(1, fitJKTotal->GetParameter(3));
						fitJKHigh->FixParameter(2, fitJKTotal->GetParameter(4));
						JKspectra->Fit(fitJKHigh,"REXO+");
          }  
				}

				tpcCorrectedSpectrum->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{-2}");
				tpcCorrectedSpectrum->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				             particleInfo->GetParticleSymbol(pid).Data()));
				tpcCorrectedSpectrum->GetYaxis()->SetTitleFont(63);
				tpcCorrectedSpectrum->GetYaxis()->SetTitleSize(25);
        tpcCorrectedSpectrum->GetXaxis()->SetTitleFont(63);
				tpcCorrectedSpectrum->GetXaxis()->SetTitleSize(25);
				tpcCorrectedSpectrum->GetXaxis()->SetTitleOffset(1.3);

				cout<<"Saving Spectra"<<endl;
				//Save Spectra
				if (iRound==1){
					resultsFile->cd();
					resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
					if (tpcCorrectedSpectrum) tpcCorrectedSpectrum->Write();
					resultsFile->cd();
					resultsFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
				  if (tpcCorrectedSpectrum)	tpcRawSpectrum->Write();
					resultsFile->cd();
				}

				cout<<"SAVED"<<endl;

			}//End of rapidity loop

		  if (iRound==0) {
				tSlopeGraph1.at(iCentBin)->Fit(tSlopeFit1,"EX0","",-1.8,-0.48);
				tSlopeGraph2.at(iCentBin)->Fit(tSlopeFit2,"EX0","",-1.8,-0.48);
			}

		}//End of loop over fitting rounds

	//	if (draw){
	//		dNdyCanvas->cd();
	//		dNdyGraph.at(iCentBin)->Draw("APZ");
	//	}

    //Save Plots
		tSlopeGraph1.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph1.at(iCentBin)->GetYaxis()->SetTitle("T/inverseSlope MeV");
		tSlopeGraph2.at(iCentBin)->GetXaxis()->SetTitle("y_{lab}");
		tSlopeGraph2.at(iCentBin)->GetYaxis()->SetTitle("T/inverseSlope MeV");


		resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
		if (tSlopeGraph1.at(iCentBin)->GetN() > 0){
			tSlopeGraph1.at(iCentBin)->Write();
		}
		if (tSlopeGraph2.at(iCentBin)->GetN() > 0){
			tSlopeGraph2.at(iCentBin)->Write();
		}

		resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0) dNdyGraph.at(iCentBin)->Write();
    if (dNdyGraph1.at(iCentBin)->GetN() > 0) dNdyGraph1.at(iCentBin)->Write();
    if (dNdyGraph2.at(iCentBin)->GetN() > 0) dNdyGraph2.at(iCentBin)->Write();
	

	}//End of centrality loop

}//End of function

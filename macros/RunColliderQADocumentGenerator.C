void RunColliderQADocumentGenerator(TString embeddingFile, TString dataQAFile, TString outDir, int pid, int charge,
		   TString starLibrary, Double_t energy=0, TString eventConfig=""){
  
  //Load the Necessary Libraries
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/usefulDataStructs_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/ColliderQADocumentGenerator_cxx.so");

  //Set the User Cuts
  SetVariableUserCuts(energy,eventConfig,starLibrary);

  ColliderQADocumentGenerator(embeddingFile,dataQAFile,outDir,pid,charge);

}

//Fit the Proton Spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

bool draw = false;

TGraphErrors *FitWithDoubleExponential(std::vector< std::vector<TMultiGraph *> > *fullSpectra, int pid, int charge,
				       int iCentBin, int userRapidityBin, TCanvas *canvas, TCanvas *dNdyCanvas,
				       TFile *resultsFile);
TGraphErrors *FitWithSiemensRasmussen(std::vector< std::vector<TMultiGraph *> > *fullSpectra, int pid, int charge,
				       int iCentBin, int userRapidityBin, TCanvas *canvas, TCanvas *dNdyCanvas,
				       TFile *resultsFile);

//____MAIN________________________________________________________________________________________
void fitProtonSpectra(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge,
		      Int_t userCentBin = -999, Double_t userRapidity = -999){

  TVirtualFitter::SetMaxIterations(10000);
  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();

  //Get the User's rapidity bin
  Int_t userRapidityBin = GetRapidityIndex(userRapidity);
  if (userRapidity != -999 && userRapidityBin < 0){
    cout <<"ERROR: FitSpectra - Invalid value of rapidity. EXITING!\n";
    exit (EXIT_FAILURE);
  }

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Create the Output Results File
  TFile *resultsFile = new TFile(resultFileName,"RECREATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("FinalSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
  
  //Create a Canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,650,1800,500);
    dNdyCanvas->Divide(4,1);
    dNdyCanvas->cd(1);
    gPad->DrawFrame(-2,0,2,150);
    dNdyCanvas->cd(2);
    gPad->DrawFrame(-2,0,2,0.4);
  }


  //Get the Spectra From the File
  std::vector< std::vector< std::pair<TGraphErrors *,TGraphErrors *> > >
    spectra(nCentBins, std::vector<std::pair<TGraphErrors *,TGraphErrors *> >
	    (nRapidityBins, std::pair<TGraphErrors *, TGraphErrors *>
	     (std::make_pair((TGraphErrors *)NULL,(TGraphErrors *)NULL))));
  std::vector< std::vector< TMultiGraph *> >
    fullSpectra(nCentBins, std::vector<TMultiGraph *>
		(nRapidityBins, (TMultiGraph *)NULL));
  
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    
    //If the user has specified a valid cent bin,
    //then skip all others
    if (userCentBin != -999 && userCentBin >= 0 &&
	userCentBin < nCentBins && iCentBin != userCentBin)
      continue;

      for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
	
	//Skip Rapidity bins not of interest
	if (yIndex < GetMinRapidityIndexOfInterest(pid) || yIndex > GetMaxRapidityIndexOfInterest(pid))
	  continue;
	
	//If the user has specified a vaid rapidity value,
	//then skip all other rapidity bins
	if (userRapidityBin >= 0 && userRapidityBin != yIndex)
	  continue;
	
	//Compute Rapidity
	Double_t rapidity = GetRapidityRangeCenter(yIndex);
	
	//Get the Spectra
	spectraFile->cd();
	spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	spectra.at(iCentBin).at(yIndex).first =
	  (TGraphErrors *)gDirectory->Get(Form("correctedSpectra_%s_Cent%02d_yIndex%02d",
					       particleInfo->GetParticleName(pid,charge).Data(),
					       iCentBin,yIndex));
	spectra.at(iCentBin).at(yIndex).second =
	  (TGraphErrors *)gDirectory->Get(Form("correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
					       particleInfo->GetParticleName(pid,charge).Data(),
					       iCentBin,yIndex));

	fullSpectra.at(iCentBin).at(yIndex) = new TMultiGraph();
	if (spectra.at(iCentBin).at(yIndex).first)
	  fullSpectra.at(iCentBin).at(yIndex)->Add(spectra.at(iCentBin).at(yIndex).first);
	if (spectra.at(iCentBin).at(yIndex).second)
	  fullSpectra.at(iCentBin).at(yIndex)->Add(spectra.at(iCentBin).at(yIndex).second);
	fullSpectra.at(iCentBin).at(yIndex)->SetName(Form("finalSpectrum_%s_Cent%02d_yIndex%02d",
							  particleInfo->GetParticleName(pid,charge).Data(),
							  iCentBin,yIndex));

	//Save Spectra to the results file
	resultsFile->cd();
	resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	if (spectra.at(iCentBin).at(yIndex).first)
	  spectra.at(iCentBin).at(yIndex).first->Write();
	if (spectra.at(iCentBin).at(yIndex).second)
	  spectra.at(iCentBin).at(yIndex).second->Write();
	resultsFile->cd();
	resultsFile->cd(Form("FinalSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
	fullSpectra.at(iCentBin).at(yIndex)->Write();
	
      }//End Loop Over Rapidity Bins    
  }//End Loop Over Centrality Bins

  std::vector<TGraphErrors *> dNdyGraph(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> dNdyGraphSym(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> dNdyGraphSys(nCentBins,(TGraphErrors *)NULL);
    
  //Loop Over the Centrality and rapidity bins and fit the spectra
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    
    //If the user has specified a valid cent bin,
    //then skip all others
    if (userCentBin != -999 && userCentBin >= 0 &&
	userCentBin < nCentBins && iCentBin != userCentBin)
      continue;

    
    //dNdyGraph.at(iCentBin) = FitWithDoubleExponential(&fullSpectra,pid,charge,iCentBin,userRapidityBin,canvas,dNdyCanvas,resultsFile);
    cout <<"DONE WITH FITTING DOUBLE EXPONENTIAL" <<endl;

    dNdyGraph.at(iCentBin) = FitWithSiemensRasmussen(&fullSpectra,pid,charge,iCentBin,userRapidityBin,canvas,dNdyCanvas,resultsFile);
    cout <<"DONE WITH FITTING SIEMENS RASMUSSEN" <<endl;
    
    //Symmeterize the Rapidity Density Graph
    dNdyGraphSym.at(iCentBin) = SymmeterizedNdyGraph(dNdyGraph.at(iCentBin));
    dNdyGraphSym.at(iCentBin)->SetName(Form("%s_Symm",dNdyGraph.at(iCentBin)->GetName()));
    dNdyGraphSym.at(iCentBin)->SetMarkerStyle(dNdyGraph.at(iCentBin)->GetMarkerStyle());
    dNdyGraphSym.at(iCentBin)->SetMarkerColor(dNdyGraph.at(iCentBin)->GetMarkerColor());

    //Get the Systematic Error Graph
    dNdyGraphSys.at(iCentBin) = SystematicdNdyGraph(dNdyGraph.at(iCentBin));
    dNdyGraphSys.at(iCentBin)->SetName(Form("%s_SysErr",dNdyGraphSym.at(iCentBin)->GetName()));
    dNdyGraphSys.at(iCentBin)->SetMarkerStyle(kDot);
    dNdyGraphSys.at(iCentBin)->SetFillColor(particleInfo->GetParticleColor(pid));
    dNdyGraphSys.at(iCentBin)->SetFillStyle(3001);

    if (draw){
      dNdyCanvas->cd(1);
      dNdyGraphSys.at(iCentBin)->Draw("2");
      dNdyGraphSym.at(iCentBin)->Draw("PZ");
    }
    
    resultsFile->cd();
    resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraph.at(iCentBin)->GetN() > 0){
      dNdyGraph.at(iCentBin)->Write();
      dNdyGraphSym.at(iCentBin)->Write();
      dNdyGraphSys.at(iCentBin)->Write();
    }

    
  }//End Loop Over Cent Bin
  
}


//____________________________________________________________________________________________________
TGraphErrors *FitWithDoubleExponential(std::vector<std::vector<TMultiGraph *> > *fullSpectra, int pid, int charge,
				       int iCentBin, int userRapidityBin, TCanvas *canvas, TCanvas *dNdyCanvas,
				       TFile *resultsFile){
  
  ParticleInfo *particleInfo = new ParticleInfo();
  TGraphErrors *dNdyGraph = new TGraphErrors();
  TGraphErrors *tSlopeGraph1 = new TGraphErrors();
  TGraphErrors *tSlopeGraph2 = new TGraphErrors();
  TGraphErrors *ampGraph1 = new TGraphErrors();
  TGraphErrors *ampGraph2 = new TGraphErrors();
  
  dNdyGraph->SetName(Form("RapidityDensity_%s_Cent%02d",
				       particleInfo->GetParticleName(pid,charge).Data(),
				       iCentBin));
  tSlopeGraph1->SetName(Form("slopeParam1_%s_Cent%02d",
					  particleInfo->GetParticleName(pid,charge).Data(),
					  iCentBin));
  ampGraph1->SetName(Form("ampParam1_%s_Cent%02d",
					particleInfo->GetParticleName(pid,charge).Data(),
					iCentBin));
  tSlopeGraph2->SetName(Form("slopeParam2_%s_Cent%02d",
					  particleInfo->GetParticleName(pid,charge).Data(),
					  iCentBin));
  ampGraph2->SetName(Form("ampParam2_%s_Cent%02d",
					particleInfo->GetParticleName(pid,charge).Data(),
					iCentBin));
  
  dNdyGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  tSlopeGraph1->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  tSlopeGraph2->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  ampGraph1->SetMarkerStyle(kFullCircle);
  ampGraph2->SetMarkerStyle(kFullCross);
  
  dNdyGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));
  tSlopeGraph1->SetMarkerColor(particleInfo->GetParticleColor(pid));
  tSlopeGraph2->SetMarkerColor(particleInfo->GetParticleColor(pid));
  ampGraph1->SetMarkerColor(particleInfo->GetParticleColor(pid));
  ampGraph2->SetMarkerColor(particleInfo->GetParticleColor(pid));


  TF1 *totalFit = new TF1("totalFit",DoubleExponential,0,2,6);
  totalFit->FixParameter(5,particleInfo->GetParticleMass(pid));
  
  //Slope Fits
  TF1 *tSlopeGraph1Fit = new TF1("tSlopeGraph1Fit","gaus",-1,1);
  TF1 *tSlopeGraph2Fit = new TF1("tSlopeGraph2Fit","gaus",-1,1);
  
  //Amp Filts
  TF1 *ampGraph1Fit = new TF1("tAmpGraph1Fit","gaus",-1,1);
  TF1 *ampGraph2Fit = new TF1("tAmpGraph2Fit","pol0",-1,1);

  TMultiGraph *fullSpectrum = NULL;
  
  //We do multiple rounds of fitting. The first to parameterize the slope,
  //and the second to extract the dNdy.
  int nRounds(5);
  for (int iRound=0; iRound<nRounds; iRound++){
    
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      //Skip Rapidity bins not of interest
      if (yIndex < GetMinRapidityIndexOfInterest(pid) || yIndex > GetMaxRapidityIndexOfInterest(pid))
	continue;
      
      //If the user has specified a vaid rapidity value,
      //then skip all other rapidity bins
      if (userRapidityBin >= 0 && userRapidityBin != yIndex)
	continue;
      
      //Compute Rapidity
      Double_t rapidity = GetRapidityRangeCenter(yIndex);
      
      //Set the Local Pointers
      fullSpectrum = fullSpectra->at(iCentBin).at(yIndex);
      
      //Set the Fit Parameters
      totalFit->SetParameter(0,10);
      totalFit->SetParameter(1,51);
      totalFit->SetParameter(2,.23);
      totalFit->SetParameter(3,-15);
      totalFit->SetParameter(4,.15);
      
      Double_t A1min(.1), A1max(80);
      Double_t A2min(-30), A2max(-5);
      
      if (iRound == 0){
	totalFit->SetParLimits(0,0,500);
	totalFit->SetParLimits(1,A1min,A1max);
	totalFit->SetParLimits(2,0.01,1);
	totalFit->SetParLimits(3,A2min,A2max);
	totalFit->SetParLimits(4,0.001,.3);
      }
      
      if (iRound >= 1){
	totalFit->FixParameter(2,tSlopeGraph1Fit->Eval(GetRapidityRangeCenter(yIndex)));
      }
      
      if (iRound >= 2){
	totalFit->FixParameter(4,tSlopeGraph2Fit->Eval(GetRapidityRangeCenter(yIndex)));
      }
      
      if (iRound >= 3){
	totalFit->FixParameter(1,ampGraph1Fit->Eval(GetRapidityRangeCenter(yIndex)));
      }
      
      if (iRound >= 4){
	totalFit->FixParameter(3,ampGraph2Fit->Eval(GetRapidityRangeCenter(yIndex)));
      }
      
      //For the three most peripheral bins FIX A2 and T2 such that the
      //second expoential is irrelevant
      if (iCentBin < 4){
	totalFit->FixParameter(3,0);
	totalFit->FixParameter(4,0);
      }      
      
      //Fit the Spectrum
      int status(-1);
      int nAttempts(0);
      while (status != 0 && nAttempts < 5){
	status = fullSpectrum->Fit(totalFit,"REX0");
	nAttempts++;
      }
      
      Double_t dNdy = totalFit->GetParameter(0);
      Double_t dNdyErr = totalFit->GetParError(0);
      
      if (iRound == 0){
	tSlopeGraph1->SetPoint(tSlopeGraph1->GetN(),
			       GetRapidityRangeCenter(yIndex),
			       totalFit->GetParameter(2));
	tSlopeGraph1->SetPointError(tSlopeGraph1->GetN()-1,
				    0,
				    totalFit->GetParError(2));
      }
      
      if (iRound == 1){
	tSlopeGraph2->SetPoint(tSlopeGraph2->GetN(),
			       GetRapidityRangeCenter(yIndex),
			       totalFit->GetParameter(4));
	tSlopeGraph2->SetPointError(tSlopeGraph2->GetN()-1,
				    0,
				    totalFit->GetParError(4));
      }
      
      if (iRound == 2){
	ampGraph1->SetPoint(ampGraph1->GetN(),
			    GetRapidityRangeCenter(yIndex),
			    totalFit->GetParameter(1));
	ampGraph1->SetPointError(ampGraph1->GetN()-1,
				 0,
				 totalFit->GetParError(1));
      }
      
      
      if (iRound == 3){
	ampGraph2->SetPoint(ampGraph2->GetN(),
			    GetRapidityRangeCenter(yIndex),
			    totalFit->GetParameter(3));
	ampGraph2->SetPointError(ampGraph2->GetN()-1,
				 0,
				 totalFit->GetParError(3));
      }
      
      if (iRound == nRounds-1){
	dNdyGraph->SetPoint(dNdyGraph->GetN(),
			    GetRapidityRangeCenter(yIndex),
			    dNdy);
	dNdyGraph->SetPointError(dNdyGraph->GetN()-1,
				 0,
				 dNdyErr);
	
	resultsFile->cd();
	resultsFile->cd(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));
	totalFit->SetName(Form("%s_Fit",fullSpectrum->GetName()));
	totalFit->Write();
      }
      
      if (draw){
	canvas->cd();
	canvas->SetLogy();
	fullSpectrum->Draw("APZ");
	canvas->Update();
	
	dNdyCanvas->cd(1);
	dNdyGraph->Draw("APZ");
	dNdyCanvas->cd(2);
	tSlopeGraph1->Draw("PZ");
	tSlopeGraph2->Draw("PZ");
	dNdyCanvas->cd(3);
	ampGraph1->Draw("APZ");
	dNdyCanvas->cd(4);
	ampGraph2->Draw("APZ");
	
	dNdyCanvas->Update();
	
	gSystem->Sleep(100);
	
      }//End If Draw
      
      
    }//End Loop Over yIndex
    
    if (iRound == 0){
      //Fit the tSlope1Graph
      tSlopeGraph1Fit->FixParameter(1,0.0);
      tSlopeGraph1->Fit(tSlopeGraph1Fit,"EX0");
    }
    
    if (iRound == 1){
      //Fit the tSlopeGraph2
      tSlopeGraph2Fit->FixParameter(1,0.0);
      tSlopeGraph2->Fit(tSlopeGraph2Fit,"EX0");
    }
    
    if (iRound == 2){
      //Fit the ampGraph1
      ampGraph1Fit->FixParameter(1,0.0);
      ampGraph1->Fit(ampGraph1Fit,"EX0");
    }
    
    if (iRound == 3){
      //Fit the ampGraph2
      //ampGraph2Fit->FixParameter(1,0.0);
      ampGraph2->Fit(ampGraph2Fit,"EX0","",-.5,.5);
    }
    
  }//End Loop Over number of rounds

  //Save the parameter Graphs
  resultsFile->cd();
  resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
  if (tSlopeGraph1->GetN() > 0){
    tSlopeGraph1->Write();
    tSlopeGraph1Fit->Write();
  }
  if (tSlopeGraph2->GetN() > 0){
    tSlopeGraph2->Write();
    tSlopeGraph2Fit->Write();
  }
  if (ampGraph1->GetN() > 0){
    ampGraph1->Write();
    ampGraph1Fit->Write();
  }
  if (ampGraph2->GetN() > 0){
    ampGraph2->Write();
    ampGraph2Fit->Write();
  }    
  
  
  delete particleInfo;
  delete tSlopeGraph1;
  delete tSlopeGraph2;
  delete ampGraph1;
  delete ampGraph2;
  
  return dNdyGraph;
  
  }


//__________________________________________________________
TGraphErrors *FitWithSiemensRasmussen(std::vector< std::vector<TMultiGraph *> > *fullSpectra, int pid, int charge,
				       int iCentBin, int userRapidityBin, TCanvas *canvas, TCanvas *dNdyCanvas,
				       TFile *resultsFile){


  ParticleInfo *particleInfo = new ParticleInfo();
  TGraphErrors *dNdyGraph = new TGraphErrors();
  TGraphErrors *tSlopeGraph = new TGraphErrors();
  TGraphErrors *rBetaGraph = new TGraphErrors();
  
  dNdyGraph->SetName(Form("RapidityDensity_%s_Cent%02d",
			  particleInfo->GetParticleName(pid,charge).Data(),
			  iCentBin));

  dNdyGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  dNdyGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));

  tSlopeGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  tSlopeGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));
  
  rBetaGraph->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
  rBetaGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));

  TF1 *totalFit = new TF1("totalFit",SiemensRasmussen,0.3,2,4);
  totalFit->SetNpx(10000);
  totalFit->FixParameter(3,particleInfo->GetParticleMass(pid));

  TF1 *tSlopeGraphFit = NULL;
  /*
  new TF1("","gaus",-.65,.65);
  tSlopeGraphFit->FixParameter(1,0.0);
  */
  
  TF1 *rBetaGraphFit = new TF1("","pol0",-.65,.65);

  TMultiGraph *fullSpectrum = NULL;

  //Multiple rounds of fitting to constrain the parameters
  const int nRounds(2);
  for (int iRound=0; iRound<nRounds; iRound++){
    
    //Loop Over the Rapidity Bins and Fit the Spectra
    for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      //Skip Rapidity bins not of interest
      if (yIndex < GetMinRapidityIndexOfInterest(pid) || yIndex > GetMaxRapidityIndexOfInterest(pid))
	continue;
      
      //If the user has specified a vaid rapidity value,
      //then skip all other rapidity bins
      if (userRapidityBin >= 0 && userRapidityBin != yIndex)
	continue;
      
      //Compute Rapidity
      Double_t rapidity = GetRapidityRangeCenter(yIndex);
      
      //Set the Local Pointers
      fullSpectrum = fullSpectra->at(iCentBin).at(yIndex);
      
      //Set the Fit Parameters
      totalFit->SetParameter(0,10);
      totalFit->SetParameter(1,.2);
      totalFit->SetParameter(2,.1);

      if (tSlopeGraph->GetN() > 1)
	totalFit->SetParameter(1,tSlopeGraph->GetN()-1);
      if (rBetaGraph->GetN() > 1)
	totalFit->SetParameter(2,rBetaGraph->GetN()-1);
      
      totalFit->SetParLimits(0,0,100);
      totalFit->SetParLimits(1,.1,.6);
      totalFit->SetParLimits(2,0.0,.8);

      //Fix The Slope Parameter in the second round
      if (iRound >= 1){
	/*
	Double_t tVals[2] = {
	  tSlopeGraph->GetY()[TGraphFindPoint(tSlopeGraph,-fabs(rapidity))],
	  tSlopeGraph->GetY()[TGraphFindPoint(tSlopeGraph,fabs(rapidity))]
	};
	Double_t meanT = TMath::Mean(2,tVals);
	totalFit->FixParameter(1,meanT);
	*/
	totalFit->FixParameter(1,tSlopeGraphFit->Eval(GetRapidityRangeCenter(yIndex)));
      }
      if (iRound >= 2){

	//Fix the Radial Flow to the mean of the two sides
	Double_t betaVals[2] =
	  {rBetaGraph->GetY()[TGraphFindPoint(rBetaGraph,-fabs(rapidity))],
	   rBetaGraph->GetY()[TGraphFindPoint(rBetaGraph,fabs(rapidity))]};
	Double_t meanBeta = TMath::Mean(2,betaVals);	
	totalFit->FixParameter(2,meanBeta);
      }

      
      //Fit the Spectrum
      int status(-1);
      int nAttempts(0);
      while (status != 0 && nAttempts < 5){
	//if (iCentBin < 3)
	totalFit->FixParameter(2,0.0);
	status = fullSpectrum->Fit(totalFit,"REX0");
	nAttempts++;
      }
      

      if (iRound==0){
	//if (fabs((totalFit->GetChisquare()/totalFit->GetNDF())-1) < 2){
	  tSlopeGraph->SetPoint(tSlopeGraph->GetN(),GetRapidityRangeCenter(yIndex),totalFit->GetParameter(1));
	  tSlopeGraph->SetPointError(tSlopeGraph->GetN()-1,rapidityBinWidth/2.0,totalFit->GetParError(1));
	  //}
      }

      else if (iRound==1){
	//if (fabs((totalFit->GetChisquare()/totalFit->GetNDF())-1) < 2){
	  rBetaGraph->SetPoint(rBetaGraph->GetN(),GetRapidityRangeCenter(yIndex),totalFit->GetParameter(2));
	  rBetaGraph->SetPointError(rBetaGraph->GetN()-1,rapidityBinWidth/2.0,totalFit->GetParError(2));
	  //}
      }

      if (iRound == nRounds-1){
	Double_t dNdy = totalFit->GetParameter(0);
	Double_t dNdyErr = totalFit->GetParError(0);      
	
	dNdyGraph->SetPoint(dNdyGraph->GetN(),
			    GetRapidityRangeCenter(yIndex),dNdy);
	dNdyGraph->SetPointError(dNdyGraph->GetN()-1,
				 0,dNdyErr);
      }
      
      if (draw){
	canvas->cd();
	canvas->SetLogy();
	fullSpectrum->Draw("APZ");
	canvas->Update();
	
	dNdyCanvas->cd(1);
	dNdyGraph->Draw("APZ");
	
	dNdyCanvas->cd(2);
	tSlopeGraph->Draw("APZ");
	
	dNdyCanvas->cd(3);
	rBetaGraph->Draw("APZ");
	
	dNdyCanvas->Update();
	//gSystem->Sleep(1000);
      }//End IfDraw

      
    resultsFile->cd();
    resultsFile->cd(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));
    totalFit->SetName(Form("%s_Fit",fullSpectrum->GetName()));
    totalFit->Write();
    
  }//End Loop Over Rapidity Bins

    //Fit the Temperature parameter on the first round
    if (iRound==0){

      //First Remove outliers
      Double_t median = TMath::Median(tSlopeGraph->GetN(),tSlopeGraph->GetY());
      Double_t rms    = TMath::RMS(tSlopeGraph->GetN(),tSlopeGraph->GetY());
      for (int i=0; i<tSlopeGraph->GetN(); i++){
	if (fabs(tSlopeGraph->GetY()[i]-median) > rms*3)
	  tSlopeGraph->RemovePoint(i);
      }
      
      Double_t rangeMin = GetRapidityRangeLow(GetMinRapidityIndexOfInterest(pid));
      Double_t rangeMax = GetRapidityRangeHigh(GetMaxRapidityIndexOfInterest(pid));
      
      //Try both a constant and a gaussian to see
      //which is the better one
      TF1 tempFit1("","pol0",rangeMin,rangeMax);
      tSlopeGraph->Fit(&tempFit1,"RN");

      TF1 tempFit2("","gaus",rangeMin,rangeMax);
      tempFit2.FixParameter(1,0.0);
      tSlopeGraph->Fit(&tempFit2,"RN");

      if (fabs((tempFit1.GetChisquare()/tempFit1.GetNDF()) - 1) <
	  fabs((tempFit2.GetChisquare()/tempFit2.GetNDF()) - 1)){
	tSlopeGraphFit = new TF1("","pol0",rangeMin,rangeMax);
      }
      else {
	tSlopeGraphFit = new TF1("","gaus",rangeMin,rangeMax);
	tSlopeGraphFit->FixParameter(1,0.0);
      }
      
      //tSlopeGraphFit = new TF1("","gaus",rangeMin,rangeMax);
      //tSlopeGraphFit->FixParameter(1,0.0);
      tSlopeGraph->Fit(tSlopeGraphFit,"R");
    }
    if (iRound==1){
      rBetaGraph->Fit(rBetaGraphFit,"R");
    }

    if (draw)
      dNdyCanvas->Update();
       
  }//End Loop Over Rounds of Fitting

  /*
  //Determine the Error Associated with Fixing the radial flow to a constant
  //by varying the parameter to its one sigma value
  Double_t meanBeta = TMath::Mean(rBetaGraph->GetN(),rBetaGraph->GetY());
  Double_t oneSigmaBeta = TMath::RMS(rBetaGraph->GetN(),rBetaGraph->GetY());
  for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

    //Skip Rapidity bins not of interest
    if (yIndex < GetMinRapidityIndexOfInterest(pid) || yIndex > GetMaxRapidityIndexOfInterest(pid))
      continue;
    
    //If the user has specified a vaid rapidity value,
    //then skip all other rapidity bins
    if (userRapidityBin >= 0 && userRapidityBin != yIndex)
      continue;
    
    //Compute Rapidity
    Double_t rapidity = GetRapidityRangeCenter(yIndex);
    
    //Set the Local Pointers
    fullSpectrum = fullSpectra->at(iCentBin).at(yIndex);
    
    //Set the Fit Parameters
    totalFit->SetParameter(0,10);
    totalFit->FixParameter(1,tSlopeGraphFit->Eval(rapidity));
    
    totalFit->SetParLimits(0,0,100);

    //Do the High RMS Fit
    totalFit->FixParameter(2,meanBeta+oneSigmaBeta);

    fullSpectrum->Fit(totalFit,"REX0N");
    Double_t highdNdy = totalFit->GetParameter(0);

    //Do the Low RMS Fit
    totalFit->FixParameter(2,meanBeta-oneSigmaBeta);

    fullSpectrum->Fit(totalFit,"REXON");
    Double_t lowdNdy = totalFit->GetParameter(0);

    Double_t vals[2] = {highdNdy,lowdNdy};
    Double_t rmsdNdy = TMath::RMS(2,vals);

    Int_t dNdyPoint = TGraphFindPoint(dNdyGraph,rapidity);
    dNdyGraph->SetPointError(dNdyPoint,
			     dNdyGraph->GetEX()[dNdyPoint],
			     dNdyGraph->GetEY()[dNdyPoint]+rmsdNdy);


  }//End Loop Over Rapidity Bins
  */
  
  delete totalFit;
  delete particleInfo;
  /*
  delete tSlopeGraph;
  delete rBetaGraph;
  delete tSlopeGraphFit;
  delete rBetaGraphFit;
  */
  
  return dNdyGraph;
  
}
  

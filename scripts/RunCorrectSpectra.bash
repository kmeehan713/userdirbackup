#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunCorrectSpectra.bash"
    echo "    PURPOSE:"
    echo "        This corrects the raw spectra by applying several corrections: "
    echo "        1. Finite bin size (dmT)"
    echo "        2. Energy Loss (pT_Reco - pT_Emb)"
    echo "        3. TPC Tracking Efficiency (nTracksReco/nTracksEmb)"
    echo "    USAGE:"
    echo "        ./RunCorrectSpectra.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -p - only do this particle ID (pion=0, kaon=1, proton=2)" 
    echo "        -q - only do this charge (-1 or 1)"
    echo ""
    echo ""
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:q:h" opts; do
	case "$opts" in
	    p) userSpecies="${OPTARG}"; shift;;
	    q) userCharge="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile


#If the User has specfied a particular species and/or charge then only do that species
if [ ! -z $userSpecies ]; then

    #If the User has specified a particular charge then only do that charge
    if [ ! -z $userCharge ]; then
        root -l ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",$userSpecies,$userCharge,\"$starLib\",$energy,\"$eventConfig\"\) #>> $logDir/SpectraCorrection.log 2>&1
    else
	root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",$userSpecies,-1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
	root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",$userSpecies,1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
    fi

    exit 0
fi

#If the user has specified only a particular charge
if [ ! -z $userCharge ]; then

    root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",0,$userCharge,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
    root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",1,$userCharge,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
    root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",2,$userCharge,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1

    exit 0
fi

#If the user has specified neither charge nor species (the default)
root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",0,-1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",0,1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
#root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",1,-1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
#root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",1,1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
#root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",2,-1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFile\",\"$correctionFile\",2,1,\"$starLib\",$energy,\"$eventConfig\"\) >> $logDir/SpectraCorrection.log 2>&1
    
